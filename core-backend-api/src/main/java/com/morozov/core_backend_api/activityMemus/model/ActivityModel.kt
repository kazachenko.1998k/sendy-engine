package com.morozov.core_backend_api.activityMemus.model

import com.morozov.core_backend_api.feedMemus.model.OwnerModel
import com.morozov.core_backend_api.feedMemus.model.PostModel

interface ActivityModel {
    val sourceActionOwners: List<OwnerModel>
    val activityType: ActivityType
    val date: String
    val isSeen: Boolean
    val isSubscribe: Boolean?
    val post: PostModel?
}