package com.morozov.core_backend_api.activityMemus.modelResponse

import com.morozov.core_backend_api.activityMemus.model.ActivityModel


data class GetActivityResponse(val activities: List<ActivityModel>)