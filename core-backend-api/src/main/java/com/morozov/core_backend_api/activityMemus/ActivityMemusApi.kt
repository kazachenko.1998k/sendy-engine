package com.morozov.core_backend_api.activityMemus

import com.morozov.core_backend_api.activityMemus.modelResponse.GetActivityResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ActivityMemusApi {

    @GET("content.getActivity")
    fun getActivity(): Single<GetActivityResponse?>

}