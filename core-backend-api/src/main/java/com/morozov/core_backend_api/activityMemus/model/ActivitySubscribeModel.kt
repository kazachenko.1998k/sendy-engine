package com.morozov.core_backend_api.activityMemus.model

import com.morozov.core_backend_api.feedMemus.model.OwnerModel
import com.morozov.core_backend_api.feedMemus.model.PostModel

data class ActivitySubscribeModel(
    override val sourceActionOwners: List<OwnerModel>,
    override val activityType: ActivityType = ActivityType.SUBSCRIBE,
    override val date: String = "01.01.2000 00:00:00",
    override val isSeen: Boolean = false,
    override val isSubscribe: Boolean = false,
    override val post: PostModel? = null
) : ActivityModel