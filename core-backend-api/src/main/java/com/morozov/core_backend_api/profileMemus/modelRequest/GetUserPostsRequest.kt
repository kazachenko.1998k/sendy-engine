package com.morozov.core_backend_api.profileMemus.modelRequest

data class GetUserPostsRequest(val idUser: String)