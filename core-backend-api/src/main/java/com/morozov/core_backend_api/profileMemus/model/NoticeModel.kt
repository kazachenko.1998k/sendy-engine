package com.morozov.core_backend_api.profileMemus.model

data class NoticeModel(
    val likes: Boolean,
    val comments: Boolean,
    val new_subscriber: Boolean,
    val post_in_best: Boolean,
    val added_to_chat: Boolean,
    val removed_from_chat: Boolean
)