package com.morozov.core_backend_api.profileMemus.model

data class ProfileModel(
    val id: String,
    val phone: String,
    val avatar: String?,
    val username: String,
    val email: String?,
    val name: String?,
    val about: String?,
    val subscribersCount: Int,
    val subscriptionCount: Int,
    val chanelCount: Int
)