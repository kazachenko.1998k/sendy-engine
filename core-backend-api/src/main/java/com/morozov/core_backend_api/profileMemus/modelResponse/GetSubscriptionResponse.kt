package com.morozov.core_backend_api.profileMemus.modelResponse

import com.morozov.core_backend_api.profileMemus.model.NoticeModel

data class GetSubscriptionResponse (val subscriptions :List<NoticeModel>)