package com.morozov.core_backend_api.profileMemus.modelResponse

import com.morozov.core_backend_api.profileMemus.model.UserModel


data class GetSearchUsersResponse(val users: List<UserModel>)