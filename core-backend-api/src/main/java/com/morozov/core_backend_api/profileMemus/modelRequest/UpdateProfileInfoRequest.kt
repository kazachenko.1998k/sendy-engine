package com.morozov.core_backend_api.profileMemus.modelRequest

import com.morozov.core_backend_api.profileMemus.model.ProfileModel

data class UpdateProfileInfoRequest(val profile: ProfileModel)