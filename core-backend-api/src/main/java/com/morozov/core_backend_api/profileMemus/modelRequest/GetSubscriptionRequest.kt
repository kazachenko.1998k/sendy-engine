package com.morozov.core_backend_api.profileMemus.modelRequest

data class GetSubscriptionRequest(val position: Int, val count: Int)