package com.morozov.core_backend_api.profileMemus.model

data class UserModel(
    val avatar: String,
    val userId: String,
    val userName: String?,
    val description: String?,
    var isSubscribed: Boolean
)