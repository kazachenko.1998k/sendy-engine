package com.morozov.core_backend_api.profileMemus.modelResponse

import com.morozov.core_backend_api.profileMemus.model.ProfileModel

data class GetProfileInfoResponse(
    val profile: ProfileModel
)