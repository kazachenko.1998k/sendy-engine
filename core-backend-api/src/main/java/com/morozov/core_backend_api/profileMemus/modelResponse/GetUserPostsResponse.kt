package com.morozov.core_backend_api.profileMemus.modelResponse

import com.morozov.core_backend_api.feedMemus.model.PostModel


data class GetUserPostsResponse(val idUser: String,  val postUser: List<PostModel>)