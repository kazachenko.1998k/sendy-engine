package com.morozov.core_backend_api.profileMemus

import com.morozov.core_backend_api.profileMemus.modelRequest.*
import com.morozov.core_backend_api.profileMemus.modelResponse.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ProfileMemusApi {

    @POST("content.getSubscription")
    fun getSubscription(@Body getSubscriptionRequest: GetSubscriptionRequest): Single<GetSubscriptionResponse?>


    @GET("content.getProfile")
    fun getProfileInfo(): Single<GetProfileInfoResponse?>

    @POST("content.updateProfile")
    fun updateProfileInfo(@Body updateProfileInfoRequest: UpdateProfileInfoRequest): Single<UpdateProfileInfoResponse?>

    @POST("content.getUserPosts")
    fun getUserPosts(@Body getUserPostsRequest: GetUserPostsRequest): Single<GetUserPostsResponse?>

    @POST("content.getSearchUsers")
    fun getSearchUsers(@Body getSearchUsersRequest: GetSearchUsersRequest): Single<GetSearchUsersResponse?>

    @POST("content.getLastSearchUsers")
    fun getLastSearchUsers(): Single<GetLastSearchUsersResponse?>
}