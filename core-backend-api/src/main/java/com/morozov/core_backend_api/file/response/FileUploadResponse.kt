package com.morozov.core_backend_api.file.response

import com.morozov.core_backend_api.AnyResponse

data class FileUploadResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<FileUploadResponse.Data>() {
    data class Data(
        val file_id: String,
        val file_upload_to: String
    )
}