package com.morozov.core_backend_api.file.request

data class FileUploadRequest(
    val name: String,
    val size: Int,
    val ext: String,
    var duration: Int? = null,
    var title: String? = null
) {
    var width: Int? = null
    var height: Int? = null
    var quality: String? = null
    var source_url: String? = null
}