package com.morozov.core_backend_api.file

import androidx.annotation.IntDef
import com.squareup.moshi.Json

data class FileModel(

    //Уникальный идентификатор файла
    @field:Json(name = "file_id")
    var fileId: String? = null,

    @FileType
    @field:Json(name = "type")
    var type: Int? = -1,

    // Расширение файла
    var ext: String? = null,

    //Ширина файла (для картинок)
    @field:Json(name = "width")
    var width: Int? = null,

    //Высота файла (для картинок)
    @field:Json(name = "height")
    var height: Int? = null,

    //Длина файла (для аудио, для видео)
    @field:Json(name = "length")
    var length: Int? = null,

    //Размер файла в байтах
    @field:Json(name = "size")
    var size: Int? = null,

    //Название файла (например для аудио - название артиста)
    @field:Json(name = "title")
    var title: String? = null,

    //Название файла (например для аудио - название артиста)
    @field:Json(name = "state")
    var state: Int? = null,

    //Название файла (например для аудио - название артиста)
    @field:Json(name = "source_url")
    var sourceUrl: String? = null
) {
    companion object {

        @Target(AnnotationTarget.PROPERTY, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
        @IntDef(
            flag = false,
            value = [FILE_TYPE_IMAGE,
                FILE_TYPE_AUDIO,
                FILE_TYPE_GIF,
                FILE_TYPE_MP4,
                FILE_TYPE_HLS,
                FILE_TYPE_TXT,
                FILE_TYPE_ZIP,
                FILE_TYPE_EXE,
                FILE_TYPE_ANOTHER]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class FileType

        //1 - картинка
        //2 - аудио
        //3 - гифка
        //4 - видео mp4
        //5 - видео hls
        //6 - текстовый файл
        //7 - архив
        //8 - исполняемый файл
        //9 - прочее
        const val FILE_TYPE_IMAGE = 1
        const val FILE_TYPE_AUDIO = 2
        const val FILE_TYPE_GIF = 3
        const val FILE_TYPE_MP4 = 4
        const val FILE_TYPE_HLS = 5
        const val FILE_TYPE_TXT = 6
        const val FILE_TYPE_ZIP = 7
        const val FILE_TYPE_EXE = 8
        const val FILE_TYPE_ANOTHER = 9
    }

}