package com.morozov.core_backend_api.file

import android.content.Context
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.response.FileUploadResponse
import io.reactivex.Single

interface FileApi {
    companion object{
        const val FILE_UPLOAD = "file.upload"
    }

    // Отправка файлов
    fun uploadFile(request: AnyRequest, callback: (response: FileUploadResponse) -> Unit = {})
}