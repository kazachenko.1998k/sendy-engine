package com.morozov.core_backend_api.file.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.file.FileModel

class FileConverter {
    @TypeConverter
    fun fromFile(files: MutableList<FileModel>?): String? {
        return if (files == null) null
        else
            Gson().toJson(files)
    }

    @TypeConverter
    fun toFile(data: String?): MutableList<FileModel>? {
        return if (data == null) null
        else {
            val turnsType = object : TypeToken<MutableList<FileModel>>() {}.type
            Gson().fromJson(data, turnsType)
        }
    }
}