package com.morozov.core_backend_api.file.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.morozov.core_backend_api.file.FileModel

class FileModelConverter {
    @TypeConverter
    fun fromFileModel(fileModel: FileModel?): String? {
        return if (fileModel == null) null
        else
            Gson().toJson(fileModel)
    }

    @TypeConverter
    fun toFileModel(data: String?): FileModel? {
        return if (data == null) null
        else
            Gson().fromJson(data, FileModel::class.java)
    }
}