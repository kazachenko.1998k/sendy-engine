package com.morozov.core_backend_api


data class EmptyResponse(private val dateCreate: Long = System.currentTimeMillis()) : AnyResponse<Any>()