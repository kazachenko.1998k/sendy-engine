package com.morozov.core_backend_api

import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import java.security.MessageDigest

object SignData {
    var session = ""
    var serverFile = ""
    var sign = ""
    var server_file = ""
    var uid = 0L
    var lambdaTime = 0L
    val connectionState = MutableLiveData<Boolean>()

    const val appPublicKey =
        "81e8e8aadee1dc9ef2aa24b0b2aa316eb8d376e6b69b1ba63aabdbffb282966b\\r\\nd49da090057b49a6250ded2449f1a9faf743f899cec2961eac0e1a7890057c43"

    const val applicationId = 10

    fun AnyRequest.updateSign() {
        this.time = (System.currentTimeMillis() - lambdaTime) / 1000
        this.sign2 = this.generateSign2()

    }

    data class AnyRequestForSign(
        val data: AnyInnerRequest,
        val rid: Int,
        val time: Long
    )


    private fun AnyRequest.generateSign(): String {
        return this.toGsonString().toSecure()
    }

    private fun AnyRequest.generateSign2(): String {
        return AnyRequestForSign(this.data, this.rid, this.time).toGson2String().toSecure()
    }

    private fun AnyRequest.toGsonString(): String {
        val gson = Gson()
        val json = gson.toJson(this.data)
        val stringBuilder = StringBuilder()
        stringBuilder
            .append("data=").append(json.toString())
            .append("rid=").append(this.rid)
            .append("time=").append(this.time).append(".")
            .append((this.time * 1000) % 1000)
            .append(appPublicKey)
            .append(session)
        return stringBuilder.toString()
    }

    private fun AnyRequestForSign.toGson2String(): String {
        val gson = Gson()
        val json = gson.toJson(this)
        val stringBuilder = StringBuilder()
        stringBuilder
            .append(json)
            .append(appPublicKey)
            .append(session)
        return stringBuilder.toString()
    }

    private fun String.toSecure() = this.toBase64().toMD5()

    private fun String.toBase64(): String =
        Base64.encodeToString(this.toByteArray(), Base64.NO_WRAP)

    private fun String.toMD5(): String {
        val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
        return bytes.toHex()
    }

    private fun ByteArray.toHex(): String {
        return joinToString("") { "%02x".format(it) }
    }
}