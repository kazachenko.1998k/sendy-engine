package com.morozov.core_backend_api.location.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.morozov.core_backend_api.location.Location

class LocationConverter {
    @TypeConverter
    fun fromLocation(location: Location?): String? {
        return if (location == null) null
        else
            Gson().toJson(location)
    }

    @TypeConverter
    fun toLocation(data: String?): Location? {
        return if (data == null) null
        else
            Gson().fromJson(data, Location::class.java)
    }
}