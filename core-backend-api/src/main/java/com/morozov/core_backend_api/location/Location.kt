package com.morozov.core_backend_api.location

import com.squareup.moshi.Json

data class Location (

    @field:Json(name = "lat")
    var lat: Double? = 0.0,

    @field:Json(name = "lon")
    var lng: Double? = 0.0,

    //Текстовый адрес места
    @field:Json(name = "address")
    var address: String? = "",

    //Идентификатор города
    @field:Json(name = "city_id")
    var cityId: Long? = -1,

    //Идентификатор страны
    @field:Json(name = "country_id")
    var countryId: Long? = -1
)