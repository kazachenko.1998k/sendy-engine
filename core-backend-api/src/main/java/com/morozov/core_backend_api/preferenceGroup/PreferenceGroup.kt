package com.morozov.core_backend_api.preferenceGroup

import com.squareup.moshi.Json

data class PreferenceGroup(

    //Идентификатор группы настроек
    @field:Json(name = "code")
    var code: Int? = null,

    //Название
    @field:Json(name = "title")
    var title: String? = null
)
