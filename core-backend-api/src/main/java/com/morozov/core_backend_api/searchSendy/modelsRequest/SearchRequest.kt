package com.morozov.core_backend_api.searchSendy.modelsRequest

import com.morozov.core_backend_api.searchSendy.models.ModelSearchTarget

data class SearchRequest(val typeSearch: ModelSearchTarget, val exp: String, val amount: Int)