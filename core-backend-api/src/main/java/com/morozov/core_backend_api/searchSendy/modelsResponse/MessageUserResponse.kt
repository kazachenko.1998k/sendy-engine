package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelMessageUser

data class MessageUserResponse(val messagesUser : List<ModelMessageUser>)