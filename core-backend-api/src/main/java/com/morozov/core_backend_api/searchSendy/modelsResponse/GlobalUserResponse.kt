package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelGlobalUser

data class GlobalUserResponse(val globalUsers : List<ModelGlobalUser>)