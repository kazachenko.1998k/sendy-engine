package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelGlobalGroup

data class GlobalGroupResponse(val globalGroups : List<ModelGlobalGroup>)