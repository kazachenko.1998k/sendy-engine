package com.morozov.core_backend_api.searchSendy.models

data class ModelMessageGroup(val id: String, val avatar: String, val nameGroup: String,
                             val login: String, val data: Long, val textMessage: String)