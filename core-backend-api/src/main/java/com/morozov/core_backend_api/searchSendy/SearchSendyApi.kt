package com.morozov.core_backend_api.searchSendy

import com.morozov.core_backend_api.searchSendy.modelsRequest.*
import com.morozov.core_backend_api.searchSendy.modelsResponse.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface SearchSendyApi {

    @POST("search.search")
    fun search(@Body request: SearchRequest): Single<SearchResponse?>

}