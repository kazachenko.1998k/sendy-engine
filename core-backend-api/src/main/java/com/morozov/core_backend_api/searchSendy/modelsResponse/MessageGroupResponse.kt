package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelMessageGroup

data class MessageGroupResponse(val messagesGroup : List<ModelMessageGroup>)