package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelGlobalChannel

data class SearchResponse(val response : List<ModelGlobalChannel>)