package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelLocalGroup

data class LocalGroupResponse(val localGroups : List<ModelLocalGroup>)