package com.morozov.core_backend_api.searchSendy.models

data class ModelGlobalUser(val id: String, val avatar: String, val firstName: String, val secondName: String,
                           val login: String, val dataLastOnline: Long)