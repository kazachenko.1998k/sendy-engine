package com.morozov.core_backend_api.searchSendy.models

data class ModelGlobalChannel(val id: String, val avatar: String, val name: String,
                              val countSubscribers: Int, val lastMessage: String)