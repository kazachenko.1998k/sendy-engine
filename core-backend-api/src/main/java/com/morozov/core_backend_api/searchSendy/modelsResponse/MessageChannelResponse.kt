package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelMessageChannel

data class MessageChannelResponse(val messagesChannels : List<ModelMessageChannel>)