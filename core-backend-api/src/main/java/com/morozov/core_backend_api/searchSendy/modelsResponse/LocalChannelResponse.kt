package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelLocalChannel

data class LocalChannelResponse(val localChannels : List<ModelLocalChannel>)