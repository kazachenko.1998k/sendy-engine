package com.morozov.core_backend_api.searchSendy.models

data class ModelLocalGroup(val id: String, val avatar: String, val name: String,
                           val countParticipants: Int, val lastMessage: String)