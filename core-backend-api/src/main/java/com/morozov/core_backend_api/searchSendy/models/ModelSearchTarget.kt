package com.morozov.core_backend_api.searchSendy.models

enum class ModelSearchTarget {
    LOCAL_USER,
    GLOBAL_USER,
    LOCAL_CHANNEL,
    GLOBAL_CHANNEL,
    LOCAL_GROUP,
    GLOBAL_GROUP,
    USER_MESSAGE,
    CHANNEL_MESSAGE,
    GROUP_MESSAGE
}