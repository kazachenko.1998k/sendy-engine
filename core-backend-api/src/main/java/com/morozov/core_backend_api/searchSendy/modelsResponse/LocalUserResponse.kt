package com.morozov.core_backend_api.searchSendy.modelsResponse

import com.morozov.core_backend_api.searchSendy.models.ModelLocalUser

data class LocalUserResponse(val localUsers : List<ModelLocalUser>)