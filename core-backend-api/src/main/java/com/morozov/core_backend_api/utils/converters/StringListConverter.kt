package com.morozov.core_backend_api.utils.converters

import android.os.Build
import androidx.room.TypeConverter
import java.util.*
import java.util.stream.Collectors

class StringListConverter {
    private val delimiter = "_!DEL!_"

    @TypeConverter
    fun fromList(list: List<String>): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.stream().collect(Collectors.joining(delimiter))
        } else {
            var resStr = ""
            for (s in list) {
                resStr += s + delimiter
            }
            resStr
        }
    }

    @TypeConverter
    fun toList(data: String): List<String> {
        return data.split(delimiter)
    }
}