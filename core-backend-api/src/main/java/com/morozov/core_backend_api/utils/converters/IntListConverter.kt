package com.morozov.core_backend_api.utils.converters

import android.os.Build
import androidx.room.TypeConverter
import java.util.*
import java.util.stream.Collectors

class IntListConverter {
    private val delimiter = "_!DEL!_"

    @TypeConverter
    fun fromList(list: List<Int>): String {
        var resStr = ""
        for (s in list) {
            resStr += s.toString() + delimiter
        }
        return resStr
    }

    @TypeConverter
    fun toList(data: String): List<Int> {
        val resList = mutableListOf<Int>()
        for (s in data.split(delimiter)) {
            if (s.isNotEmpty())
                resList.add(s.toInt())
        }
        return resList
    }
}