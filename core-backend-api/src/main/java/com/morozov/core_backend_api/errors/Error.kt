package com.morozov.core_backend_api.errors

data class Error(val code: Int = -1, val msg: String = "")