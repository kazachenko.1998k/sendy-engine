package com.morozov.core_backend_api

abstract class AnyResponse<T>(
    var data: T? = null
) : AbstractResponse() {
    override fun toString(): String {
        return "\"result\":$result, " +
                "\"rid\":$rid, " +
                "\"method\":$method, " +
                "\"data\":$data, " +
                "\"error\":$error"
    }
}