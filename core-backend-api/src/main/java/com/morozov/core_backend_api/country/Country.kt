package com.morozov.core_backend_api.country


import com.squareup.moshi.Json

data class Country (

    //Идентификатор страны
    @field:Json(name = "id")
    var id: Int? = -1,

    //Название страны
    @field:Json(name = "title")
    var title: String? = "",

    //Код страны
    @field:Json(name = "code")
    var code: String? = "",

    // Стандартный префикс телефона
    @field:Json(name = "phone")
    var phone: Int? = -1
)