package com.morozov.core_backend_api

import kotlin.random.Random

data class AnyRequest(
    var data: AnyInnerRequest,
    var rid: Int = Random.nextInt(0, Int.MAX_VALUE),
    var sign2: String = "",
    var time: Long = 0L
)
