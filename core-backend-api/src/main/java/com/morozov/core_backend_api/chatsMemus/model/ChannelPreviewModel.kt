package com.morozov.core_backend_api.chatsMemus.model

data class ChannelPreviewModel (var avatar: String,
                                val channelId: String?,
                                var channelName: String?,
                                var countSubscribers: Int?,
                                var dateActive: String?)