package com.morozov.core_backend_api.chatsMemus

import com.morozov.core_backend_api.chatsMemus.modelRequest.GetChannelsRequest
import com.morozov.core_backend_api.chatsMemus.modelRequest.GetChatsRequest
import com.morozov.core_backend_api.chatsMemus.modelResponse.GetChannelsResponse
import com.morozov.core_backend_api.chatsMemus.modelResponse.GetChatsResponse
import com.morozov.core_backend_api.commonMemus.modelResponse.CommonGetUpdatesResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ChatsMemusApi {


    @POST("content.getChats")
    fun getChatsPreview(@Body getChatsRequest: GetChatsRequest): Single<GetChatsResponse?>


    @POST("content.getChats")
    fun getChannelsPreview(@Body getChannelsRequest: GetChannelsRequest): Single<GetChannelsResponse?>

}