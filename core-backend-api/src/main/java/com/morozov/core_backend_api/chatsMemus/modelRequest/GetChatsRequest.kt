package com.morozov.core_backend_api.chatsMemus.modelRequest

data class GetChatsRequest(private val position: Int, private val count: Int)