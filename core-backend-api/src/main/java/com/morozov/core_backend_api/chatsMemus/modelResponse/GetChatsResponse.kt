package com.morozov.core_backend_api.chatsMemus.modelResponse

import com.morozov.core_backend_api.chatsMemus.model.ChatPreviewModel

data class GetChatsResponse(val chats: List<ChatPreviewModel>)