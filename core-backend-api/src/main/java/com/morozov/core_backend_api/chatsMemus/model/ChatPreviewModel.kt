package com.morozov.core_backend_api.chatsMemus.model

data class ChatPreviewModel (var avatar: String,
                             val userId: String?,
                             var userName: String?,
                             var description: String?,
                             var dateActive: String?)