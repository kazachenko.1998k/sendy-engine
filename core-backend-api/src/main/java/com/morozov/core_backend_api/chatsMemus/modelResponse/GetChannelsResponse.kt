package com.morozov.core_backend_api.chatsMemus.modelResponse

import com.morozov.core_backend_api.chatsMemus.model.ChannelPreviewModel

data class GetChannelsResponse(val channels: List<ChannelPreviewModel>)