package com.morozov.core_backend_api.session

import com.squareup.moshi.Json

data class Session(
    //Идентификатор сессии
    @field:Json(name = "session_id")
    var sessionId: String = "",

    //время начала сессии
    @field:Json(name = "session_start")
    var sessionStart: Long = -1,

    //Время последней активности в рамках данной сессии
    @field:Json(name = "last_active")
    var lastActive: Long? = -1,

    //IP адрес последней активности в рамках сессии
    @field:Json(name = "ip_addr")
    var ipAddr: String? = ""

)