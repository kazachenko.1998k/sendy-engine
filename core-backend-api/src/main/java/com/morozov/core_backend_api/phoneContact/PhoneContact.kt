package com.morozov.core_backend_api.phoneContact

import com.squareup.moshi.Json

data class PhoneContact(

    //Телефон контакта
    var phone: String = "",

    //Имя контакта
    @field:Json(name = "first_name")
    var firstName: String = "",

    //Фамилия контакта
    @field:Json(name = "second_name")
    var secondName: String = ""

)