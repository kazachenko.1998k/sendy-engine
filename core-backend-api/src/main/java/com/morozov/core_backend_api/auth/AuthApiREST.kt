package com.morozov.core_backend_api.auth

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.auth.modelsRequest.AuthInitRequest
import com.morozov.core_backend_api.auth.modelsRequest.AuthSetTokenRequest
import com.morozov.core_backend_api.auth.modelsResponse.AuthStartResponse
import com.morozov.core_backend_api.auth.modelsResponse.InitResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApiREST {


    @POST("http/v1.0/info")
    fun getTime(
        @Header("user_session") userSession: String,
        @Body request: AuthInitRequest?
    ): Single<String?>

    @POST("http/init")
    fun init(
        @Body request: AuthInitRequest?
    ): Single<InitResponse>

    @POST("http/v1.0/auth")
    fun setToken(
        @Body request: AnyRequest?
    ): Single<EmptyResponse>

    @POST("server/start")
    fun start(): Single<AuthStartResponse>

}