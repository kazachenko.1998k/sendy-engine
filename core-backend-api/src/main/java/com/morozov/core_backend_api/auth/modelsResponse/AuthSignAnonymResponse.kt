package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.UserChat
import com.morozov.core_backend_api.user.User
import com.squareup.moshi.Json

data class AuthSignAnonymResponse
    (private val dateCreate: Long = System.currentTimeMillis())
    : AnyResponse<AuthSignAnonymResponse.Data?>() {
    data class Data(
        var user: User = User(),
        var prefrence: List<String> = mutableListOf(),
        var chats:  List<UserChat> = mutableListOf(),
        @field:Json(name = "chats_favorite")
        var chatsFavorite: List<Chat> = mutableListOf(),
        @field:Json(name = "chats_recommended")
        var chatsRecommended: List<Chat> = mutableListOf(),
        @field:Json(name = "feed_recommend")
        var feedRecommend: List<String> = mutableListOf()
    )
}