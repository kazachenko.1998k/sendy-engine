package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User
import com.squareup.moshi.Json

data class AuthCheckCodeResponse
    (private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<AuthCheckCodeResponse.Data?>() {
    data class Data(
        var user: User = User(),
        var prefrence: List<String> = mutableListOf(),
        @field:Json(name = "need_nick")
        var needNick: Boolean = false
    )
}

