package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse

data class InitResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InitResponse.Data>() {
    data class Data(
        val server: String = "",
        val server_socket: String = "",
        val server_file: String = "",
        //Строка идентификатор сессии пользователя - необходим для всех следующих запросов!
        val session: String = "",
        val b: Int = 0,
        val sign: String = ""
    )
}
