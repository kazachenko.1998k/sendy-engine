package com.morozov.core_backend_api.auth.modelsRequest

data class AuthSetTokenRequest(
    val notification_token: String
)