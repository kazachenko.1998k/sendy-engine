package com.morozov.core_backend_api.auth.modelsRequest

import com.squareup.moshi.Json

data class AuthCheckCodeRequest(
    var code: Int
)