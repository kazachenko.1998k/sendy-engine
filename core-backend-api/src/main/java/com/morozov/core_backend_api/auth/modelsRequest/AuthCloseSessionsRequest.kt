package com.morozov.core_backend_api.auth.modelsRequest

import com.squareup.moshi.Json

data class AuthCloseSessionsRequest(
    //Идентификатор закрываемой сессии. Если отправить значение 0, то будут закрыты все сессии, кроме текущей.
    var session_id: String
)