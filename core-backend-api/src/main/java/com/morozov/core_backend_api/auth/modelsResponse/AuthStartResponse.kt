package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse

data class AuthStartResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<AuthStartResponse.Data>() {
    data class Data(
        val time: Long = 0L,
        val time_ms: Long = 0L,
        val country: String = "RU",
        //Строка идентификатор сессии пользователя - необходим для всех следующих запросов!
        val config_udapte: Long = 0L,
        val info_update: Long = 0L
    )
}
