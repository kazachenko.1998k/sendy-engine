package com.morozov.core_backend_api.auth

import android.content.Context
import android.preference.PreferenceManager

object AuthPreferences {

    const val AUTH_ALLOWED = "AUTH_ALLOWED"



    fun setPreference(context: Context, pref: String, value: Boolean) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putBoolean(pref, value)
        editor.apply()
    }

    fun getBoolPreference(context: Context, pref: String): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean(pref, false)
    }
}