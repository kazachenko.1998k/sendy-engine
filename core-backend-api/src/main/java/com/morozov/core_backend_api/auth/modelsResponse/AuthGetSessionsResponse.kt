package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.session.Session
import com.squareup.moshi.Json

data class AuthGetSessionsResponse
    (private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<AuthGetSessionsResponse.Data?>() {
    data class Data(
        @field:Json(name = "sessions")
        var sessions: MutableList<Session>? = null
    )
}