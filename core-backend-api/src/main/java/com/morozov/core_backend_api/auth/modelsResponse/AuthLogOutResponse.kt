package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse
import com.squareup.moshi.Json

data class AuthLogOutResponse
    (private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<AuthLogOutResponse.Data?>() {
    data class Data(
        //Текст сообщения который надо показать пользователю - в нем будет указана причина разлогина.
        @field:Json(name = "msg")
        var msg: String? = null
    )
}