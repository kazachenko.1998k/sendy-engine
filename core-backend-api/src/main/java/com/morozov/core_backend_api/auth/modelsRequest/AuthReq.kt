package com.morozov.core_backend_api.auth.modelsRequest

import com.morozov.core_backend_api.SignData

data class AuthReq(
    val device_hash: String,
    val app_key_id: String = SignData.applicationId.toString(),
    val user_id: Long,
    val user_session: String,
    val device_model: String,
    val timestamp: Long = System.currentTimeMillis()
)