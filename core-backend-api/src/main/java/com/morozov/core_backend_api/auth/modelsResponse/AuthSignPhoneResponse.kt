package com.morozov.core_backend_api.auth.modelsResponse

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.errors.Error
import com.squareup.moshi.Json

data class AuthSignPhoneResponse
    (private val dateCreate: Long = System.currentTimeMillis())
    : AnyResponse<AuthSignPhoneResponse.Data?>() {
    data class Data(
        @field:Json(name = "code_id")
        var codeId: Int,
        @field:Json(name = "send_type")
        var sendType: Int,
        @field:Json(name = "next_code_time")
        var nextCodeTime: Long
    )
    constructor(error: Error?) : this() {
        this.error = error
    }
}