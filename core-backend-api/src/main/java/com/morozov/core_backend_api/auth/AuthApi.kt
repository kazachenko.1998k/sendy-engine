package com.morozov.core_backend_api.auth

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.auth.modelsRequest.AuthInitRequest
import com.morozov.core_backend_api.auth.modelsResponse.*
import io.reactivex.Scheduler

interface AuthApi {

    //REST
    fun init(
        request: AuthInitRequest?,
        callback: (response: InitResponse) -> Unit = {}
    )

    //REST
    fun getTime(
        userSession: String,
        request: AuthInitRequest?,
        callback: (response: String) -> Unit = {}
    )

    fun signAnonim(request: AnyRequest, callback: (response: AuthSignAnonymResponse) -> Unit = {})

    fun signPhone(request: AnyRequest, callback: (response: AuthSignPhoneResponse) -> Unit = {})

    fun checkCode(request: AnyRequest, callback: (response: AuthCheckCodeResponse) -> Unit = {})

    fun closeSessions(
        request: AnyRequest,
        callback: (response: AuthCloseSessionsResponse) -> Unit = {}
    )

    fun logOut(scheduler: Scheduler, callback: (response: AuthLogOutResponse) -> Unit = {})

    fun getSessions(request: AnyRequest, callback: (response: AuthGetSessionsResponse) -> Unit = {})

    fun setToken(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

}
