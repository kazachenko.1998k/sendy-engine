package com.morozov.core_backend_api.auth.modelsRequest


data class AuthSignPhoneRequest(

    //Уникальный идентификатор устройства (необходимо для склейки юзера с анонимным,
    // если он был на этом устройстве и к телефону не было привязано аккаунтов)
    var device_hash: String = "",

    //Телефон пользователя
    var phone: Long? = 0L,

    //Название модели устройства
    var model: String = "",

    //Версия операционной системы
    var os: Int = 0,

    //Ключ для отправки пушей клиенту
    var notification_token: String = "qwertyuiopoiuytrewertyuioiuytrewertyu",

    //тип платформы (см. поддерживаемые платформы)
    var platform: Int = 1
)