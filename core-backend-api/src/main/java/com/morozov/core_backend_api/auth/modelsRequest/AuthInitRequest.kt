package com.morozov.core_backend_api.auth.modelsRequest

import android.os.Build


data class AuthInitRequest(
    val device: String,
    val model: String =Build.BRAND + "_"+ Build.MODEL,
    val a: Int = 131072,
    val p: Int = 67436534,
    val g: Int = 3,
    val timestamp: Long = System.currentTimeMillis()
)
