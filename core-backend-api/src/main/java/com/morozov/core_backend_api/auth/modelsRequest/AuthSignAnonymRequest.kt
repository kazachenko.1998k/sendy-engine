package com.morozov.core_backend_api.auth.modelsRequest

data class AuthSignAnonymRequest(
    val device_hash: String,
    val model: String,
    val os: Int,
    val notification_token: String = "qwertyuiopoiuytrewertyuioiuytrewertyu",
    val platform: Int = 1
)