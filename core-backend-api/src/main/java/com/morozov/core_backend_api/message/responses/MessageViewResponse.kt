package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse

data class MessageViewResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MessageViewResponse.Data>() {
    data class Data(
        // массив идентификаторов сообщений которым успешно проставлен статус просмотрено
        var message_ids: MutableList<Long>? = null
    )
}