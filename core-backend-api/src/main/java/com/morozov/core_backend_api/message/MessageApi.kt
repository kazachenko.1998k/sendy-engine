package com.morozov.core_backend_api.message

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.message.responses.*

interface MessageApi {


    //message.send
    fun send(request: AnyRequest, callback: (response: MessageSendResponse) -> Unit = {})

    //message.replies
    fun replies(request: AnyRequest, callback: (response: MessageRepliesResponse) -> Unit = {})

    //message.delete
    fun delete(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //message.pin
    fun pin(request: AnyRequest, callback: (response: MessagePinResponse) -> Unit = {})

    //message.get
    fun get(request: AnyRequest, callback: (response: MessageGetResponse) -> Unit = {})

    //message.view
    fun view(request: AnyRequest, callback: (response: MessageViewResponse) -> Unit = {})

    //message.setReaction
    fun setReaction(
        request: AnyRequest,
        callback: (response: MessageSetReactionResponse) -> Unit = {}
    )
    //message.like
    fun like(
        request: AnyRequest,
        callback: (response: MessageLikeResponse) -> Unit = {}
    )
}