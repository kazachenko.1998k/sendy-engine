package com.morozov.core_backend_api.message.requests


data class MessageLikeRequest(
    //Идентификатор чата
    var chat_id: Long? = null,

    //Идентификатор сообщения к которому необходимо проставить реакцию
    var message_id: Long = -1

)