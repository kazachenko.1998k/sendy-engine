package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.messageReaction.MessageReaction

data class MessageSetReactionResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MessageSetReactionResponse.Data>() {
    data class Data(
        //Информация о сообщении
        var reaction: MessageReaction = MessageReaction()
    )
}