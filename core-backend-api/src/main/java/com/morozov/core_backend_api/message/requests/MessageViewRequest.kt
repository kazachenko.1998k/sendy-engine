package com.morozov.core_backend_api.message.requests


data class MessageViewRequest(
//Идентификатор чата
    var chat_id: Long? = null,

//Идентификаторы сообщения которые необходимо отметить как прочитанные
    var message_ids: MutableList<Long> = mutableListOf()
)