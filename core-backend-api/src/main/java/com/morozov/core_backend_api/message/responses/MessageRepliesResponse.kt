package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.UserMini

data class MessageRepliesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MessageRepliesResponse.Data>() {
    data class Data(
        var chat_id: Long? = null,
        var reply_to: Long? = null,
        var messages: MessageWithAmount? = null,
        var users: MutableList<UserMini>? = null
    )
}