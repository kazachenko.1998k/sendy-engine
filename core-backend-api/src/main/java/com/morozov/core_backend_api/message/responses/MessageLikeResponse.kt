package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.UserMini

data class MessageLikeResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MessageLikeResponse.Data>() {
    data class Data(
        var likes: Int? = null,
        var user_like: Boolean? = null
    )
}