package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.message.Message

data class MessageSendResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<Message>()