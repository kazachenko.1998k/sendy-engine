package com.morozov.core_backend_api.message.requests


data class MessagePinRequest(
//Идентификатор чата
    var chat_id: Long? = null,

//Идентификатор сообщения которое необходимо закрепить или открепить
    var message_id: Long = -1,

//0 - открепить, 1 - закрепить
    var action: Int = 1
)