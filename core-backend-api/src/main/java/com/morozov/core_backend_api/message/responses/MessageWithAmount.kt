package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.message.Message

data class MessageWithAmount(
    var amount: Int? = -1,
    var messages: MutableList<Message>? = mutableListOf()
)