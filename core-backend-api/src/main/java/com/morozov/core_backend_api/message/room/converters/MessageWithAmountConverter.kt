package com.morozov.core_backend_api.message.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.morozov.core_backend_api.message.responses.MessageWithAmount

class MessageWithAmountConverter {

    @TypeConverter
    fun fromMessageWithAmount(messageWithAmount: MessageWithAmount?): String? {
        return if (messageWithAmount == null) null
        else
            Gson().toJson(messageWithAmount)
    }

    @TypeConverter
    fun toMessageWithAmount(data: String?): MessageWithAmount? {
        return if (data == null) null
        else
            Gson().fromJson(data, MessageWithAmount::class.java)
    }
}