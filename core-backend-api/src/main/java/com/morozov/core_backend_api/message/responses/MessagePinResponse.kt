package com.morozov.core_backend_api.message.responses

import com.morozov.core_backend_api.AnyResponse

data class MessagePinResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MessagePinResponse.Data>() {
    data class Data(
        //Идентификатор чата
        var chat_id: Long? = null,

        //Идентификатор сообщения
        //ЕСЛИ НЕТ ЗАКРЕПЛЕННОГО СООБЩЕНИЯ, ТО pinned_message_id: null
        var pinned_message_id: Long? = null
    )
}