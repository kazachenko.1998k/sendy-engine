package com.morozov.core_backend_api.message

import androidx.annotation.IntDef
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.file.converters.FileConverter
import com.morozov.core_backend_api.location.Location
import com.morozov.core_backend_api.location.room.converters.LocationConverter
import com.morozov.core_backend_api.message.responses.MessageWithAmount
import com.morozov.core_backend_api.message.room.converters.MessageWithAmountConverter
import com.morozov.core_backend_api.messageReaction.MessageReaction
import com.morozov.core_backend_api.messageReaction.room.converters.MessageReactionConverter
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.room.converters.UserMiniConverter
import com.morozov.core_backend_api.utils.converters.StringListConverter
import com.squareup.moshi.Json


@Entity(tableName = "MessageTable")
@TypeConverters(
    StringListConverter::class,
    LocationConverter::class,
    FileConverter::class,
    MessageConverter::class,
    UserMiniConverter::class,
    MessageReactionConverter::class,
    MessageWithAmountConverter::class
)
data class Message(
    //Глобальный сквозной идентификатор сообщения
    @field:Json(name = "id")
    @PrimaryKey
    var id: Long = -1L,

    //Номер сообщения внутри чата (актуально только для чатов типа канал, фид). Если просмотры не учитываются - то 0.
    @field:Json(name = "internal_id")
    var internalId: Long? = null,


    @field:Json(name = "chat_id")
    var chatId: Long? = null,

    //Уровень ветки ответа если это был ответ
    @field:Json(name = "replies_amount")
    var repliesAmount: Long? = null,

    //Идентификатор репостнутого сообщения
    @field:Json(name = "repost_id")
    var repostId: Long? = null,

    //Идентификатор сообщения, на которое это ответ
    @field:Json(name = "reply_to")
    var replyTo: Long? = null,

    //Для чатов (кроме фида и галереи) если сообщение является ответом на другое сообщение - то будет полное сообщение тут
    @field:Json(name = "reply_to_message")
    var replyToMessage: Message? = null,

    //Уровень ветки ответа если это был ответ
    @field:Json(name = "reply_level")
    var replyLevel: Long? = null,

    //Главное сообщение ветки, на которое это сообщение является овтетом
    @field:Json(name = "reply_to_first_level")
    var replyToFirstLevel: Long? = null,

    //Сообщение первого уровня ответов, на которое текущее является ответом
    @field:Json(name = "reply_to_second_level")
    var replyToSecondLevel: Long? = null,

    //Идентификатор автора сообщения. При публикациях в чаты типа “Канал” идентификатор отправителя будет 0, если в настройках канала выбрано “не подписывать сообщения.
    @field:Json(name = "uid")
    var uid: Long? = null,

    //null или объект MINI_USER. Значение поля зависит от параметров метода, которым запрошено сообщение
    @field:Json(name = "owner")
    var owner: UserMini? = null,

    //Временная метка получения сервером сообщения от клиента
    @field:Json(name = "time_add")
    var timeAdd: Long? = null,

    //Показывать превью ссылки или нет
    @field:Json(name = "preview_link")
    var previewLink: Boolean? = false,

    //Уведомление показывать/пикать или нет
    @field:Json(name = "mute")
    var mute: Boolean? = false,

    //Было отредактировано сообщение или нет
    @field:Json(name = "modified")
    var modified: Boolean? = false,

    //Тип сообщения
    @Type
    @field:Json(name = "type")
    var type: Int? = 0,

    //Статус сообщения
    @MessageStatus
    @field:Json(name = "status")
    var status: Int? = null,

    //Текст сообщения
    @field:Json(name = "is_deleted")
    var isDeleted: Boolean? = false,

    //Текст сообщения
    @field:Json(name = "text")
    var text: String? = null,

    //Объект с информацией о прикрепленных файлах
    @field:Json(name = "files")
    var files: MutableList<FileModel>? = mutableListOf(),

    //Количество просмотров (для каналов и для фида). Если не канал и не фид - отсутствует.
    @field:Json(name = "views")
    var views: Int? = null,

    //Массив с доступными реакциями пользователя на сообщение
    @field:Json(name = "reactions")
    var reactions: MutableList<MessageReaction>? = mutableListOf(),

    //Если пользователь ставил реакцию - придет ее идентификатор
    @field:Json(name = "user_reaction")
    var userReaction: Long? = null,

    //Массив с использованными хэштегами в сообщении
    @field:Json(name = "hashtags")
    var hashtags: MutableList<String>? = mutableListOf(),

    //Количество лайков
    @field:Json(name = "likes")
    var likes: Int? = 0,

    //Лайкал юзер или нет
    @field:Json(name = "user_like")
    var userLike: Boolean? = false,

    //Массив с указание широты и долготы. Анализируется только при типе сообщения Геолокация
    @field:Json(name = "location")
    var location: Location? = null,

    //Идентификатор отправляемого контакта. Анализируется только если тип сообщения Контакт. Если не контакт - то отсутствует.
    @field:Json(name = "contact_id")
    var contactd: Long? = null,

    //Идентификатор отправляемого стикера. Анализируется только если тип сообщения Стикер. Если не стикер - то отсутствует.
    @field:Json(name = "sticker_id")
    var stickerId: Long? = null,

    //Длина звонка. Анализируется только если тип сообщения звонок. Если не звонок - то отстуствует.
    @field:Json(name = "call_length")
    var callLength: Int? = null,

    @field:Json(name = "replies")
    var replies: MessageWithAmount? = null,

    @field:Json(name = "time_add_ms")
    var timeAddMS: Long? = null
) {
    companion object {

        @IntDef(
            flag = false,
            value = [SENT,
                RECEIVED,
                WATCHED]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class MessageStatus

        //0-Отправлено
        //1-Получено
        //2-Просмотрено
        const val SENT = 0
        const val RECEIVED = 1
        const val WATCHED = 2

        @IntDef(
            flag = false,
            value = [STANDARD,
                CONTACT,
                GEOLOCATION,
                STICKER,
                SYSTEM,
                VOICE,
                CALL,
                ANONYMOUS,
                RESERVE,
                REMOTE
            ]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class Type

        //0-Стандартное сообщение
        //1-Контакт пользователя
        //2-Геолокация
        //3-Стикер
        //4-Системное сообщение  ("picture change", "Somebody subcribe to channel" and so on) [сообщения такого типа генерируются автоматически на сервере]
        //5-Голосовое сообщение
        //6-Звонок
        //7-Анонимное сообщение
        //8-резерв
        //9-Удаленное сообщение!
        const val STANDARD = 0
        const val CONTACT = 1
        const val GEOLOCATION = 2
        const val STICKER = 3
        const val SYSTEM = 4
        const val VOICE = 5
        const val CALL = 6
        const val ANONYMOUS = 7
        const val RESERVE = 8
        const val REMOTE = 9
    }


}
