package com.morozov.core_backend_api.message.requests


data class MessageDeleteRequest(
//Идентификатор чата
    var chat_id: Long? = null,

//Идентификаторы сообщений, которые необходимо удалить
    var message_ids: MutableList<Long> = mutableListOf(),

//true - удаление для всех в чате, если не указан или false - только для себя.
    var for_all: Boolean = false
)