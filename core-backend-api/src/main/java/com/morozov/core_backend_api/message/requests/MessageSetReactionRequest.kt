package com.morozov.core_backend_api.message.requests


data class MessageSetReactionRequest(
    //Идентификатор чата
    var chat_id: Long? = null,

    //Идентификатор сообщения к которому необходимо проставить реакцию
    var message_id: Long = -1,

    //Идентификатор реакции (если у сообщения только одна реакция, то отправлять ее идентификатор не обязательно)
    var reaction_id: Long = -1
)