package com.morozov.core_backend_api.message.requests


data class MessageGetRequest(
//Идентификатор чата
    var chat_id: Long? = null,

//массив id сообщений которые необходимо получить
    var message_ids: MutableList<Long> = mutableListOf(),

//Возвращать объект owner в MESSAGE или нет
    var get_owner: Boolean = false
)