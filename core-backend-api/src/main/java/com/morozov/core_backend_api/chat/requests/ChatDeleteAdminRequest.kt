package com.morozov.core_backend_api.chat.requests

data class ChatDeleteAdminRequest(
    //Идентификатор чата
    var chat_id: Long,

    //Идентификатор пользователя для удаления из списка админов
    var uid: Long
)