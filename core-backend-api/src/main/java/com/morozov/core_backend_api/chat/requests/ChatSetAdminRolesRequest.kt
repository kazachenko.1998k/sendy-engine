package com.morozov.core_backend_api.chat.requests

data class ChatSetAdminRolesRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

    //Идентификатор пользователя для которого ставятся права
    var uid: Long,

    //Число, которое отражает права исходя из битовой маски прав доступа
    var roles: Int
)