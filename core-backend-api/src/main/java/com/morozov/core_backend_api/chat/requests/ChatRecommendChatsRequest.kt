package com.morozov.core_backend_api.chat.requests

data class ChatRecommendChatsRequest(val empty: Boolean = true)