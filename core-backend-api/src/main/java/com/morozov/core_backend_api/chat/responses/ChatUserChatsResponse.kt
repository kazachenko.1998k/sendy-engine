package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.UserChat

data class ChatUserChatsResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatUserChatsResponse.Data>() {
    data class Data(
        var user_chats: MutableList<UserChat>? = mutableListOf(),
        var full_chats: MutableList<Chat>? = null
    )
}
