package com.morozov.core_backend_api.chat.requests

data class ChatAddMembersRequest(

    //Идентификатор чата
    var chat_id: Long,

    //Массив идентификаторов пользователей, которых надо добавить
    var users: MutableList<Long>

)