package com.morozov.core_backend_api.chat.requests

import com.morozov.core_backend_api.chat.Chat

data class ChatSetChatPrivacyRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

    //Приватность чата
    @Chat.Companion.PrivateType
    var privacy: Int,

    //Для чатов приватности 0, 3 - можно указать ник, который будет являться короткой ссылки
    var nick: String? = null
)