package com.morozov.core_backend_api.chat.requests

import com.morozov.core_backend_api.chat.Chat

data class ChatCreateNewRequest(
    //Тип чата (поддерживается 1,2,7)
    @Chat.Companion.Type
    var type: Int = 1,

    //Если создается чат как потомок от существующего чата
    var parent_chat_id: Long? = null,

    //Название чата
    var title: String = "",

    //Описание канала
    var description: String? = null,

    //Идентификатор загруженной аватарки чата
    var icon_file_id: String? = null,

    //При создании можно сразу добавить пользователей.
    //Но только из контакт листа - пользователи которые не в контакте у создателя - игнорируются
    var users: MutableList<Long>? = mutableListOf()
)