package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat

data class ChatGetByNickResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatGetByNickResponse.Data>() {
    data class Data(
        var chat: Chat = Chat()
    )
}
