package com.morozov.core_backend_api.chat

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "ChatMiniTable")
data class ChatMini(
    //Обязательное ,поле
    //Глобальный идентификатор чата
    @field:Json(name = "chat_id")
    @PrimaryKey
    var chatId: Long = -1,

    //Тип чата,
    @Chat.Companion.Type
    @field:Json(name = "type")
    var type: Int? = null,

    //Приватность чата
    @field:Json(name = "private")
    var private: Int? = null,

    //Название
    @field:Json(name = "title")
    var title: String? = null,

    //Ссылка на идентификатор файла
    @field:Json(name = "icon_file_id")
    var iconFileId: String? = null,

    //Количество подписчиков
    @field:Json(name = "members_amount")
    var membersAmount: Int? = null,

    //Ник чата
    @field:Json(name = "nick")
    var nick: String? = null,

    //Код для вступления
    @field:Json(name = "invite_hash")
    var inviteHash: String? = null,

    //Целое число представляющее из себя битовую маску прав пользователя. Если пользователь не в чате/не подписан - то будет 0.
    @field:Json(name = "user_role")
    var userRole: Int? = null,

    //Время, когда истекает отключение уведомлений в чате. Если null - то уведомления активны.
    @field:Json(name = "mute_time")
    var muteTime: Long? = null

)