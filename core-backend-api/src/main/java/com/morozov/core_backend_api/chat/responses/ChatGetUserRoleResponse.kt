package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse

data class ChatGetUserRoleResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatGetUserRoleResponse.Data>() {
    data class Data(
        var user_role: Int
    )
}
