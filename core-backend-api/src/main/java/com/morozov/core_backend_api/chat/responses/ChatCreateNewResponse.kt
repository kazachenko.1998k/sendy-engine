package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat

data class ChatCreateNewResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatCreateNewResponse.Data>() {
    data class Data(
        var chat: Chat = Chat()
    )
}
