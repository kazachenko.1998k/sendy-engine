package com.morozov.core_backend_api.chat.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.morozov.core_backend_api.message.Message

class MessageConverter {
   @TypeConverter
    fun fromMessage(message: Message?): String? {
        return if (message == null) null
        else
            Gson().toJson(message)
    }

    @TypeConverter
    fun toMessage(data: String?): Message? {
        return if (data == null) null
        else
            Gson().fromJson(data, Message::class.java)
    }
}