package com.morozov.core_backend_api.chat.requests

data class ChatDeleteRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long

)