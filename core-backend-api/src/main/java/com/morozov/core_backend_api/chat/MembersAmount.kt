package com.morozov.core_backend_api.chat

data class MembersAmount(val all: Int = 0, val block: Int = 0, val admin: Int = 0)