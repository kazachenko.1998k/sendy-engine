package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.MembersAmount

data class ChatGetMembersAmountResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatGetMembersAmountResponse.Data>() {
    data class Data(
        var members_amount: MembersAmount = MembersAmount()
    )
}
