package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse

data class ChatMuteResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatMuteResponse.Data>() {
    data class Data(
        // Идентификаторы сообщений в порядке убывания или возврастания по дате в зависимости от параметра direction
        var chat_ids: MutableList<Long> = mutableListOf(),
        var error_chat_ids: MutableList<Long> = mutableListOf()
    )
}
