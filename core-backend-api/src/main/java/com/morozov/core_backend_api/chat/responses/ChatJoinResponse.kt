package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message

data class ChatJoinResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatJoinResponse.Data>() {
    data class Data(
        var chat: Chat = Chat(),
        // Идентификаторы сообщений в порядке убывания или возврастания по дате в зависимости от параметра direction
        var message_ids: MutableList<Long> = mutableListOf(),
        var full_message: MutableList<Message> = mutableListOf()
    )
}
