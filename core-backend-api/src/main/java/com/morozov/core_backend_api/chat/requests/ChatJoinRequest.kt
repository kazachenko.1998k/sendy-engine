package com.morozov.core_backend_api.chat.requests

data class ChatJoinRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

//Хеш для приглашения (при вступлении в приватные группы)
    var invite_hash: String? = null,

    //Если не задано - то в соответствии с общим конфигом
    //Если больше 0, то вернется последние N указанных сообщений
    var message_amount: Int? = null,

    //Если не задано -то false
    //Если указано true - то будут возвращены не только идентификаторы сообщений, но и отдельно полные сообщения.
    //Если false - то full_message возвращены не будут.
    var full_message: Boolean = false
)