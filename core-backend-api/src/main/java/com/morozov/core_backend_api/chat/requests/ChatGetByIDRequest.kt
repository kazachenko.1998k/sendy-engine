package com.morozov.core_backend_api.chat.requests

data class ChatGetByIDRequest(
    val chat_ids: MutableList<Long>
)