package com.morozov.core_backend_api.chat.requests

data class ChatGetByHashRequest(
    val chat_invite_hash: String
)