package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse

data class ChatUpdateInviteHashResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatUpdateInviteHashResponse.Data>() {
    data class Data(
        var invite_hash: String = ""
    )
}
