package com.morozov.core_backend_api.chat

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "UserChatTable")
data class UserChat(

    //Обязательное ,поле
    //Глобальный идентификатор чата
    @field:Json(name = "chat_id")
    @PrimaryKey
    var chatId: Long = -1,

    //Время, когда истекает отключение уведомлений в чате. Если 0 - то уведомления активны.
    @field:Json(name = "mute_time")
    var muteTime: Long? = null,

    //Какое сообщение было последним просмотренным в чате
    @field:Json(name = "last_view_message_id")
    var lastViewMessageId: Long? = null,

    //Время последнего сообщения в чате
    @field:Json(name = "last_message_time")
    var lastMessageTime: Long? = null,

    //ID последнего сообщения в чате
    @field:Json(name = "last_message_id")
    var lastMessageId: Long? = null,

    var isRecommended: Boolean = false
)