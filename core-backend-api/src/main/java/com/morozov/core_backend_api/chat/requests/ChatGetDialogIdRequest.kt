package com.morozov.core_backend_api.chat.requests

data class ChatGetDialogIdRequest(
    val uid: Long
)