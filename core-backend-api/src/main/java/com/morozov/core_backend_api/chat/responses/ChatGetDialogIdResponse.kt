package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat

data class ChatGetDialogIdResponse(private val dateCreate: Long = System.currentTimeMillis()):
    AnyResponse<ChatGetDialogIdResponse.Data>() {
    data class Data(val chat: Chat)
}