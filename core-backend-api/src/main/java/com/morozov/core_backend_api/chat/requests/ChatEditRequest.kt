package com.morozov.core_backend_api.chat.requests


data class ChatEditRequest(
    var chat_id: Long,

    //Название чата
    var title: String?,

    //Описание канала
    var description: String?,

    //Идентификатор загруженной аватарки чата
    var icon_file_id: String?,

    //Если создается чат как потомок от существующего чата
    var parent_chat_id: Long? = null

)