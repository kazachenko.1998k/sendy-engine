package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message

data class ChatDeleteResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<Boolean>()
