package com.morozov.core_backend_api.chat.requests

data class ChatClearHistoryRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

    //false - удаление только для меня, true - удаление для всех участников
    var for_all: Boolean = false
)