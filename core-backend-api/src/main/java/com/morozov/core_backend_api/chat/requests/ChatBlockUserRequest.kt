package com.morozov.core_backend_api.chat.requests

data class ChatBlockUserRequest(
    //Идентификатор чата
    var chat_id: Long,

    //Идентификатор пользователя для которого ставятся права
    var uid: Long,

    //true - заблокировать
    //false - разблокировать
    var action: Boolean

)