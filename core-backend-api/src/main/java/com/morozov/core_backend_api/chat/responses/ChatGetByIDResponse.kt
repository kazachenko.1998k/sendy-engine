package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat

data class ChatGetByIDResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatGetByIDResponse.Data>() {
    data class Data(
        var chats: MutableList<Chat> = mutableListOf(),
        var error_chat_ids: MutableList<Long> = mutableListOf()
    )
}
