package com.morozov.core_backend_api.chat.requests

data class ChatGetByNickRequest(
    val chat_nick: String
)