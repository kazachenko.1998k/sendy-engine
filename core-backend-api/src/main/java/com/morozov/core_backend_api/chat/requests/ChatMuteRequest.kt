package com.morozov.core_backend_api.chat.requests

data class ChatMuteRequest(

    //Идентификатор чата
    //Обязательное поле
    var chat_ids: MutableList<Long>,

    //На сколько по времени отключить уведомления (в секундах)
    var mute_time: Long? = null

)