package com.morozov.core_backend_api.chat.requests

import com.morozov.core_backend_api.chat.Chat

data class ChatUserActionRequest(

    //Идентификатор чата
    var chat_id: Long,

    //Тип действия пользователя
    @Chat.Companion.EventAction
    var chat_action: Int

)