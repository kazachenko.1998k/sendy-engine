package com.morozov.core_backend_api.chat

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.chat.responses.*

interface ChatApi {

    //Получение информации о чате
    fun getByID(request: AnyRequest, callback: (response: ChatGetByIDResponse) -> Unit = {})

    fun getByIDWithDB(request: AnyRequest, callback: (response: ChatGetByIDResponse) -> Unit = {})

    //Получение чата, если существует, по uid пользователя
    fun getDialogID(request: AnyRequest, callback: (response: ChatGetDialogIdResponse) -> Unit = {})

    fun getByNick(
        request: AnyRequest,
        callback: (response: ChatGetByNickResponse) -> Unit = {}
    )

    fun getByHash(
        request: AnyRequest,
        callback: (response: ChatGetByHashResponse) -> Unit = {}
    )

    //Получение сообщений в чате
    fun getMessages(
        request: AnyRequest,
        callback: (response: ChatGetMessagesResponse) -> Unit = {}
    )

    //Приоединение/выход из чата
    fun join(request: AnyRequest, callback: (response: ChatJoinResponse) -> Unit = {})

    fun leave(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun blockUser(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun delete(request: AnyRequest, callback: (response: ChatDeleteResponse) -> Unit = {})

    fun deleteAdmin(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun setGroupRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun setAdminRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun clearHistory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Создание и редактирование чата
    fun createNew(
        request: AnyRequest,
        callback: (response: ChatCreateNewResponse) -> Unit = {}
    )

    fun setChatPrivacy(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun updateInviteHash(
        request: AnyRequest,
        callback: (response: ChatUpdateInviteHashResponse) -> Unit = {}
    )

    fun edit(request: AnyRequest, callback: (response: ChatCreateNewResponse) -> Unit = {})

    fun setUserRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun userChats(
        request: AnyRequest,
        callback: (response: ChatUserChatsResponse) -> Unit = {}
    )

    fun getMembers(
        request: AnyRequest,
        callback: (response: ChatGetMembersResponse) -> Unit = {}
    )

    fun getMembersAmount(
        request: AnyRequest,
        callback: (response: ChatGetMembersAmountResponse) -> Unit = {}
    )

    fun mute(request: AnyRequest, callback: (response: ChatMuteResponse) -> Unit = {})

    fun userAction(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun addFavorite(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun addMembers(request: AnyRequest, callback: (response: ChatAddMembersResponse) -> Unit = {})

    fun getUserRole(request: AnyRequest, callback: (response: ChatGetUserRoleResponse) -> Unit = {})
}