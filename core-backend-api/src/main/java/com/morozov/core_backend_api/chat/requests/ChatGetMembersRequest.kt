package com.morozov.core_backend_api.chat.requests

data class ChatGetMembersRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

    //Если не задано - то значение по умолчанию members_chat_get_amount
    //Если задано - то сколько задано но не более members_chat_get_amount_max
    val amount: Int? = null,

    //Если не задано - то по умолчанию 0 страница
    val page: Int? = null,

    //Последний полученный идентификатор пользователя
    val last_uid: Long? = null,

    //По умолчанию false.
    //Если задано true, то будут возвращены все участники чата, которые в нем есть и есть среди контактов пользователя
    val all_contacts: Boolean? = null,

    //0 - подписчики(По дефолту)
    //1 - заблокированные
    //2 - администраторы
    val members_filter: Int? = null
) {
    companion object {
        const val FILTER_ALL = 0
        const val FILTER_BLACK_LIST = 1
        const val FILTER_ADMIN = 2
    }
}