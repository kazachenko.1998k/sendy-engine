package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.UserMini

data class ChatGetMembersResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatGetMembersResponse.Data>() {
    data class Data(
        var users: MutableList<UserMini>? = null
    )
}
