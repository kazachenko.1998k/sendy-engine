package com.morozov.core_backend_api.chat

import androidx.annotation.IntDef
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.morozov.core_backend_api.location.Location
import com.morozov.core_backend_api.location.room.converters.LocationConverter
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.utils.converters.StringListConverter
import com.squareup.moshi.Json
import kotlin.math.pow

@Entity(tableName = "ChatTable")
@TypeConverters(StringListConverter::class, LocationConverter::class, MessageConverter::class)
data class Chat(

    //Обязательное ,поле
    //Глобальный идентификатор чата
    @PrimaryKey
    var id: Long = -1,

    //Обязательное поле
    //Тип чата
    @Type
    var type: Int = SYSTEM,

    //Обязательное поле
    //Уровень приватности чата
    @PrivateType
    var private: Int = CLOSE_CHAT,

    //Создатель чата (не указывается для каналов - приходит 0) М.б. еще есть кейсы, когда надо скрывать.
    @field:Json(name = "creator_uid")
    var creatorUid: Long? = null,

    //Если это персональный диалог - то тут указан идентификатор с кем диалог
    @field:Json(name = "dialog_uid")
    var dialogUid: Long? = null,

    //Указывается в случае, если чат создан от имени другого чата
    @field:Json(name = "parent_chat_id")
    var parentChatId: Long? = null,

    //Обязательное поле
    //Название чата
    @field:Json(name = "title")
    var title: String? = "",

    //Краткое описание чата
    @field:Json(name = "description")
    var description: String? = null,

    //Полноценное описание чата
    @field:Json(name = "full_description")
    var fullDescription: String? = null,

    //Указатель на FILE_ID с аватаркой, 0 если аватарки нет
    @field:Json(name = "icon_file_id")
    var iconFileId: String? = null,

    //Указатель на FILE_ID с фоном, 0 если фона нет
    @field:Json(name = "background_file_id")
    var backgroundFileId: String? = null,

    //Короткий адрес чата, может отсутствовать для приватных чатов
    @field:Json(name = "nick")
    var nick: String? = null,

    //Обязательное поле
    //Ссылка хеш для вступления в чат
    @field:Json(name = "invite_hash")
    var inviteHash: String? = "",

    //Идентификатор чата, который привязан к данному чату (например группа для обсуждения постов в канале)
    @field:Json(name = "discuss_chat_id")
    var discussChatId: Long? = null,

    //Идентификатор пака стикеров, привязанных к данному чату
    @field:Json(name = "sticker_pack_id")
    var stickerPackId: Long? = null,

    //Объект с широтой и долготой, адресом чата
    @field:Json(name = "location")
    var location: Location? = null,

    //Идентификатор основной категории чата
    @field:Json(name = "main_category")
    var mainCategory: Int? = null,

    //Массив с идентификаторами категорий чата
    @field:Json(name = "categories")
    var categories: MutableList<String> = mutableListOf(),

    //массив со списком хэштегов
    @field:Json(name = "hashtags")
    var hashtags: MutableList<String> = mutableListOf(),

    //Массив с объектами типа LINK
    @field:Json(name = "links")
    var links: MutableList<String> = mutableListOf(),

    //Временная метка последнего сообщения в чате
    @field:Json(name = "last_message_time")
    var lastMessageTime: Long? = null,

    //ID последнего сообщения в чате
    @field:Json(name = "last_message_id")
    var lastMessageId: Long? = null,

    //последнее сообщение в чате целиком
    @field:Json(name = "last_message")
    var lastMessage: Message? = null,

    //Общее количество сообщений в чате (в случае фида считается общее количество с учетом комментариев)
    @field:Json(name = "message_amount")
    var messageAmount: Long? = null,

    //Если нет закрепленных сообщений - то 0, если есть - то id закрепленного сообщения.
    @field:Json(name = "pinned_message_id")
    var pinnedMessageId: Long? = null,

    //Количество участников чата
    @field:Json(name = "members_amount")
    var membersAmount: Int? = null,

    //Целое число представляющее из себя битовую маску прав пользователя. Если пользователь не в чате/не подписан - то будет 0.
    @field:Json(name = "user_role")
    var userRole: Int? = null,

    //Целое число представляющее из себя битовую маску прав пользователя по умлчанию.
    @field:Json(name = "default_user_role")
    var defaultUserRole: Int? = null,

    //Время, когда истекает отключение уведомлений в чате. Если null - то уведомления активны.
    @field:Json(name = "mute_time")
    var muteTime: Long? = null,

    @field:Json(name = "edit_time_ms")
    var editTimeMS: Long? = null,

    @Ignore
    var unreadMessage: Int = 0,

    @Ignore
    var lastOnline: Long? = null
) {

    fun toMini(): ChatMini {
        val userMini = ChatMini()
        userMini.chatId = id
        userMini.type = type
        userMini.private = private
        userMini.title = title
        userMini.iconFileId = iconFileId
        userMini.membersAmount = membersAmount
        userMini.nick = nick
        userMini.inviteHash = inviteHash
        return userMini
    }


    fun getAccessLevels(): MutableList<AccessLevel> = getAccessLevels(userRole)

    fun getDefaultAccessLevels(): MutableList<AccessLevel> = getAccessLevels(defaultUserRole)


    companion object {

        fun setAccessLevels(list: MutableList<AccessLevel>?): Int {
            var result = 1
            if (!list.isNullOrEmpty()) {
                result = 0
                list.forEach {
                    result += (2.0.pow(it.level.toDouble())).toInt()
                }
            }
            return result
        }

        fun getAccessLevels(value: Int?): MutableList<AccessLevel> {
            val result = mutableListOf<AccessLevel>()
            var currentRole: Int? = value ?: return result
            var identification = 0
            while (currentRole!! > 0) {
                if (currentRole % 2 == 1) {
                    result.add(AccessLevel.values().find { it.level == identification }!!)
                }
                currentRole /= 2
                identification++
            }
            return result
        }

        val validDataPermissionForGroupEdit = listOf(
            Companion.AccessLevel.SEND_MESSAGE,
            Companion.AccessLevel.SEND_MEDIA,
            Companion.AccessLevel.FORWARD_MESSAGE,
            Companion.AccessLevel.PREVIEW_LINK,
            Companion.AccessLevel.ADD_USERS,
            Companion.AccessLevel.VIEW_SUBSCRIBERS_USERS,
            Companion.AccessLevel.PINNED_MESSAGE
        )

        val defaultAdminRoles = listOf(
            Companion.AccessLevel.READ_MESSAGE,
            Companion.AccessLevel.SEND_MESSAGE,
            Companion.AccessLevel.SEND_MEDIA,
            Companion.AccessLevel.PREVIEW_LINK,
            Companion.AccessLevel.FORWARD_MESSAGE,
            Companion.AccessLevel.ADMIN_CHAT
        )

        @Target(AnnotationTarget.PROPERTY, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
        @IntDef(
            flag = false,
            value = [PERSONAL_DIALOG,
                GROUP_CHAT,
                CHANNEL,
                PERSONAL_FEED,
                PERSONAL_GALLERY,
                FAVORITES,
                SYSTEM,
                PRIVATE_CHANNELS,
                SECRET_CHAT]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class Type

        //0 - Personal Dialog - диалог между двумя пользователями
        //1 - Group Chat - беседа, неограниченное количество пользователей
        //2 - Channel (аналогично групповому чату, но писать могут только администраторы)
        //3 - Personal feed - персональная лента пользователя
        //4 - Personal Gallery (for user gallery) - фото/видео галерея пользователя
        //5 - Favorites (saved messages) избранное пользователя (чат с одним участником владельцем канала)
        //6 - System (welcome bot) - системный чат для прямой отправки пользователю каких-либо сообщений
        //7 - Private Channels  - события - вынесены в отдельный тип, чтобы показывать в определенном формате визуальном и возможность их поиска по ним.
        //8 - Secret chat - зашифрованный peer-to-peer чат между пользователями
        const val PERSONAL_DIALOG = 0
        const val GROUP_CHAT = 1
        const val CHANNEL = 2
        const val PERSONAL_FEED = 3
        const val PERSONAL_GALLERY = 4
        const val FAVORITES = 5
        const val SYSTEM = 6
        const val PRIVATE_CHANNELS = 7
        const val SECRET_CHAT = 8

        @IntDef(
            flag = false,
            value = [PUBLIC_CHAT,
                PRIVATE_CHAT,
                FEED_OR_USER_GALLERY,
                CLOSE_CHAT,
                PERSONAL_CHAT]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class PrivateType

        //1-Публичный чат, доступный по короткой ссылке с настроенным адресом, вступить может любой
        //2-Закрытый публичный чат с короткой ссылкой, вступить в него нельзя - только ручное добавление администраторами
        //3-Приватный чат (отсутствует в поиске, добавить юзеров может пользователь с соответствующими правами, можно пригласить пользователя по прямой ссылке [хеш ссылка])
        //4-Фид/галерея юзера (вся логика доступа строится на основе доступности фида аккаунта и статуса подписок между пользователями)
        //5-Персональные чаты (в поиске их нет, вступить по ссылке/по инвайту нельзя) или секретные чаты
        const val PUBLIC_CHAT = 1
        const val PRIVATE_CHAT = 3
        const val FEED_OR_USER_GALLERY = 4
        const val CLOSE_CHAT = 2
        const val PERSONAL_CHAT = 5

        enum class AccessLevel(val level: Int) {
            // БИТ СВОДОБЕН
            EMPTY(0),

            //Чтение сообщений
            READ_MESSAGE(1),

            //Отправка сообщений
            SEND_MESSAGE(2),

            //Отправка медиа
            SEND_MEDIA(3),

            //Пересылка сообщений
            FORWARD_MESSAGE(4),

            //Предпросмотр ссылок
            PREVIEW_LINK(5),

            //Добавление участников
            ADD_USERS(6),

            //Закрепление сообщений
            PINNED_MESSAGE(7),

            //Редактировать профиль чата
            EDIT_PROFILE(8),

            //Редактирование чужих сообщений
            EDIT_OTHER_MESSAGE(9),

            //Удаление чужих сообщений
            DELETE_OTHER_MESSAGE(10),

            //Назначение администраторов
            SET_ADMIN(11),

            //Редактирование приватности
            EDIT_PRIVACY(12),

            //Блокирование/разблокирование пользователей
            BLOCK_UNBLOCK_USER(13),

            //Просмотр участников группы/канала
            VIEW_SUBSCRIBERS_USERS(14),

            //Удаление чата
            DELETE_CHAT(15),

            //Администратор чата
            ADMIN_CHAT(16)
        }

        @IntDef(
            flag = false,
            value = [NOTHING, WRITE_TEXT, SEND_PHOTO,
                SEND_VIDEO, SEND_DATA, RECORD_AUDIO]
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class EventAction

        //0 - Не делает никаких действий (читает например)
        //1 - Пользователь печатает
        //2 - Отправляет фото
        //3 - Отправляет видео
        //4 - Отправляет файл
        //5 - Записывает аудио сообщение
        const val NOTHING = 0
        const val WRITE_TEXT = 1
        const val SEND_PHOTO = 2
        const val SEND_VIDEO = 3
        const val SEND_DATA = 4
        const val RECORD_AUDIO = 5
    }

}