package com.morozov.core_backend_api.chat.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.message.Message

data class ChatAddMembersResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<ChatAddMembersResponse.Data>() {
    data class Data(
        // Идентификаторы сообщений в порядке убывания или возврастания по дате в зависимости от параметра direction
        var add: MutableList<Long> = mutableListOf(),
        var fail: MutableList<Long> = mutableListOf()
    )
}
