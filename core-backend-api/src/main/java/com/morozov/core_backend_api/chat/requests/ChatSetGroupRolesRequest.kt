package com.morozov.core_backend_api.chat.requests

data class ChatSetGroupRolesRequest(
    //Идентификатор чата
    var chat_id: Long,

    //Число, которое отражает права исходя из битовой маски прав доступа
    var roles: Int
)