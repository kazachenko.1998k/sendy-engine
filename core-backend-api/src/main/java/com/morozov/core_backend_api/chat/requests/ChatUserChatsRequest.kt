package com.morozov.core_backend_api.chat.requests

import com.morozov.core_backend_api.chat.Chat

data class ChatUserChatsRequest(
    //Типы чатов, которые хотим получить. Если не указано - то 0,1,2,5,6,7
    var type: MutableList<@Chat.Companion.Type Int>? = null
)