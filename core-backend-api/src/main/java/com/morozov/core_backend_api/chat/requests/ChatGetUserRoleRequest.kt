package com.morozov.core_backend_api.chat.requests

data class ChatGetUserRoleRequest(
    //Идентификатор чата
    //Обязательное поле
    var chat_id: Long,

    //Идентификатор пользователя
    //Обязательное поле
    val uid: Long
)