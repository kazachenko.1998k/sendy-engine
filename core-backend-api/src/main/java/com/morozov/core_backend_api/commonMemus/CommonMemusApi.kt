package com.morozov.core_backend_api.commonMemus

import com.morozov.core_backend_api.commonMemus.modelResponse.CommonGetUpdatesResponse
import io.reactivex.Single
import retrofit2.http.GET

interface CommonMemusApi {

    @GET("content.getUpdates")
    fun getUpdates(): Single<CommonGetUpdatesResponse?>
}