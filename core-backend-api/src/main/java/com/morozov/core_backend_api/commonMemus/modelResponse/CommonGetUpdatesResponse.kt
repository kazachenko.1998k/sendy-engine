package com.morozov.core_backend_api.commonMemus.modelResponse

data class CommonGetUpdatesResponse (val timeLastChatSeen: String, val timeLastActivitySeen: String?)