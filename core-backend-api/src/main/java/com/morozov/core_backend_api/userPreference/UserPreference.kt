package com.morozov.core_backend_api.userPreference

import com.squareup.moshi.Json

data class UserPreference(

    //Идентификатор настроек доступа
    @field:Json(name = "pref_code")
    var code: String? = null,

//      Установленное значение настроек
    var value: Int = -1
)
