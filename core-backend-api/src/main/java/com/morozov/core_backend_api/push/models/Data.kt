package com.morozov.core_backend_api.push.models

data class Data(
    var chat_id: Long? = null,
    var user_id: Long? = null,
    var msg_id: Long? = null,
    var status: Int? = null,
    var time_add: Long? = null,
    var chat_title: String? = null,
    var name: String? = null,
    var nick: String? = null,
    var icon_id: String? = null,
    var uniq_id: String? = null,
    var type: String = ""
)