package com.morozov.core_backend_api.push

import com.morozov.core_backend_api.push.models.Data
import com.morozov.core_backend_api.push.models.Notification
data class PushResponse(
    var notification: Notification,
    var data: Data?
){
    companion object{
        const val TYPE_DC_UPDATE = "DC_UPDATE"
        const val TYPE_SESSION_REVOKE = "SESSION_REVOKE"
        const val TYPE_DIALOG_MESSAGE = "DIALOG_MESSAGE"
        const val TYPE_MESSAGE_ADD = "MESSAGE_ADD"
        const val TYPE_MESSAGE_DELETED = "MESSAGE_DELETED"
        const val TYPE_MESSAGE_EDIT = "MESSAGE_EDIT"
        const val TYPE_MESSAGE_PIN = "MESSAGE_PIN"
        const val TYPE_MESSAGE_UNPIN = "MESSAGE_UNPIN"
        const val TYPE_MESSAGE_STATUS = "MESSAGE_STATUS"
        const val TYPE_CHAT_CREATED = "CHAT_CREATED"
        const val TYPE_CHAT_ADD = "CHAT_ADD"
        const val TYPE_CHAT_REMOVE = "CHAT_REMOVE"
        const val TYPE_CHAT_EDIT = "CHAT_EDIT"
        const val TYPE_CHAT_DELETE = "CHAT_DELETE"
        const val TYPE_CONTACT_JOINED = "CONTACT_JOINED"
        const val TYPE_SUBSCRIBE_USER = "SUBSCRIBE_USER"
        const val TYPE_FEED_NEW_POST = "FEED_NEW_POST"
        const val TYPE_PHONE_CALL_REQUEST = "PHONE_CALL_REQUEST"
        const val TYPE_PHONE_CALL_MISSED = "PHONE_CALL_MISSED"
    }
}