package com.morozov.core_backend_api.push.models

data class Notification(var title: String, var body: String?, var icon_id: String?)