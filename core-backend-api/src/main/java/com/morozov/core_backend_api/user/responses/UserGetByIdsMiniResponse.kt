package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.UserMini

data class UserGetByIdsMiniResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetByIdsMiniResponse.Data>() {
    data class Data(
        var users: MutableList<UserMini> = mutableListOf()
    )
}