package com.morozov.core_backend_api.user.requests

data class UserDeleteContactRequest(

    //Идентификатор пользователя, которого удаляем из контактов
    var uid: Long

)