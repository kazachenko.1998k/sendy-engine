package com.morozov.core_backend_api.user.requests

import com.morozov.core_backend_api.phoneContact.PhoneContact

data class UserImportContactsRequest(

    //Массив с объектами контактами пользователя с телефона.
    var contacts: MutableList<PhoneContact>

)