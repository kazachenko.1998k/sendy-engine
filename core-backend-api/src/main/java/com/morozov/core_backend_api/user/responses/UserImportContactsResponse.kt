package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.contact.Contact

data class UserImportContactsResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserImportContactsResponse.Data>() {
    data class Data(
        var contacts: MutableList<Contact> = mutableListOf()
    )
}