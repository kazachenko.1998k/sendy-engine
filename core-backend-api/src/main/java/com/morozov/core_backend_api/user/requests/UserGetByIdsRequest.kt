package com.morozov.core_backend_api.user.requests

data class UserGetByIdsRequest(
    //Ник пользователя
    val uids: MutableList<Long>,

    //если указан и true - будет возвращена полная информация по профилям USER.
    //Если не указан или false - то USER_MINI
    val full: Boolean? = null
)