package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User

data class UserGetSubscribersResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetSubscribersResponse.Data>() {
    data class Data(
        var amount: Int = -1,
        var users: MutableList<User> = mutableListOf()
    )
}