package com.morozov.core_backend_api.user.requests

data class UserBlockRequest(
    //Ник пользователя
    val uid: Long,

    //true - block, false - unblock
    val action: Boolean
)