package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User

data class UserGetByNickResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetByNickResponse.Data>() {
    data class Data(
        val user: User
    )
}