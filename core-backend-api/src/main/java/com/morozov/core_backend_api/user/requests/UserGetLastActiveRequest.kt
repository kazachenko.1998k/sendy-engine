package com.morozov.core_backend_api.user.requests

data class UserGetLastActiveRequest(
    //Ник пользователя
    val uid: Long

)