package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User

data class UserGetByIdsResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetByIdsResponse.Data>() {
    data class Data(
        var users: MutableList<User> = mutableListOf()
    )
}