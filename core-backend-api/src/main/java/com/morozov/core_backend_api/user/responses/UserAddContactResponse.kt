package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.contact.Contact

data class UserAddContactResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserAddContactResponse.Data>() {
    data class Data(
        var contact: Contact? = null
    )
}