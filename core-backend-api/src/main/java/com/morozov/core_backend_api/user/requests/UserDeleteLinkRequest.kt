package com.morozov.core_backend_api.user.requests

data class UserDeleteLinkRequest(
    //Идентификатор типа ссылки которую необходимо удалить
    var link_type_id: Int
)