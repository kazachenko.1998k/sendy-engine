package com.morozov.core_backend_api.user.requests

data class UserSubscribeRequest(
    //Идентификатор пользователя
    val uid: Long,

    //true - подписка, false - отписка
    val action: Boolean
)