package com.morozov.core_backend_api.user.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.morozov.core_backend_api.user.UserMini

class UserMiniConverter {

    @TypeConverter
    fun fromUserMini(userMini: UserMini?): String? {
        return if (userMini == null) null
        else
            Gson().toJson(userMini)
    }

    @TypeConverter
    fun toUserMini(data: String?): UserMini? {
        return if (data == null) null
        else
            Gson().fromJson(data, UserMini::class.java)
    }
}