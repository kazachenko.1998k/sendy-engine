package com.morozov.core_backend_api.user

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.morozov.core_backend_api.link.room.converters.LinkListConverter
import com.morozov.core_backend_api.utils.converters.IntListConverter
import com.morozov.core_backend_api.utils.converters.StringListConverter
import com.squareup.moshi.Json

@Entity(tableName = "UserMiniTable")
data class UserMini(

    //Идентификатор пользователя в рамках приложения
    @PrimaryKey
    var uid: Long = -1,

    //Указатель на файл с аватаркой
    @field:Json(name = "avatar_file_id")
    var avatarFileId: String? = null,

    //Ник пользователя
    var nick: String = "anonym_00074",

    //Имя пользователя (м.б. пустое)
    @field:Json(name = "first_name")
    var firstName: String? = "Anonimus",

    //Фамилия пользователя (м.б. пустое)
    @field:Json(name = "second_name")
    var secondName: String? = null,

    //Признак что пользователь удален
    @field:Json(name = "is_deleted")
    var isDeleted: Boolean = false,

    //Подтвержден аккаунт пользователя или нет.
    // Если не подтвержден - то круг его действий сильно ограничен - он может просматривать контент,
    // подписываться на каналы, реагировать на сообщения), но не может писать.
    @field:Json(name = "is_confirmed")
    var isConfirmed: Boolean = false,

    //Время последней активности юзера (если не скрыто настройками приватности)
    @field:Json(name = "last_active")
    var lastActive: Long = -1,

    //Подписан тот кто запросил на этого пользователя или нет. Для запросах о себе - true.
    @field:Json(name = "is_subscribe")
    var isSubscribe: Boolean = true
)