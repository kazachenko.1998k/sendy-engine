package com.morozov.core_backend_api.user.requests

import com.morozov.core_backend_api.phoneContact.PhoneContact

data class UserAddContactRequest(

    //Контакт пользователя с телефона
    var contact: PhoneContact

)