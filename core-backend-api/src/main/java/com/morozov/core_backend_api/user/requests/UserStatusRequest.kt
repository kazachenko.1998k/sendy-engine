package com.morozov.core_backend_api.user.requests

data class UserStatusRequest(
    //Чат в который уходит предупреждение о изменение статуса
    val chat_id: Long,

    //
    val dialog_uid: Long,

    //Статус пользователя
    val status: Int
)