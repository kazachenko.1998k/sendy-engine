package com.morozov.core_backend_api.user.requests

import com.morozov.core_backend_api.userPreference.UserPreference

data class UserSavePrefrenceRequest(
    //Необходимо отправить только обновленные настройки приватности пользователя.
    val preference: MutableList<UserPreference>
)