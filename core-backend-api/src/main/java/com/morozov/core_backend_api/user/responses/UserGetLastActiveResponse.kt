package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User

data class UserGetLastActiveResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetLastActiveResponse.Data>() {
    data class Data(
        var last_active: Long
    )
}