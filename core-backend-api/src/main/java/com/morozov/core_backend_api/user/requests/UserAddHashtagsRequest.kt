package com.morozov.core_backend_api.user.requests

data class UserAddHashtagsRequest(
    //Массив с хэштегами которые необходимо сохранить.
    //Если надо удалить все хештеги - на входе должен быть пустой массив.
    //Все хештеги, которые были раньше, но не пришли в запросе - удаляются.
    var hashtags: MutableList<String>
)