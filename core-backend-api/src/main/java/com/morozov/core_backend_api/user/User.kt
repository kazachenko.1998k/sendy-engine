package com.morozov.core_backend_api.user

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.morozov.core_backend_api.link.Link
import com.morozov.core_backend_api.link.room.converters.LinkListConverter
import com.morozov.core_backend_api.utils.converters.IntListConverter
import com.morozov.core_backend_api.utils.converters.StringListConverter
import com.squareup.moshi.Json

@Entity(tableName = "UserTable")
@TypeConverters(StringListConverter::class, IntListConverter::class, LinkListConverter::class)
data class User(

    //Идентификатор пользователя в рамках приложения
    @PrimaryKey
    var uid: Long = -1,

    //Подтвержден аккаунт пользователя или нет.
    // Если не подтвержден - то круг его действий сильно ограничен - он может просматривать контент,
    // подписываться на каналы, реагировать на сообщения), но не может писать.
    @field:Json(name = "is_confirmed")
    var isConfirmed: Boolean = false,

    //Указатель на выбранный пользователем язык
    @field:Json(name = "lang_id")
    var langId: Int? = null,

    @field:Json(name = "time_reg")
    var timeReg: Long = -1,

    //Указатель на идентификатор страны.
    @field:Json(name = "country_id")
    var countryId: Int? = null,

    //Указатель на идентификатор города
    @field:Json(name = "city_id")
    var cityId: Int? = null,

    //Указатель на файл с аватаркой
    @field:Json(name = "avatar_file_id")
    var avatarFileId: String? = null,

    //Указатель на файл с фоном
    @field:Json(name = "background_file_id")
    var backgroundFileId: String? = null,

    //Имя пользователя (м.б. пустое)
    @field:Json(name = "first_name")
    var firstName: String? = "Anonimus",

    //Фамилия пользователя (м.б. пустое)
    @field:Json(name = "second_name")
    var secondName: String? = null,

    //Биография пользователя
    @field:Json(name = "short_bio")
    var shortBio: String? = null,

    //Официальный аккаунт или нет (галочка в профиле)
    @field:Json(name = "is_official")
    var isOfficial: Boolean = false,

    //Признак что пользователь удален
    @field:Json(name = "is_deleted")
    var isDeleted: Boolean = false,

    //Ник пользователя
    var nick: String = "anonym_00074",

    //Телефон пользователя (если не скрыт настройками приватности)
    var phone: String? = null,

    //Статус пользователя (вывод в профиле) - аналог ватсаппа
    var status: String? = null,

    //Пользователь забанен (не может делать никаких действий? Юзер стори? )
    @field:Json(name = "is_ban")
    var isBan: Boolean? = false,

    //Когда истечет блокировка
    @field:Json(name = "ban_expired")
    var banExpired: Long? = null,

    //Время последней активности юзера (если не скрыто настройками приватности)
    @field:Json(name = "last_active")
    var lastActive: Long = -1,

    //Указатель на идентификатор чата, который является лентой пользователя. Если не скрыт настройками приватности - иначе 0
    @field:Json(name = "feed_chat_id")
    var feedChatId: Long? = null,

    //Количество подписчиков у пользователя
    var subscribers: Int? = 0,

    //Указатель на идентификатор чата, который является галерей пользователя Если не скрыта настройками приватности - иначе 0.
    @field:Json(name = "gallery_chat_id")
    var galleryChatId: Long? = null,

    //Указатель на идентификатор чата, который является избранным для пользователя (Если информация запрошена самим пользователем о себе). Если другим - придет 0.
    @field:Json(name = "favorites_chat_id")
    var favoritesChatId: Long? = null,

    //Подписан тот кто запросил на этого пользователя или нет. Для запросах о себе - true.
    @field:Json(name = "is_subscribe")
    var isSubscribe: Boolean = true,

    //Заблокирован ли этот пользователь у того, кто запросил. Для запросах о себе - false
    @field:Json(name = "is_blocked")
    var isBlocked: Boolean = false,

    //Массив с идентификаторами категорий пользователя
    var categories: MutableList<Int>? = mutableListOf(),

    //Массив с хештегами о пользователе
    var hashtags: MutableList<String>? = mutableListOf(),

    //Массив с избранными хештегами пользователя
    @field:Json(name = "favorites_hashtags")
    var favoritesHashtags: MutableList<String>? = mutableListOf(),

    //Массив с объектами типа LINK
    var links: MutableList<Link>? = mutableListOf()
) {
    fun toMini(): UserMini {
        val userMini = UserMini()
        userMini.uid = uid
        userMini.avatarFileId = avatarFileId
        userMini.nick = nick
        userMini.firstName = firstName
        userMini.secondName = secondName
        userMini.isDeleted = isDeleted
        userMini.isConfirmed = isConfirmed
        userMini.lastActive = lastActive
        userMini.isSubscribe = isSubscribe
        return userMini
    }
}