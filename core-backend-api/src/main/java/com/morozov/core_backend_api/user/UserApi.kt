package com.morozov.core_backend_api.user

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.user.responses.*
import io.reactivex.Scheduler

interface UserApi {

    //Редактирование профиля пользователя
    fun saveProfile(request: AnyRequest, callback: (response: UserSaveProfileResponse) -> Unit = {})

    //Получение пользователя по нику
    fun getByNick(request: AnyRequest, callback: (response: UserGetByNickResponse) -> Unit = {})

    //Получение пользователей по идентификаторам
    fun getByIds(request: AnyRequest, callback: (response: UserGetByIdsResponse) -> Unit = {})

    //Получение пользователей по идентификаторам
    fun getLastActive(request: AnyRequest, callback: (response: UserGetLastActiveResponse) -> Unit = {})

    fun getByIdFromDB(request: Long, callback: (response: User) -> Unit = {})

    fun getByIdsMini(
        request: AnyRequest,
        callback: (response: UserGetByIdsMiniResponse) -> Unit = {}
    )

    //Блокировка/разблокировка пользователя
    fun block(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Удаление аккаунта
    fun delete(callback: (response: EmptyResponse) -> Unit = {})

    //Сохранение настроек пользователя
    fun savePreference(
        request: AnyRequest,
        callback: (response: UserSavePreferenceResponse) -> Unit = {}
    )

    //Сохранение категорий пользователя
    fun setCategory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

//  Методы работы с пользовательскими хештегами

    //Сохранение всех хештегов пользователя
    fun saveHashtags(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Добавление хештега
    fun addHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Удаление хештегов
    fun deleteHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

//  Методы работы с ссылками в профиле пользователя

    fun setLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun deleteLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

//  Методы работы с подписками пользователя

    //Подписка/отписка на пользователя
    fun subscribe(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Заявка на подписку на пользователя
    fun subscribeQueue(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Принятие/отклонение заявки на подписку на пользователя
    fun subscribeConfirm(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    //Получение списка подписчиков/заявок пользователя
    fun getSubscribers(
        request: AnyRequest,
        callback: (response: UserGetSubscribersResponse) -> Unit = {}
    )

    fun getSubscribersMini(
        request: AnyRequest,
        callback: (response: UserGetSubscribersMiniResponse) -> Unit = {}
    )

//  Методы работы с пользовательскими контактами

    fun importContacts(
        request: AnyRequest,
        callback: (response: UserImportContactsResponse) -> Unit = {}
    )

    fun addContact(request: AnyRequest, callback: (response: UserAddContactResponse) -> Unit = {})

    fun deleteContact(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun getContacts(callback: (response: UserGetContactsResponse) -> Unit = {})

    fun sendStatus(request: AnyRequest, callback: (response: EmptyResponse) -> Unit = {})

    fun subscribeStatus(scheduler: Scheduler, callback: (response: UserStatusResponse) -> Unit = {})

}

