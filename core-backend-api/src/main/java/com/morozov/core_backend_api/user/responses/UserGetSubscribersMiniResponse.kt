package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.UserMini

data class UserGetSubscribersMiniResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserGetSubscribersMiniResponse.Data>() {
    data class Data(
        var amount: Int = -1,
        var users: MutableList<UserMini> = mutableListOf()
    )
}