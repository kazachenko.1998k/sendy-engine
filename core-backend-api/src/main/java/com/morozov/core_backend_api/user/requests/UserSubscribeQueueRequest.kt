package com.morozov.core_backend_api.user.requests

data class UserSubscribeQueueRequest(
    //Идентификатор пользователя
    val uid: Long,

    //true - подписка, false - отписка
    val action: Boolean
)