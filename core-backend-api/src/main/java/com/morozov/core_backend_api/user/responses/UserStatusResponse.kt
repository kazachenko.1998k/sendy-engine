package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse

data class UserStatusResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserStatusResponse.Data>() {
    data class Data(
        //Например CHAT_STATUS
        var type: String?,

        //К какому чату относится
        var chat_id: Long,

        //Кто делает действие
        var uid: Long,

        //Статус
        var status: Int
    )
}