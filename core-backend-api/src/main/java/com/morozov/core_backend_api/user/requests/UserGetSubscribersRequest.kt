package com.morozov.core_backend_api.user.requests

data class UserGetSubscribersRequest(

    //Количество в ответе
    var amount: Int? = null,

    //стартовое смещение
    var start: Int? = null,

    //Какая страница - по дефолту 0
    var page: Int? = null,

    //true - показывать на кого подписан я
    //false - кто подписан на меня
    //По дефолту false
    var my: Boolean? = null,

    //по дефолту false
    //true вернет не MINI_USER, а USER
    var full: Boolean? = null,

    //ДЛЯ ДОБАВЛЕНИЯ СОРТИРОВКИ НУЖЕН ФИДБЕК ОТ КЛИЕНТОВ ЧТО НУЖНО!
    var sort: Int? = null

)