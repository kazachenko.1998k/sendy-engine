package com.morozov.core_backend_api.user.requests

data class UserGetByNickRequest(
    //Ник пользователя
    val nick: String,

    //true - полная информация
    //false - минимальная
    val full: Boolean
)