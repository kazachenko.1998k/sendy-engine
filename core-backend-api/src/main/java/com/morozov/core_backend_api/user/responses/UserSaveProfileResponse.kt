package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.user.User

data class UserSaveProfileResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<User>() {
}