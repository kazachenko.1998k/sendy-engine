package com.morozov.core_backend_api.user.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.userPreference.UserPreference

data class UserSavePreferenceResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<UserSavePreferenceResponse.Data>() {
    data class Data(
        var preference: MutableList<UserPreference> = mutableListOf()
    )
}