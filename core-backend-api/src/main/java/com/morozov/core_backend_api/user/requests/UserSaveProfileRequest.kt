package com.morozov.core_backend_api.user.requests

data class UserSaveProfileRequest (

    // Код языка пользователя
    var lang_id: Int? = null,

    // Код страны пользователя
    var country_id: Int? = null,

    // Код города пользователя
    var city_id: Int? = null,

    // Идентификатор файла-аватарки
    var avatar_file_id: String? = null,

    // Идентификатор файла-фона
    var background_file_id: String? = null,

    // Ник пользователя
    var nick: String? = null,

    // Имя пользователя
    var first_name: String? = null,

    // Фамилия пользователя
    var second_name: String? = null,

    // Биография пользователя
    var short_bio: String? = null
)
