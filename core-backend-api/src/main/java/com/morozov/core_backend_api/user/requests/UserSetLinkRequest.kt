package com.morozov.core_backend_api.user.requests

data class UserSetLinkRequest(
    //Идентификатор типа ссылки
    var link_type_id: Int,

    //Урл, который необходимо поставить
    var link: String
)