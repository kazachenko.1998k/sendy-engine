package com.morozov.core_backend_api

import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.activityMemus.ActivityMemusApi
import com.morozov.core_backend_api.auth.AuthApi
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chatsMemus.ChatsMemusApi
import com.morozov.core_backend_api.common.CommonApi
import com.morozov.core_backend_api.commonMemus.CommonMemusApi
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.morozov.core_backend_api.file.FileApi
import com.morozov.core_backend_api.info.InfoApi
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.profileMemus.ProfileMemusApi
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.search.SearchApi
import com.morozov.core_backend_api.update.UpdateApi
import com.morozov.core_backend_api.user.UserApi
import io.reactivex.Flowable

interface FeatureBackendApi {

    /**
     * CommonApi
     */
    var connectionIsOpen: Boolean
    var pushListener: Flowable<PushResponse>
    fun authApi(): AuthApi

    /**
     * User api
     * */
    fun userApi(): UserApi

    /**
     * File api
     * */
    fun fileApi(): FileApi

    /**
     * MemUs Api
     */

    fun feedMemusApi(): FeedMemusApi

    fun activityMemusApi(): ActivityMemusApi

    fun profileMemusApi(): ProfileMemusApi

    fun commonMemusApi(): CommonMemusApi

    fun chatMemusApi(): ChatsMemusApi


    /**
     * New sendy Api
     */

    fun chatApi(): ChatApi

    fun searchApi(): SearchApi

    fun messageApi(): MessageApi

    fun feedApi(): FeedApi

    fun infoApi(): InfoApi

    fun commonApi(): CommonApi

    fun updateApi(): UpdateApi
}