package com.morozov.core_backend_api.messageReaction

import com.squareup.moshi.Json

data class MessageReaction(

    //Идентификатор реакции
    @field:Json(name = "rid")
    var rid: Long? = null,

    //Название реакции [“like”, “я за“, “буду участвовать”]
    @field:Json(name = "rname")
    var rname: String? = null,

    //Количество пользователей, выбравших реакцию
    @field:Json(name = "amount")
    var amount: Int? = null
)