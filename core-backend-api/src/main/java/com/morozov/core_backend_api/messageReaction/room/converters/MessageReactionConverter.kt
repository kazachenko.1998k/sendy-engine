package com.morozov.core_backend_api.messageReaction.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.messageReaction.MessageReaction

class MessageReactionConverter {

    @TypeConverter
    fun fromMessageReaction(messageReaction: MutableList<MessageReaction>?): String? {
        return if (messageReaction == null) null
        else
            Gson().toJson(messageReaction)
    }

    @TypeConverter
    fun toMessageReaction(data: String?): MutableList<MessageReaction>? {
        return if (data == null) null
        else {
            val turnsType = object : TypeToken<MutableList<MessageReaction>>() {}.type
            Gson().fromJson(data, turnsType)
        }
    }
}