package com.morozov.core_backend_api.info

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.info.response.*

interface InfoApi {

    fun getCountriesCities(
        request: AnyRequest,
        callback: (response: InfoGetCountriesCitiesResponse) -> Unit = {}
    )

    fun getCategories(callback: (response: InfoGetCategoriesResponse) -> Unit = {})

    fun getPreferences(callback: (response: InfoGetPreferencesResponse) -> Unit = {})

    fun linkTypes(callback: (response: InfoLinkTypesResponse) -> Unit = {})

    fun getConfig(callback: (response: InfoGetConfigResponse) -> Unit = {})

    fun time(
        callback: (response: InfoTimeResponse) -> Unit = {}
    )

}