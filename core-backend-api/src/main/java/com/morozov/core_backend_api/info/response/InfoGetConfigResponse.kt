package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.config.Config

data class InfoGetConfigResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoGetConfigResponse.Data>() {
    data class Data(
        var config: Config? = null
    )
}