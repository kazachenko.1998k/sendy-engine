package com.morozov.core_backend_api.info.request

data class InfoGetCountriesCitiesRequest(
    //Идентификатор конкретной страны
    //Если не указано - вернет все страны
    var country: Int? = null,

    //Надо возвращать города или нет. Если не указан - то считается false
    var city: Boolean? = null
)