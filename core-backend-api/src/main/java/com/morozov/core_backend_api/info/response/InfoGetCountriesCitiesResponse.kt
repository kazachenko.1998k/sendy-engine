package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.city.City
import com.morozov.core_backend_api.country.Country

data class InfoGetCountriesCitiesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoGetCountriesCitiesResponse.Data>() {
    data class Data(
        var countries: MutableList<Country>? = mutableListOf(),
        var cities: MutableList<City>? = mutableListOf()
    )
}