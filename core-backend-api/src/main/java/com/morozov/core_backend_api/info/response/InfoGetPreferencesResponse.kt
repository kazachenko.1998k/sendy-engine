package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.preference.Preference
import com.morozov.core_backend_api.preferenceGroup.PreferenceGroup

data class InfoGetPreferencesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoGetPreferencesResponse.Data>() {
    data class Data(
        var pref_group: MutableList<PreferenceGroup>? = mutableListOf(),
        var pref: MutableList<Preference>? = mutableListOf()
    )
}