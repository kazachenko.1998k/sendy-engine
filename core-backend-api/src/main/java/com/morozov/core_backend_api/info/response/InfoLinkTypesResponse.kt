package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.linksType.LinksType

data class InfoLinkTypesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoLinkTypesResponse.Data>() {
    data class Data(
        var links_type: MutableList<LinksType>? = mutableListOf()
    )
}