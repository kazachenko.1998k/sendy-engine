package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.category.Category

data class InfoGetCategoriesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoGetCategoriesResponse.Data>() {
    data class Data(
        var categories: MutableList<Category>? = mutableListOf()
    )
}