package com.morozov.core_backend_api.info.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.config.Config

data class InfoTimeResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<InfoTimeResponse.Data>() {
    data class Data(
        var time: Long? = null
    )
}