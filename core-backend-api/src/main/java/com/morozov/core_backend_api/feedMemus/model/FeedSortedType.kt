package com.morozov.core_backend_api.feedMemus.model

enum class FeedSortedType {
    DATE,
    LIKE,
    COMMENT
}