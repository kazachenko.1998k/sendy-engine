package com.morozov.core_backend_api.feedMemus.modelResponse

import com.morozov.core_backend_api.feedMemus.model.FeedMyTagModel

data class GetFeedMyTagsResponse(val tags: List<FeedMyTagModel>)