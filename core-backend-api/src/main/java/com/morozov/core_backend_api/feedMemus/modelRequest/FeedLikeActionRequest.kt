package com.morozov.core_backend_api.feedMemus.modelRequest

data class FeedLikeActionRequest(var postId: String, var isLike: Boolean)