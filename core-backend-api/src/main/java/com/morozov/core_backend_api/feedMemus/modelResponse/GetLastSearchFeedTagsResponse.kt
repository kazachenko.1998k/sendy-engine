package com.morozov.core_backend_api.feedMemus.modelResponse

import com.morozov.core_backend_api.feedMemus.model.FeedTagModel

data class GetLastSearchFeedTagsResponse(val tags: List<FeedTagModel>)