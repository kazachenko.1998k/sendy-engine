package com.morozov.core_backend_api.feedMemus.model

data class SizeModel (var width: Int, var height: Int)