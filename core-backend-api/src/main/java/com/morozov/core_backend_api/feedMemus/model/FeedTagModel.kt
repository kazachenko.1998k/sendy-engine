package com.morozov.core_backend_api.feedMemus.model

data class FeedTagModel(val name: String, val countPosts: Int)