package com.morozov.core_backend_api.feedMemus.modelResponse

import com.morozov.core_backend_api.feedMemus.model.PostModel

data class GetBestMemesFeedResponse (val posts: List<PostModel>, val startItemIdInNextPage: String?)