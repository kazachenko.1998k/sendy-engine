package com.morozov.core_backend_api.feedMemus.model

enum class FileType {
    IMAGE,
    AUDIO,
    GIF,
    VIDEO_MP4,
    VIDEO_HLS,
    FILE_TXT,
    ARCHIVE,
    RUN_FILE,
    OTHER
}
