package com.morozov.core_backend_api.feedMemus.modelResponse

import com.morozov.core_backend_api.feedMemus.model.PostModel

data class FeedLikeActionResponse (val result: Boolean, val post: PostModel?)