package com.morozov.core_backend_api.feedMemus.model

enum class FeedCollectionModel {
    BEST_MEMES,
    COLLECTION,
    FAVOURITE,
    SUBSCRIPTION
}