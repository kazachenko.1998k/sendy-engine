package com.morozov.core_backend_api.feedMemus


import com.morozov.core_backend_api.feedMemus.modelRequest.*
import com.morozov.core_backend_api.feedMemus.modelResponse.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface FeedMemusApi {

    @POST("content.getSearchTags")
    fun getSearchTags(@Body searchFeedTagsRequest: GetSearchFeedTagsRequest): Single<GetSearchFeedTagsResponse?>

    @POST("content.getMyTags")
    fun getMyTags(): Single<GetFeedMyTagsResponse?>

    @POST("content.getLastSearchTags")
    fun getLastSearchTags(): Single<GetLastSearchFeedTagsResponse?>

    @POST("content.bestMemes")
    fun getBestMemesFeed(@Body bestMemesOfFeedRequest: GetBestMemesFeedRequest): Single<GetBestMemesFeedResponse?>

    @POST("content.collective")
    fun getCollectiveFeed(@Body bestMemesOfFeedRequest: GetCollectiveFeedRequest): Single<GetCollectiveFeedResponse?>

    @POST("content.subscription")
    fun getSubscriptionsFeed(@Body bestMemesOfFeedRequest: GetSubscriptionsFeedRequest): Single<GetSubscriptionsFeedResponse?>

    @POST("content.likePost")
    fun likePost(@Body likeActionRequest: FeedLikeActionRequest): Single<FeedLikeActionResponse?>

}