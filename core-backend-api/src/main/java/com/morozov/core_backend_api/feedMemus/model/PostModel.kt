package com.morozov.core_backend_api.feedMemus.model


data class PostModel(
    val id: String,
    val memType: FileType,
    var urls: List<String>,
    var date: String,
    var likeCount: Int?,
    var commentCount: Int?,
    var isLike: Boolean,
    var isBookmark: Boolean,
    val owner: OwnerModel,
    val size: SizeModel
)
