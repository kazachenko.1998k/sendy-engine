package com.morozov.core_backend_api.feedMemus.modelRequest

import com.morozov.core_backend_api.feedMemus.model.FeedSortedType

data class GetSubscriptionsFeedRequest(
    var itemsOnPage: Int,
    var shiftItem: Int,
    var compilationId: String
)