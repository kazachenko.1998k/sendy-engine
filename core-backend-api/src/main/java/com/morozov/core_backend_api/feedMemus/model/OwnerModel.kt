package com.morozov.core_backend_api.feedMemus.model

data class OwnerModel(
    var avatar: String,
    val userId: String?,
    var userName: String?,
    var isSubscribed: Boolean
)