package com.morozov.core_backend_api.feedMemus.modelRequest

import com.morozov.core_backend_api.feedMemus.model.FeedSortedType

data class GetCollectiveFeedRequest(
    var itemsOnPage: Int,
    var shiftItem: Int,
    var compilationId: String,
    var tags: List<String>,
    var sortedType: FeedSortedType
)