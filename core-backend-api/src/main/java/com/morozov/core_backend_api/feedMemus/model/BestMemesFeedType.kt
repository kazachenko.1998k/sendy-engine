package com.morozov.core_backend_api.feedMemus.model

enum class BestMemesFeedType {
    DAY,
    WEEK,
    MONTH,
    YEAR
}