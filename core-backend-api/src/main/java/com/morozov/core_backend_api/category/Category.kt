package com.morozov.core_backend_api.category

import com.squareup.moshi.Json

data class Category(
    //Идентификатор категории
    @field:Json(name = "cat_id")
    var catId: Int = -1,

    //Ссылка на родительскую категорию, если родительской категории нет - то 0 (или null - как лучше нужен фидбек от клиента или можно вообще не передавать)
    @field:Json(name = "parent_cat_id")
    var parentCatId: Int? = -1,

    //Название категории
    @field:Json(name = "title")
    var title: String? = null,

    //Описание категории
    @field:Json(name = "description")
    var description: String? = null,

    //короткая ссылка на адрес категории
    @field:Json(name = "nick")
    var nick: String? = null,

    //Указатель на файл иконку группы
    @field:Json(name = "avatar_file_id")
    var avatarFileId: String? = null,

    //Указатель на файл фон категории
    @field:Json(name = "background_file_id")
    var backgroundFileId: String? = null,

    //Показывается в интерфейсах или нет
    @field:Json(name = "is_show")
    var isShow: Boolean? = null
)