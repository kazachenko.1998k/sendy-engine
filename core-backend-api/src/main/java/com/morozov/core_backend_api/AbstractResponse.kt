package com.morozov.core_backend_api

import com.morozov.core_backend_api.errors.Error

abstract class AbstractResponse(
    var result: Boolean = true,
    var rid: Int = -1,
    var method: String = "",
    var error: Error? = null
)