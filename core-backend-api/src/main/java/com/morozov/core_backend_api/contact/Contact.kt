package com.morozov.core_backend_api.contact

import com.morozov.core_backend_api.user.UserMini
import com.squareup.moshi.Json

data class Contact(

    //Телефон контакта (если не закрыт настройками приватности контакта, если закрыт - пустая строка)
    var phone: String = "",

    //Контакт
    @field:Json(name = "user_mini")
    var userMini: UserMini? = null

)