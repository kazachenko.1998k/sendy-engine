package com.morozov.core_backend_api

import com.morozov.core_backend_api.auth.modelsResponse.*
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.responses.*
import com.morozov.core_backend_api.common.response.CommonCheckFreeNickResponse
import com.morozov.core_backend_api.common.response.CommonGetHashtagsResponse
import com.morozov.core_backend_api.feed.responses.FeedMessageByIdResponse
import com.morozov.core_backend_api.feed.responses.FeedMessagesResponse
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse
import com.morozov.core_backend_api.file.response.FileUploadResponse
import com.morozov.core_backend_api.info.response.*
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.responses.*
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.search.responses.SearchGlobalResponse
import com.morozov.core_backend_api.update.responses.UpdateChatsResponse
import com.morozov.core_backend_api.update.responses.UpdateMessagesResponse
import com.morozov.core_backend_api.user.responses.*
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable

interface AllCallsApiWS {

    @Send
    fun sendMessage(data: AnyRequest?): Boolean

    /**
     * CommonApi
     */


    @Receive
    fun observeWebSocketError(): Flowable<EmptyResponse>

    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>

    /**
     * AUTH API
     * */

    @Receive
    fun observeAuthSignAnonymResponse(): Flowable<AuthSignAnonymResponse>

    @Receive
    fun observeAuthSignPhoneResponse(): Flowable<AuthSignPhoneResponse>

    @Receive
    fun observeAuthCheckCodeResponse(): Flowable<AuthCheckCodeResponse>

    @Receive
    fun observeAuthCloseSessionsResponse(): Flowable<AuthCloseSessionsResponse>

    @Receive
    fun observeAuthGetSessionsResponse(): Flowable<AuthGetSessionsResponse>

    @Receive
    fun observeAuthLogOutResponse(): Flowable<AuthLogOutResponse>


    /**
     * USER_API
     * */
    // Изменение информации профиля
    @Receive
    fun observeUserSaveProfile(): Flowable<UserSaveProfileResponse>

    // Получение информации профиля по нику
    @Receive
    fun observeUserGetByNick(): Flowable<UserGetByNickResponse>

    @Receive
    fun observeUserGetByIds(): Flowable<UserGetByIdsResponse>

    @Receive
    fun observeUserGetLastActive(): Flowable<UserGetLastActiveResponse>

    @Receive
    fun observeUserGetByIdsMini(): Flowable<UserGetByIdsMiniResponse>

    @Receive
    fun observeUserBlock(): Flowable<EmptyResponse>

    @Receive
    fun observeUserDelete(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSavePreference(): Flowable<UserSavePreferenceResponse>

    @Receive
    fun observeUserSetCategory(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSaveHashtags(): Flowable<EmptyResponse>

    @Receive
    fun observeUserAddHashtag(): Flowable<EmptyResponse>

    @Receive
    fun observeUserDeleteHashtag(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSetLink(): Flowable<EmptyResponse>

    @Receive
    fun observeUserDeleteLink(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSubscribe(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSubscribeQueue(): Flowable<EmptyResponse>

    @Receive
    fun observeUserSubscribeConfirm(): Flowable<EmptyResponse>

    @Receive
    fun observeUserGetSubscribers(): Flowable<UserGetSubscribersResponse>

    @Receive
    fun observeUserGetSubscribersMini(): Flowable<UserGetSubscribersMiniResponse>

    @Receive
    fun observeUserImportContacts(): Flowable<UserImportContactsResponse>

    @Receive
    fun observeUserAddContact(): Flowable<UserAddContactResponse>

    @Receive
    fun observeUserDeleteContact(): Flowable<EmptyResponse>

    @Receive
    fun observeUserGetContacts(): Flowable<UserGetContactsResponse>


    @Receive
    fun observeUserSendStatus(): Flowable<EmptyResponse>


    @Receive
    fun observeUserSubscribeStatus(): Flowable<UserStatusResponse>


    /**
     * FILE_API
     * */
    // Получение информации профиля по нику
    @Receive
    fun observeUploadFile(): Flowable<FileUploadResponse>

    /**
     * CHAT_API
     */

    //Получение информации о чате
    @Receive
    fun observeChatGetByID(): Flowable<ChatGetByIDResponse>

    //Получение чата, если существует, по uid пользователя
    @Receive
    fun observeChatGetDialogID(): Flowable<ChatGetDialogIdResponse>

    @Receive
    fun observeChatGetByNick(): Flowable<ChatGetByNickResponse>

    @Receive
    fun observeChatGetByHash(): Flowable<ChatGetByHashResponse>

    //Получение сообщений в чате
    @Receive
    fun observeChatGetMessages(): Flowable<ChatGetMessagesResponse>

    //Приоединение/выход из чата
    @Receive
    fun observeChatJoin(): Flowable<ChatJoinResponse>

    @Receive
    fun observeChatLeave(): Flowable<EmptyResponse>

    @Receive
    fun observeChatDelete(): Flowable<ChatDeleteResponse>

    @Receive
    fun observeChatDeleteAdmin(): Flowable<EmptyResponse>

    @Receive
    fun observeChatBlockUser(): Flowable<EmptyResponse>

    @Receive
    fun observeChatSetAdminRoles(): Flowable<EmptyResponse>

    @Receive
    fun observeChatSetGroupRoles(): Flowable<EmptyResponse>

    @Receive
    fun observeChatClearHistory(): Flowable<EmptyResponse>

    //Создание и редактирование чата
    @Receive
    fun observeChatCreateNew(): Flowable<ChatCreateNewResponse>

    @Receive
    fun observeChatSetChatPrivacy(): Flowable<EmptyResponse>

    @Receive
    fun observeChatUpdateInviteHash(): Flowable<ChatUpdateInviteHashResponse>

    @Receive
    fun observeChatEdit(): Flowable<ChatCreateNewResponse>

    @Receive
    fun observeChatSetUserRoles(): Flowable<EmptyResponse>

    @Receive
    fun observeChatUserChats(): Flowable<ChatUserChatsResponse>

    @Receive
    fun observeChatGetMembers(): Flowable<ChatGetMembersResponse>

    @Receive
    fun observeChatGetMembersAmount(): Flowable<ChatGetMembersAmountResponse>

    @Receive
    fun observeChatMute(): Flowable<ChatMuteResponse>

    @Receive
    fun observeChatUserAction(): Flowable<EmptyResponse>

    @Receive
    fun observeChatAddFavorite(): Flowable<EmptyResponse>

    @Receive
    fun observeChatAddMembersFavorite(): Flowable<ChatAddMembersResponse>

    @Receive
    fun observeChatGetUserRole(): Flowable<ChatGetUserRoleResponse>

    //TODO НЕ ОПИСАНО - ДЛЯ СЛЕДУЮЩЕГО СПРИНТА
//    chat.setHashtags
//    chat.addHashtag
//    chat.deleteHashtag
//    chat.setCategories
//    chat.setLocation
//    chat.setTime
//    chat.setLink
//    chat.deleteLink
//

    /**
     * InfoApi
     */


    @Receive
    fun observeInfoGetCountriesCities(): Flowable<InfoGetCountriesCitiesResponse>

    @Receive
    fun observeInfoGetCategories(): Flowable<InfoGetCategoriesResponse>

    @Receive
    fun observeInfoGetPreferences(): Flowable<InfoGetPreferencesResponse>

    @Receive
    fun observeInfoLinkTypes(): Flowable<InfoLinkTypesResponse>

    @Receive
    fun observeInfoGetConfig(): Flowable<InfoGetConfigResponse>

    @Receive
    fun observeInfoTime(): Flowable<InfoTimeResponse>


    /**
     * SearchApi
     */

    @Receive
    fun observeSearchGlobal(): Flowable<SearchGlobalResponse>

    /**
     * FeedApi
     */

    @Receive
    fun observeGetFeedMessages(): Flowable<FeedMessagesResponse>

    @Receive
    fun observeGetFeedMessagesById(): Flowable<FeedMessageByIdResponse>

    @Receive
    fun observeGetFeedReplies(): Flowable<FeedRepliesResponse>


    /**
     * MessageApi
     */

    @Receive
    //message.send
    fun observeMessageSend(): Flowable<MessageSendResponse>

    @Receive
    //message.replies
    fun observeMessageReplies(): Flowable<MessageRepliesResponse>

    @Receive
    //message.delete
    fun observeMessageDelete(): Flowable<EmptyResponse>

    @Receive
    //message.pin
    fun observeMessagePin(): Flowable<MessagePinResponse>

    @Receive
    //message.get
    fun observeMessageGet(): Flowable<MessageGetResponse>

    @Receive
    //message.view
    fun observeMessageView(): Flowable<MessageViewResponse>

    @Receive
    //message.setReaction
    fun observeMessageSetReaction(): Flowable<MessageSetReactionResponse>

    @Receive
    fun observeCommonCheckFreeNick(): Flowable<CommonCheckFreeNickResponse>

    @Receive
    fun observeCommonGetHashtags(): Flowable<CommonGetHashtagsResponse>

    @Receive
    fun observeLike(): Flowable<MessageLikeResponse>

    /**
     * PUSH Api
     */

    @Receive
    fun observePush(): Flowable<PushResponse>


    /**
     * Update Api
     */
    //Получение информации о новых сообщениях в чатах, участником которых является пользователь
    @Receive
    fun observeUpdateMessages(): Flowable<UpdateMessagesResponse>

    //Получение информации о новых чатах, участником которых является пользователь или редактировании чатов
    @Receive
    fun observeUpdateChats(): Flowable<UpdateChatsResponse>

    //Получение информации о новых чатах, участником которых является пользователь или редактировании чатов
    @Receive
    fun observeUpdateLastChats(): Flowable<UpdateChatsResponse>

}

/**
 * MemUs Api
 */

//    , FeedMemusApi
//
//    , ActivityMemusApi
//
//    , ProfileMemusApi
//
//    , CommonMemusApi
//
//    , ChatsMemusApi
//
//    /**
//     * Sendy Api
//     */
//
//    , UserApi
//
//    , ChannelApi
//
//    , FeedApi
//
//    , GroupApi
//
//    , DatingApi
//
//    , SecretApi
//
//    , UserSessionsApi
//
//    , LocationApi
//
//    , PersonApi
//
//    , NotificationApi
//
//    , MuteApi
//
//    , ComplaintApi
//
//    , SearchApi
//
//    , InfoApi
//
//    , LinkApi
//
//    , FileApi
//
//    , HashTagApi
