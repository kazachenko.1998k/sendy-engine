package com.morozov.core_backend_api.common

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.common.response.CommonCheckFreeNickResponse
import com.morozov.core_backend_api.common.response.CommonGetHashtagsResponse

interface CommonApi {
    fun checkFreeNick(
        request: AnyRequest,
        callback: (response: CommonCheckFreeNickResponse) -> Unit = {}
    )

    fun getHashtags(
        request: AnyRequest,
        callback: (response: CommonGetHashtagsResponse) -> Unit = {}
    )
}