package com.morozov.core_backend_api.common.request

data class CommonGetHashtagsRequest(
    //Проверяемый хэштег - не менее пяти символов
    var tag: String
)