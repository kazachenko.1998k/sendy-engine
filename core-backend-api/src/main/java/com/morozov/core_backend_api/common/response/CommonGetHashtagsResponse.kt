package com.morozov.core_backend_api.common.response

import com.morozov.core_backend_api.AnyResponse

data class CommonGetHashtagsResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<CommonGetHashtagsResponse.Data>() {
    data class Data(
        var tags: MutableList<String> = mutableListOf()
    )
}