package com.morozov.core_backend_api.common.response

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.contact.Contact

data class CommonCheckFreeNickResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<CommonCheckFreeNickResponse.Data>() {
    data class Data(
        var status: Boolean = false
    )
}