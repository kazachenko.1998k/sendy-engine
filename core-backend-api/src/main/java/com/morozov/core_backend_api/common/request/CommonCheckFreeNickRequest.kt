package com.morozov.core_backend_api.common.request

data class CommonCheckFreeNickRequest(
    //Проверяемый ник пользователя - минимум 5 символов
    var nick: String
)