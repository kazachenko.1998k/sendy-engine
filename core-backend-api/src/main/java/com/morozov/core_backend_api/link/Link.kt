package com.morozov.core_backend_api.link


import com.squareup.moshi.Json

data class Link(

    //Идентификатор типа ссылки
    @field:Json(name = "type_id")
    var typeId: Int? = -1,

    //URL адрес
    @field:Json(name = "link")
    var link: String? = ""

)