package com.morozov.core_backend_api.link.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.link.Link

class LinkListConverter {
    @TypeConverter
    fun fromLink(linkList: List<Link>): String {
        return Gson().toJson(linkList)
    }

    @TypeConverter
    fun toLink(data: String): List<Link> {
        val notesType = object : TypeToken<List<Link>>() {}.type
        return Gson().fromJson<List<Link>>(data, notesType)
    }
}