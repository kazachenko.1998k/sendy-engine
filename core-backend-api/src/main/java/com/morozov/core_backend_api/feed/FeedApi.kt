package com.morozov.core_backend_api.feed

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.feed.responses.FeedMessageByIdResponse
import com.morozov.core_backend_api.feed.responses.FeedMessagesResponse
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse

interface FeedApi {

    //feed.get
    fun getFeedMessages(request: AnyRequest, callback: (response: FeedMessagesResponse) -> Unit = {})

    //feed.message
    fun getFeedMessageById(request: AnyRequest, callback: (response: FeedMessageByIdResponse) -> Unit = {})

    //feed.replies
    fun getFeedReplies(request: AnyRequest, callback: (response: FeedRepliesResponse) -> Unit = {})

}