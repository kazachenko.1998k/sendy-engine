package com.morozov.core_backend_api.feed.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.message.Message

data class FeedMessageByIdResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<FeedMessageByIdResponse.Data>() {
    data class Data(

        var chat_id: Long? = null,

        // массив объектов Message
        var messages: MutableList<Message>? = null,

        // массив идентификаторов, которые не удалось найти по каким-то причинам
        var messages_not_found: MutableList<Long>? = null
    )
}