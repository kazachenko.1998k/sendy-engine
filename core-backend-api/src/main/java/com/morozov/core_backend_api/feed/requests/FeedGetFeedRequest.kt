package com.morozov.core_backend_api.feed.requests

data class FeedGetFeedRequest(

    var from_subscribe: Boolean? = null,
    var last_msg_id: Long? = null,
    var amount: Int? = null,
    var start_amount: Int? = null,
    var page: Int? = null,
    var full_message: Boolean? = null,
    var add_message_owner: Boolean? = null,
    var time_period: Int? = null,
    var hashtags: List<String>? = null,
    var min_params: RequestParams? = null,
    var max_params: RequestParams? = null,
    var sort_default: Int? = null,
    var sort_direction: Boolean? = null,
    var sort_custom_feed: String? = null
)
