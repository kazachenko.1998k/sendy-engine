package com.morozov.core_backend_api.feed.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.message.Message

data class FeedMessagesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<FeedMessagesResponse.Data>() {
    data class Data(

        var message_ids: MutableList<Long> = mutableListOf(),

        // массив объектов Message
        var full_message: MutableList<Message> = mutableListOf()
    )
}