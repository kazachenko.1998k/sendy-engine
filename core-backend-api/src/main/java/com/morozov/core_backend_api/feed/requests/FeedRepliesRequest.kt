package com.morozov.core_backend_api.feed.requests

data class FeedRepliesRequest(
    var chat_id: Long? = null,
    var message_id: Long? = null,
    var page: Int? = null,
    var amount: Int? = null,
    var start_amount: Int? = null,
    var tree_view: Boolean? = null,
    var tree_amount: Int? = null,
    var direction: Int? = null,
    var get_owner: Boolean? = null
)
