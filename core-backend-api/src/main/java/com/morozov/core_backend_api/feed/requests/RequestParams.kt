package com.morozov.core_backend_api.feed.requests

data class RequestParams (val replies: Int = 10, val likes: Int = 5, val summ: Int = 50)