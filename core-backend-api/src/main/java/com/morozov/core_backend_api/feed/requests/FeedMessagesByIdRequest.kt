package com.morozov.core_backend_api.feed.requests

data class FeedMessagesByIdRequest(

    // массив id Message
    var message_ids: MutableList<Long>? = null,

    var get_owner: Boolean? = null
)
