package com.morozov.core_backend_api.feed.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.feed.requests.FeedRepliesRequest
import com.morozov.core_backend_api.message.responses.MessageWithAmount
import com.morozov.core_backend_api.user.UserMini

data class FeedRepliesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<FeedRepliesResponse.Data>() {
    data class Data(

        var chat_id: Long? = null,
        var reply_to: Long? = null,
        // массив объектов Message
        var messages: MessageWithAmount? = null,

        // массив идентификаторов, которые не удалось найти по каким-то причинам
        var users: MutableList<UserMini>? = null
//        var replies: MutableList<MessageWithAmount>? = null
    )
}