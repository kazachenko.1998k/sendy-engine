package com.morozov.core_backend_api.update.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.message.Message

data class UpdateMessagesResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MutableList<Message>>()