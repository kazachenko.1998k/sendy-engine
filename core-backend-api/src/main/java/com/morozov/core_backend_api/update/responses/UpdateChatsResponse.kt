package com.morozov.core_backend_api.update.responses

import com.morozov.core_backend_api.AnyResponse
import com.morozov.core_backend_api.chat.Chat

data class UpdateChatsResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<MutableList<Chat>>()