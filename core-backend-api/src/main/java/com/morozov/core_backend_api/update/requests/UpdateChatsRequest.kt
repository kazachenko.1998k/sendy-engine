package com.morozov.core_backend_api.update.requests

data class UpdateChatsRequest(
    //Время в  микросекундах с последнего обновления
    var last_time_get: Long,

    //Какие сообщения надо получить:
    //0 - новые
    //1 - отредактированные
    //2 - удаленные
    var type: Int,

    //Если не задано - то в соответствии с общим конфигом
    //Количество получаемых результатов (максимальное количество за 1 запрос - не более 300)
    var amount: Int? = null
)

