package com.morozov.core_backend_api.update.requests

data class UpdateLastChatsRequest(
    //Если не задано - то в соответствии с общим конфигом
    //Количество получаемых результатов (максимальное количество за 1 запрос - не более 300)
    var amount: Int? = null
)

