package com.morozov.core_backend_api.update

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.update.responses.UpdateChatsResponse
import com.morozov.core_backend_api.update.responses.UpdateMessagesResponse

interface UpdateApi {

    //Получение информации о новых сообщениях в чатах, участником которых является пользователь
    fun messages(request: AnyRequest, callback: (response: UpdateMessagesResponse) -> Unit = {})

    //Получение информации о новых чатах, участником которых является пользователь или редактировании чатов
    fun chats(request: AnyRequest, callback: (response: UpdateChatsResponse) -> Unit = {})

    //Получение информации о последних N обновленных чатов
    fun lastChats(request: AnyRequest, callback: (response: UpdateChatsResponse) -> Unit = {})

}