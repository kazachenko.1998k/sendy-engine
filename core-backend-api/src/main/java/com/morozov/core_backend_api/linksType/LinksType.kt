package com.morozov.core_backend_api.linksType


import com.squareup.moshi.Json

data class LinksType(

    //Идентификатор типа ссылки
    @field:Json(name = "type_id")
    var typeId: Int? = -1,

    //Буквенный код ссылки
    @field:Json(name = "code")
    var code: String? = "",

    //Название типа ссылки
    @field:Json(name = "title")
    var title: String? = ""


)