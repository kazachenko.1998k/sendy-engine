package com.morozov.core_backend_api



data class AnyInnerRequest(
    var param: Any,

    var method: String = ""
)