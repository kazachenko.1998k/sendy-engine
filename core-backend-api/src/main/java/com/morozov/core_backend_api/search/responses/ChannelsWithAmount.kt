package com.morozov.core_backend_api.search.responses

import com.morozov.core_backend_api.chat.ChatMini

data class ChannelsWithAmount(
    var amount: Int = -1,
    var channels: MutableList<ChatMini> = mutableListOf()
)