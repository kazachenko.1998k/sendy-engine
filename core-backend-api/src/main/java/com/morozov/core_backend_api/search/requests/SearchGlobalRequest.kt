package com.morozov.core_backend_api.search.requests

import com.squareup.moshi.Json

data class SearchGlobalRequest(
    //Поисковая строка (не менее N символов - брать из конфига!)
    var search: String,

    //Искать среди ников только
    var in_nick: Boolean? = null,

    //В названиях (для пользователей - в именах)
    var in_title: Boolean? = null,

    //В описаниях
    var in_description: Boolean? = null,

    //Нужны пользователи или нет
    var users: Boolean? = null,

    //Нужны группы или нет
    var groups: Boolean? = null,

    //Нужны каналы или нет
    var channels: Boolean? = null,

    //Количество элеметнов в резульате поиска
    var amount: Int? = null,

    //Страница выдачи
    var page: Int? = null,

    //Для исключения учатсников какого-либо чата
    var chat_id: Long? = null,

    //Для исключения учатсников какого-либо чата
    var in_group: Boolean? = null
)