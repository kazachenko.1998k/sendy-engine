package com.morozov.core_backend_api.search.responses

import com.morozov.core_backend_api.chat.ChatMini

data class GroupsWithAmount(
    var amount: Int = -1,
    var groups: MutableList<ChatMini> = mutableListOf()
)