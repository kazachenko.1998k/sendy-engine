package com.morozov.core_backend_api.search.responses

import com.morozov.core_backend_api.user.UserMini

data class UsersWithAmount(
    var amount: Int = -1,
    var users: MutableList<UserMini> = mutableListOf()
)