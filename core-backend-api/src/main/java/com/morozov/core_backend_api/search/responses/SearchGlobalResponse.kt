package com.morozov.core_backend_api.search.responses

import com.morozov.core_backend_api.AnyResponse

data class SearchGlobalResponse(private val dateCreate: Long = System.currentTimeMillis()) :
    AnyResponse<SearchGlobalResponse.Data>() {
    data class Data(
        //Информация о пользователях
        var users: UsersWithAmount? = null,

        //Информация о группах
        var groups: GroupsWithAmount? = null,

        //Информация о каналах
        var channels: ChannelsWithAmount? = null
    )
}