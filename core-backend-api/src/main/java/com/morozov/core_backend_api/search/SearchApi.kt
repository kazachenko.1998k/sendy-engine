package com.morozov.core_backend_api.search

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.search.responses.SearchGlobalResponse

interface SearchApi {

    //Поиск по нику/имени/описанию каналов search.global
    fun global(request: AnyRequest, callback: (response: SearchGlobalResponse) -> Unit = {})

}