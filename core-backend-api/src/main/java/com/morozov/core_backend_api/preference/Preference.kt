package com.morozov.core_backend_api.preference

import com.squareup.moshi.Json

data class Preference(

    //Название
    @field:Json(name = "title")
    var title: String? = null,

    //Указатель к какой группе настроек относится
    @field:Json(name = "group_code")
    var groupCode: String? = null,

    //Идентификатор настроек доступа
    @field:Json(name = "code")
    var code: String? = null
)