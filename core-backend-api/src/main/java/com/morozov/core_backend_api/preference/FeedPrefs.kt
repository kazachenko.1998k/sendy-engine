package com.morozov.core_backend_api.preference

enum class FeedPrefs {
    /**
    0 - видят все

    1 - только подписчики

    2 - никто кроме меня
     * */
    FEED_PRIVATE,
    /**
    0 - разрешено подписываться всем

    1 - Все подписки попадают в заявки

    2 - никто не может подписаться
     * */
    FEED_ALLOW_SUBSCRIBE,
    /**
    0 - разрешено всем

    1 - только моим подписчикам

    2 - никому
     * */
    FEED_ALLOW_COMMENTS
}