package com.morozov.core_backend_api.preference

enum class BasePrefs {
    /**
    0 - видят все

    1 - только контакты мои

    2 -никто
     * */
    BASE_HIDE_LAST_ACTIVE,
    /**
    0 - видят все

    1 - только контакты мои

    2 -никто
     * */
    BASE_HIDE_PHONE,
    /**
    0 - видят все

    1 - только контакты мои

    2 -никто
     * */
    BASE_HIDE_NAME
}