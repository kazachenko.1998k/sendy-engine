package com.morozov.core_backend_api.city

import com.squareup.moshi.Json

data class City (

    //Идентификатор города
    @field:Json(name = "id")
    var id: Int? = -1,

    //Идентификатор страны, к которой привязан город
    @field:Json(name = "country_id")
    var countryId: Long? = -1,

    //Название города
    @field:Json(name = "title")
    var title: String? = ""
)