package com.morozov.lib_backend.impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.searchSendy.SearchSendyApi
import com.morozov.core_backend_api.searchSendy.models.*
import com.morozov.core_backend_api.searchSendy.modelsRequest.SearchRequest
import com.morozov.core_backend_api.searchSendy.modelsResponse.*
import io.reactivex.Single
import kotlin.random.Random

class SearchSendyMock(
    private val context: Context,
    private val featureBackendMock: FeatureBackendApi
) : SearchSendyApi {

    private val charPool = ('a'..'z') + ('A'..'Z')

    private val avatars = listOf(
        "https://sun9-65.userapi.com/c850420/v850420106/1772ab/IJ-9HxcuRjk.jpg",
        "https://sun9-22.userapi.com/c831108/v831108435/16fd23/UsmGQDJ2Ano.jpg",
        "https://sun1-25.userapi.com/c847019/v847019846/1db312/zO5NuzVkDb4.jpg?ava=1",
        "https://sun9-17.userapi.com/c852136/v852136365/18ff6f/6FmJF7ecmXc.jpg",
        "https://sun9-59.userapi.com/c841237/v841237492/73a50/TpjXd1802BI.jpg",
        "https://sun1-92.userapi.com/c845416/v845416074/196bc9/6bj177ug3M4.jpg"
    )
    private val nameUsers = listOf(
        "Pepe_master",
        "dolgushev_ilya",
        "Маришка Абрамова",
        "Ангелина",
        "Максим Карасенко",
        "Виолетта Чапаева",
        "Костя",
        "Кирилл",
        "Junior",
        "Алексей Суворов",
        "morozov_evgeniy",
        "putin_vladimir",
        "baskov_nikolay",
        "trololo_123"
    )
    private val nameChannels = listOf(
        "Figma",
        "Киноманы",
        "Noname))",
        "MemUs",
        "Sendy",
        "AFK Arena",
        "Smile",
        "Sport"
    )
    private val nameGroups = listOf(
        "Figma-чатик",
        "Киноманы-чатик",
        "Noname))-кто тут?",
        "MemUs-общий",
        "Sendy-чат",
        "AFK Arena- обсуждаем",
        "Smile - группа",
        "Sport- наше все"
    )

    private val messages = listOf(
        "Когда появятся сторис с фигмой (Figma 228)",
        "Figma топ",
        "Давно не виделись. Когда планирируешь в наши ебеня?",
        "Привет. Как дела?",
        "Доказано",
        "Объясняем работу адронного колайдера на примере базисн...",
        "Кто заплатит за ужин?",
        "Напиши, как доедешь",
        "Опять не отправляется(",
        "Кееек"
    )

    private val logins = listOf(
        "eaveri",
        "Cceory",
        "Wanickaha",
        "Anurra",
        "Havarynea",
        "Gadiha",
        "Gelanalda",
        "Nithanduc",
        "Yeylanter",
        "Rlyiad",
        "Damaliest",
        "Fataco",
        "Eyanad",
        "Nanerm",
        "Heyrgu"
    )

    fun generateRandomDateLong(): Long {
        val isNear = Random.nextBoolean()
        return if (isNear)
            (System.currentTimeMillis() - 1000 * 60 * 60 * 24 * 3..System.currentTimeMillis()).random()
        else
            (System.currentTimeMillis() - 1000 * 60 * 10..System.currentTimeMillis()).random()
    }


    private fun generateLocalUser(exp: String): ModelLocalUser {
        return ModelLocalUser(
            "1",
            avatars.random(),
            "${exp}${nameUsers.random()}",
            nameUsers.random(),
            logins.random(),
            generateRandomDateLong()
            )
    }

    private fun generateGlobalUser(exp: String): ModelGlobalUser {
        return ModelGlobalUser("1",
            avatars.random(),
            "${exp}${nameUsers.random()}",
            nameUsers.random(),
            logins.random(),
            generateRandomDateLong())

    }

    private fun generateLocalChannel(exp: String): ModelLocalChannel {
        return ModelLocalChannel("1",
            avatars.random(),
            "${exp}${nameChannels.random()}",
            (1..5000000).random(),
            messages.random())

    }

    private fun generateGlobalChannel(exp: String): ModelGlobalChannel {
        return ModelGlobalChannel("1",
            avatars.random(),
            "${exp}${nameChannels.random()}",
            (1..5000000).random(),
            messages.random())

    }

    private fun generateLocalGroup(exp: String): ModelLocalGroup {
        return ModelLocalGroup("1",
            avatars.random(),
            "${exp}${nameGroups.random()}",
            (1..5000000).random(),
            messages.random())

    }

    private fun generateGlobalGroup(exp: String): ModelGlobalGroup {
        return ModelGlobalGroup("1",
            avatars.random(),
            "${exp}${nameGroups.random()}",
            (1..5000000).random(),
            messages.random())

    }

    private fun generateMessageFromUser(exp: String): ModelMessageUser {
        return ModelMessageUser("1",
            avatars.random(),
            nameUsers.random(),
            nameUsers.random(),
            logins.random(),
            generateRandomDateLong(),
            "${exp}${messages.random()}")

    }

    private fun generateMessageFromChannel(exp: String): ModelMessageChannel {
        return ModelMessageChannel("1",
            avatars.random(),
            nameChannels.random(),
            logins.random(),
            generateRandomDateLong(),
            "${exp}${messages.random()}")

    }

    private fun generateMessageFromGroup(exp: String): ModelMessageGroup {
        return ModelMessageGroup("1",
            avatars.random(),
            nameGroups.random(),
            logins.random(),
            generateRandomDateLong(),
            "${exp}${messages.random()}")

    }

    override fun search(request: SearchRequest): Single<SearchResponse?> {
        return Single.create {  }
    }
}