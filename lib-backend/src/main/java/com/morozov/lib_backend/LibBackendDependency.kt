package com.morozov.lib_backend

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_impl.FeatureBackendImpl
import com.tinder.scarlet.lifecycle.LifecycleRegistry

object LibBackendDependency {

    var vFeatureBackendImpl: FeatureBackendApi? = null
    var lifecycleRegistry: LifecycleRegistry? = null
    val liveDataBackEndApi: MutableLiveData<FeatureBackendApi?> = MutableLiveData(vFeatureBackendImpl)

    fun clearFeatureBackend() {
        vFeatureBackendImpl = null
        liveDataBackEndApi.postValue(vFeatureBackendImpl)
    }

    fun featureBackendApi(
        endPointWss: String? = null,
        context: Context? = null,
        onConnectionFailed: (lifecycle: LifecycleRegistry) -> Unit = {}
    ): FeatureBackendApi {
        if (vFeatureBackendImpl == null && endPointWss != null && context != null) {
            val registry = lifecycleRegistry ?: LifecycleRegistry(0L)
            vFeatureBackendImpl =
                FeatureBackendImpl(context, endPointWss, registry, onConnectionFailed)
            liveDataBackEndApi.postValue(vFeatureBackendImpl)
        }
        if (vFeatureBackendImpl == null) {
            throw IllegalStateException("FeatureBackendImpl is null, need init")
        }
        return vFeatureBackendImpl!!
    }
}