package com.morozov.core_backend_impl.access

import android.util.Log
import com.morozov.core_backend_api.SignData
import okhttp3.Interceptor
import okhttp3.Response

class AccessTokenInterceptor(
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val authenticatedRequest = chain.request().newBuilder()
        val userId = 0

        authenticatedRequest
            .addHeader("Access-Control-Allow-Origin", "*")
            .addHeader("content-type", "application/x-www-form-urlencoded")
            .addHeader("app_key_id", SignData.applicationId.toString())
            .addHeader("user_id", userId.toString())
            .addHeader("debug_mode", "57xbmudsIsWE7W7mLp406")
            .addHeader("sec-websocket-extensions", "permessage-deflate; client_max_window_bits")

        Log.d("REQUEST", authenticatedRequest.build().headers.toString())
        val result = chain.proceed(authenticatedRequest.build())
        Log.d("RESPONSE", result.toString())
        return result
    }


}