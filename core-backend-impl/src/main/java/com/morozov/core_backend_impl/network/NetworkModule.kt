package com.morozov.core_backend_impl.network

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.morozov.core_backend_impl.access.AccessTokenInterceptor
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.LifecycleRegistry
import com.tinder.scarlet.messageadapter.moshi.MoshiMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.security.cert.CertificateException
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object NetworkModule {
    private const val endPointHttps = "https://dev2.sendy.systems/"

    private fun okHttpSsl(interceptor: Interceptor?): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder().apply {
                interceptor?.let { addInterceptor(it) }
                sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                hostnameVerifier(HostnameVerifier { hostname, session -> true })
            }
            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    private fun okHttp(interceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    private fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()

        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return mapper
    }

    private fun convertFactory(mapper: ObjectMapper): retrofit2.Converter.Factory {
        return JacksonConverterFactory.create(mapper)
    }

    fun scarlet(endPointWss: String, lifecycleRegistry: LifecycleRegistry): Scarlet {
        return Scarlet.Builder()
            .webSocketFactory(okHttpSsl(null).newWebSocketFactory(endPointWss))
            .lifecycle(lifecycleRegistry)
            .addMessageAdapterFactory(MoshiMessageAdapter.Factory())
            .addStreamAdapterFactory(RxJava2StreamAdapterFactory())
            .build()
    }

    fun retrofit(interceptor: Interceptor = AccessTokenInterceptor()): Retrofit {
        return Retrofit.Builder()
            .baseUrl(endPointHttps)
            .client(okHttpSsl(interceptor))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(convertFactory(objectMapper()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }
}