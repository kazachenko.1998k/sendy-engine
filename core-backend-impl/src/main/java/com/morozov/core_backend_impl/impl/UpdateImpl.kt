package com.morozov.core_backend_impl.impl

import android.annotation.SuppressLint
import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.update.UpdateApi
import com.morozov.core_backend_api.update.responses.UpdateChatsResponse
import com.morozov.core_backend_api.update.responses.UpdateMessagesResponse
import com.morozov.core_backend_impl.FeatureBackendImpl

@Suppress("UNCHECKED_CAST")
@SuppressLint("CheckResult")
class UpdateImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) :
    UpdateApi {

    override fun messages(
        request: AnyRequest,
        callback: (response: UpdateMessagesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            MESSAGES,
            webSocketApi.observeUpdateMessages(),
            callback
        )
    }

    override fun chats(
        request: AnyRequest,
        callback: (response: UpdateChatsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            CHATS,
            webSocketApi.observeUpdateChats(),
            callback
        )
    }

    override fun lastChats(
        request: AnyRequest,
        callback: (response: UpdateChatsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            LAST_CHATS,
            webSocketApi.observeUpdateLastChats(),
            callback
        )
    }

    companion object {

        //Получение информации о чате
        const val MESSAGES = "update.messages"
        const val CHATS = "update.chats"
        const val LAST_CHATS = "update.lastChats"
    }


}