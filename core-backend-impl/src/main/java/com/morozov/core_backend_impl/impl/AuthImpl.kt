package com.morozov.core_backend_impl.impl

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AbstractResponse
import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.auth.AuthApi
import com.morozov.core_backend_api.auth.AuthApiREST
import com.morozov.core_backend_api.auth.modelsRequest.AuthInitRequest
import com.morozov.core_backend_api.auth.modelsResponse.*
import com.morozov.core_backend_impl.FeatureBackendImpl
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit

@SuppressLint("CheckResult")
class AuthImpl(
    private val executor: FeatureBackendImpl,
    private val context: Context,
    private val webSocketApi: AllCallsApiWS,
    private val retrofit: Retrofit
) :
    AuthApi {

    private val authApiREST: Lazy<AuthApiREST> = lazy {
        retrofit.create(AuthApiREST::class.java)
    }

    override fun signPhone(
        request: AnyRequest,
        callback: (response: AuthSignPhoneResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SIGN_PHONE,
            webSocketApi.observeAuthSignPhoneResponse(),
            callback
        )
    }

    override fun checkCode(
        request: AnyRequest,
        callback: (response: AuthCheckCodeResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            CHECK_CODE,
            webSocketApi.observeAuthCheckCodeResponse()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                if (!it.data?.user?.nick.isNullOrEmpty())
                    Repository.userDao.insert(it.data!!.user)
            }
        }
    }

    override fun closeSessions(
        request: AnyRequest,
        callback: (response: AuthCloseSessionsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            CLOSE_SESSIONS,
            webSocketApi.observeAuthCloseSessionsResponse(),
            callback
        )
    }

    override fun logOut(scheduler: Scheduler,
                        callback: (response: AuthLogOutResponse) -> Unit) {
        executor.executeNewSubscribe(
            scheduler,
            LOG_OUT,
            webSocketApi.observeAuthLogOutResponse(),
            callback
        )
    }

    override fun getSessions(
        request: AnyRequest,
        callback: (response: AuthGetSessionsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_SESSIONS,
            webSocketApi.observeAuthGetSessionsResponse(),
            callback
        )
    }

    override fun setToken(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        request.data.method = "auth.setToken"
        authApiREST.value.setToken(request).subscribeOn(Schedulers.io())
            .subscribe({ callback(it) }, {})
    }

    override fun getTime(
        userSession: String,
        request: AuthInitRequest?,
        callback: (response: String) -> Unit
    ) {
        authApiREST.value.getTime(userSession, request).subscribeOn(Schedulers.io())
            .subscribe({ callback(it.toString()) }, {})
    }

    override fun init(
        request: AuthInitRequest?,
        callback: (response: InitResponse) -> Unit
    ) {
        authApiREST.value.init(request).subscribeOn(Schedulers.io())
            .subscribe({ callback(it) }, {
                Log.e("Retrofit", it.message.toString())
            })
    }

    override fun signAnonim(
        request: AnyRequest,
        callback: (response: AuthSignAnonymResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SIGN_ANONYM,
            webSocketApi.observeAuthSignAnonymResponse()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                if (!it.data?.user?.nick.isNullOrEmpty()) {
                    Repository.userDao.insert(it.data!!.user)
                }
            }
        }
    }

    companion object {

        const val SIGN_ANONYM = "auth.signAnonym"
        const val SIGN_PHONE = "auth.signPhone"
        const val CHECK_CODE = "auth.checkCode"
        const val CLOSE_SESSIONS = "auth.closeSessions"
        const val LOG_OUT = "auth.logOut"
        const val GET_SESSIONS = "auth.getSessions"

    }
}