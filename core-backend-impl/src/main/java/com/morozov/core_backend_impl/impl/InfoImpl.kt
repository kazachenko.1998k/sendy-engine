package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.info.InfoApi
import com.morozov.core_backend_api.info.response.*
import com.morozov.core_backend_impl.FeatureBackendImpl

@Suppress("UNCHECKED_CAST")
class InfoImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : InfoApi {


    override fun getCountriesCities(
        request: AnyRequest,
        callback: (response: InfoGetCountriesCitiesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_COUNTRIES_CITIES,
            webSocketApi.observeInfoGetCountriesCities(),
            callback
        )
    }

    override fun getCategories(callback: (response: InfoGetCategoriesResponse) -> Unit) {
        executor.executeNewRequest(
            GET_CATEGORIES,
            webSocketApi.observeInfoGetCategories(),
            callback
        )
    }

    override fun getPreferences(callback: (response: InfoGetPreferencesResponse) -> Unit) {
        executor.executeNewRequest(
            GET_PREFERENCES,
            webSocketApi.observeInfoGetPreferences(),
            callback
        )
    }

    override fun linkTypes(callback: (response: InfoLinkTypesResponse) -> Unit) {
        executor.executeNewRequest(
            LINK_TYPES,
            webSocketApi.observeInfoLinkTypes(),
            callback
        )
    }

    override fun getConfig(callback: (response: InfoGetConfigResponse) -> Unit) {
        executor.executeNewRequest(
            GET_CONFIG,
            webSocketApi.observeInfoGetConfig(),
            callback
        )
    }

    override fun time(callback: (response: InfoTimeResponse) -> Unit) {
        executor.executeNewRequest(
            TIME,
            webSocketApi.observeInfoTime(),
            callback
        )
    }

    companion object {
        const val GET_COUNTRIES_CITIES = "info.getCountriesCities"
        const val GET_CATEGORIES = "info.getCategories"
        const val GET_PREFERENCES = "info.getPreferences"
        const val LINK_TYPES = "info.linkTypes"
        const val GET_CONFIG = "info.getConfig"
        const val TIME = "info.time"
    }

}