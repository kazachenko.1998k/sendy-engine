package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.responses.*
import com.morozov.core_backend_impl.FeatureBackendImpl

@Suppress("UNCHECKED_CAST")
class MessageImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : MessageApi {

    override fun send(request: AnyRequest, callback: (response: MessageSendResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SEND,
            webSocketApi.observeMessageSend(),
            callback
        )
    }

    override fun replies(
        request: AnyRequest,
        callback: (response: MessageRepliesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            REPLIES,
            webSocketApi.observeMessageReplies(),
            callback
        )
    }

    override fun delete(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE,
            webSocketApi.observeMessageDelete(),
            callback
        )
    }

    override fun pin(request: AnyRequest, callback: (response: MessagePinResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            PIN,
            webSocketApi.observeMessagePin(),
            callback
        )
    }

    override fun get(request: AnyRequest, callback: (response: MessageGetResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            GET,
            webSocketApi.observeMessageGet(),
            callback
        )
    }

    override fun view(request: AnyRequest, callback: (response: MessageViewResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            VIEW,
            webSocketApi.observeMessageView(),
            callback
        )
    }

    override fun setReaction(
        request: AnyRequest,
        callback: (response: MessageSetReactionResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SET_REACTION,
            webSocketApi.observeMessageSetReaction(),
            callback
        )
    }

    override fun like(request: AnyRequest, callback: (response: MessageLikeResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            LIKE,
            webSocketApi.observeLike(),
            callback
        )
   }

    companion object {

        //Получение информации о чате
        const val SEND = "message.send"
        const val REPLIES = "message.replies"
        const val DELETE = "message.delete"
        const val PIN = "message.pin"
        const val GET = "message.get"
        const val VIEW = "message.view"
        const val SET_REACTION = "message.setReaction"
        const val LIKE = "message.like"
    }

}