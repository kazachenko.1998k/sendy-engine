package com.morozov.core_backend_impl.impl

import android.annotation.SuppressLint
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AbstractResponse
import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chat.requests.ChatRecommendChatsRequest
import com.morozov.core_backend_api.chat.responses.*
import com.morozov.core_backend_impl.FeatureBackendImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
@SuppressLint("CheckResult")
class ChatImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) :
    ChatApi {

    override fun getByID(request: AnyRequest, callback: (response: ChatGetByIDResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            GET_BY_ID,
            webSocketApi.observeChatGetByID()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                it.data?.chats?.forEach { chat ->
                    Repository.chatDao.insert(chat)
                    if (chat.lastMessage != null)
                        Repository.messageDao.insertWithUpdate(chat.lastMessage!!)
                }
            }
        }
    }

    override fun getByIDWithDB(
        request: AnyRequest,
        callback: (response: ChatGetByIDResponse) -> Unit
    ) {
        var callbackIsSend = false
        Repository.chatDaoImpl.getByIDWithDB(request) { getByIDResponse ->
            if (!callbackIsSend) {
                callback(getByIDResponse)
            }
        }
        executor.executeNewRequest(
            request,
            GET_BY_ID,
            webSocketApi.observeChatGetByID()
        ) {
            callbackIsSend = true
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                it.data?.chats?.forEach { chat ->
                    Repository.chatDao.insert(chat)
                    if (chat.lastMessage != null)
                        Repository.messageDao.insertWithUpdate(chat.lastMessage!!)
                }
            }
        }
    }

    override fun getDialogID(
        request: AnyRequest,
        callback: (response: ChatGetDialogIdResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_DIALOG_ID,
            webSocketApi.observeChatGetDialogID()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                val chat = it.data?.chat
                if (chat != null) {
                    Repository.chatDao.insert(chat)
                }
            }
        }
    }

    override fun getByNick(
        request: AnyRequest,
        callback: (response: ChatGetByNickResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_BY_NICK,
            webSocketApi.observeChatGetByNick()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                val chat = it.data?.chat
                if (chat != null) {
                    Repository.chatDao.insert(chat)
                }
            }
        }
    }

    override fun getByHash(
        request: AnyRequest,
        callback: (response: ChatGetByHashResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_BY_HASH,
            webSocketApi.observeChatGetByHash(),
            callback
        )
    }

    override fun getMessages(
        request: AnyRequest,
        callback: (response: ChatGetMessagesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_MESSAGES,
            webSocketApi.observeChatGetMessages()
        ) {
            callback(it)
            CoroutineScope(Dispatchers.IO).launch {
                it.data?.full_message?.forEach { message ->
                    Repository.messageDao.insertWithUpdate(message)
                }
            }
        }
    }

    override fun join(request: AnyRequest, callback: (response: ChatJoinResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            JOIN,
            webSocketApi.observeChatJoin(),
            callback
        )
    }

    override fun leave(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            LEAVE,
            webSocketApi.observeChatLeave(),
            callback
        )
    }

    override fun delete(request: AnyRequest, callback: (response: ChatDeleteResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE,
            webSocketApi.observeChatDelete(),
            callback
        )
    }

    override fun deleteAdmin(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE_ADMIN,
            webSocketApi.observeChatDeleteAdmin(),
            callback
        )
    }

    override fun setGroupRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_GROUP_ROLES,
            webSocketApi.observeChatSetGroupRoles(),
            callback
        )
    }

    override fun setAdminRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_ADMIN_ROLES,
            webSocketApi.observeChatSetAdminRoles(),
            callback
        )
    }

    override fun blockUser(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            BLOCK_USER,
            webSocketApi.observeChatBlockUser(),
            callback
        )
    }

    override fun clearHistory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            CLEAR_HISTORY,
            webSocketApi.observeChatClearHistory(),
            callback
        )
    }

    override fun createNew(
        request: AnyRequest,
        callback: (response: ChatCreateNewResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            CREATE_NEW,
            webSocketApi.observeChatCreateNew(),
            callback
        )
    }

    override fun setChatPrivacy(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_CHATP_RIVACY,
            webSocketApi.observeChatSetChatPrivacy(),
            callback
        )
    }

    override fun updateInviteHash(
        request: AnyRequest,
        callback: (response: ChatUpdateInviteHashResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            UPDATE_INVITE_HASH,
            webSocketApi.observeChatUpdateInviteHash(),
            callback
        )
    }

    override fun edit(request: AnyRequest, callback: (response: ChatCreateNewResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            EDIT,
            webSocketApi.observeChatEdit(),
            callback
        )
    }

    override fun setUserRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_USER_ROLES,
            webSocketApi.observeChatSetUserRoles(),
            callback
        )
    }

    override fun userChats(
        request: AnyRequest,
        callback: (response: ChatUserChatsResponse) -> Unit
    ) {
        if (request.data.param is ChatRecommendChatsRequest) {
            executor.executeNewRequest(
                request,
                GET_RECOMMENDS,
                webSocketApi.observeChatUserChats(),
                callback
            )
        } else {
            Repository.chatDaoImpl.userChats(request) {
                callback(it)
            }
        }
    }

    override fun getMembers(
        request: AnyRequest,
        callback: (response: ChatGetMembersResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_MEMBERS,
            webSocketApi.observeChatGetMembers(),
            callback
        )
    }

    override fun getMembersAmount(
        request: AnyRequest,
        callback: (response: ChatGetMembersAmountResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_MEMBERS_AMOUNT,
            webSocketApi.observeChatGetMembersAmount(),
            callback
        )
    }

    override fun mute(request: AnyRequest, callback: (response: ChatMuteResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            MUTE,
            webSocketApi.observeChatMute(),
            callback
        )
    }

    override fun userAction(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            USER_ACTION,
            webSocketApi.observeChatUserAction(),
            callback
        )
    }

    override fun addFavorite(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            ADD_FAVORITE,
            webSocketApi.observeChatAddFavorite(),
            callback
        )
    }

    override fun addMembers(
        request: AnyRequest,
        callback: (response: ChatAddMembersResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            ADD_MEMBERS,
            webSocketApi.observeChatAddMembersFavorite(),
            callback
        )
    }

    override fun getUserRole(
        request: AnyRequest,
        callback: (response: ChatGetUserRoleResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_USER_ROLE,
            webSocketApi.observeChatGetUserRole(),
            callback
        )
    }

    companion object {

        //Получение информации о чате
        const val GET_BY_ID = "chat.getByIDs"
        const val GET_DIALOG_ID = "chat.getDialogId"
        const val GET_BY_NICK = "chat.getByNick"
        const val GET_BY_HASH = "chat.getByHash"

        //Получение сообщений в чате
        const val GET_MESSAGES = "chat.getMessages"

        //Присоединение/выход из чата
        const val JOIN = "chat.join"
        const val LEAVE = "chat.leave"
        const val DELETE = "chat.delete"
        const val DELETE_ADMIN = "chat.deleteAdmin"
        const val SET_GROUP_ROLES = "chat.setGroupRoles"
        const val SET_ADMIN_ROLES = "chat.setAdminRoles"
        const val BLOCK_USER = "chat.blockUser"
        const val CLEAR_HISTORY = "chat.clearHistory"
        const val ADD_MEMBERS = "chat.addMembers"
        const val GET_USER_ROLE = "chat.getUserRole"

        //Создание и редактирование чата
        const val CREATE_NEW = "chat.createNew"
        const val SET_CHATP_RIVACY = "chat.setChatPrivacy"
        const val UPDATE_INVITE_HASH = "chat.updateInviteHash"
        const val EDIT = "chat.edit"
        const val SET_USER_ROLES = "chat.setUserRoles"
        const val USER_CHATS = "chat.userChats"
        const val GET_RECOMMENDS = "chat.getRecommends"
        const val GET_MEMBERS = "chat.getMembers"
        const val GET_MEMBERS_AMOUNT = "chat.getMembersAmount"
        const val MUTE = "chat.mute"
        const val USER_ACTION = "chat.userAction"
        const val ADD_FAVORITE = "chat.addFavorite"
    }
}