package com.morozov.core_backend_impl.impl

import com.example.util_cache.Repository
import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_api.user.requests.UserGetByIdsRequest
import com.morozov.core_backend_api.user.responses.*
import com.morozov.core_backend_impl.FeatureBackendImpl
import io.reactivex.Scheduler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
class UserImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : UserApi {

    override fun saveProfile(
        request: AnyRequest,
        callback: (response: UserSaveProfileResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SAVE_PROFILE,
            webSocketApi.observeUserSaveProfile(),
            callback
        )
    }

    override fun getByNick(
        request: AnyRequest,
        callback: (response: UserGetByNickResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_BY_NICK,
            webSocketApi.observeUserGetByNick(),
            callback
        )
    }

    override fun getLastActive(
        request: AnyRequest,
        callback: (response: UserGetLastActiveResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_LAST_ACTIVE,
            webSocketApi.observeUserGetLastActive(),
            callback
        )
    }

    override fun getByIds(request: AnyRequest, callback: (response: UserGetByIdsResponse) -> Unit) {
        var callbackIsSend = false
        val req = request.data.param
        if (req is UserGetByIdsRequest && req.uids.isNullOrEmpty().not()) {
            val id = req.uids.first()
            Repository.userDaoImpl.getByIdFromDB(id) { user ->
                if (!callbackIsSend) {
                    callback(UserGetByIdsResponse().apply {
                        data = UserGetByIdsResponse.Data(mutableListOf(user))
                    })
                }
            }
        }

        executor.executeNewRequest(
            request,
            GET_BY_IDS,
            webSocketApi.observeUserGetByIds()
        ) {
            val resp = it
            callbackIsSend = true
            if (resp.data?.users != null)
                CoroutineScope(Dispatchers.IO).launch {
                    Repository.userDao.insert(resp.data!!.users)
                }
            callback(resp)
        }
    }

    override fun getByIdFromDB(request: Long, callback: (response: User) -> Unit) {
        Repository.userDaoImpl.getByIdFromDB(request) {
            callback(it)
        }
    }

    override fun getByIdsMini(
        request: AnyRequest,
        callback: (response: UserGetByIdsMiniResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_BY_IDS,
            webSocketApi.observeUserGetByIdsMini(),
            callback
        )
    }

    override fun block(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            BLOCK,
            webSocketApi.observeUserBlock(),
            callback
        )
    }

    override fun delete(callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            DELETE,
            webSocketApi.observeUserDelete(),
            callback
        )
    }

    override fun savePreference(
        request: AnyRequest,
        callback: (response: UserSavePreferenceResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SAVE_PREFRENCE,
            webSocketApi.observeUserSavePreference(),
            callback
        )
    }

    override fun setCategory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_CATEGORY,
            webSocketApi.observeUserSetCategory(),
            callback
        )
    }

    override fun saveHashtags(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SAVE_HASHTAGS,
            webSocketApi.observeUserSaveHashtags(),
            callback
        )
    }

    override fun addHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            ADD_HASHTAG,
            webSocketApi.observeUserAddHashtag(),
            callback
        )
    }

    override fun deleteHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE_HASHTAG,
            webSocketApi.observeUserDeleteHashtag(),
            callback
        )
    }

    override fun setLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SET_LINK,
            webSocketApi.observeUserSetLink(),
            callback
        )
    }

    override fun deleteLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE_LINK,
            webSocketApi.observeUserDeleteLink(),
            callback
        )

    }

    override fun subscribe(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SUBSCRIBE,
            webSocketApi.observeUserSubscribe(),
            callback
        )
    }

    override fun subscribeQueue(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            SUBSCRIBE_QUEUE,
            webSocketApi.observeUserSubscribeQueue(),
            callback
        )
    }

    override fun subscribeConfirm(
        request: AnyRequest,
        callback: (response: EmptyResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            SUBSCRIBE_CONFIRM,
            webSocketApi.observeUserSubscribeConfirm(),
            callback
        )
    }

    override fun getSubscribers(
        request: AnyRequest,
        callback: (response: UserGetSubscribersResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_SUBSCRIBERS,
            webSocketApi.observeUserGetSubscribers(),
            callback
        )
    }

    override fun getSubscribersMini(
        request: AnyRequest,
        callback: (response: UserGetSubscribersMiniResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_SUBSCRIBERS,
            webSocketApi.observeUserGetSubscribersMini(),
            callback
        )
    }

    override fun importContacts(
        request: AnyRequest,
        callback: (response: UserImportContactsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            IMPORT_CONTACTS,
            webSocketApi.observeUserImportContacts(),
            callback
        )
    }

    override fun addContact(
        request: AnyRequest,
        callback: (response: UserAddContactResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            ADD_CONTACT,
            webSocketApi.observeUserAddContact(),
            callback
        )
    }

    override fun deleteContact(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            DELETE_CONTACT,
            webSocketApi.observeUserDeleteContact(),
            callback
        )
    }

    override fun getContacts(callback: (response: UserGetContactsResponse) -> Unit) {
        executor.executeNewRequest(
            GET_CONTACTS,
            webSocketApi.observeUserGetContacts(),
            callback
        )
    }

    override fun sendStatus(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            STATUS,
            webSocketApi.observeUserSendStatus(),
            callback
        )
    }

    override fun subscribeStatus(
        scheduler: Scheduler,
        callback: (response: UserStatusResponse) -> Unit
    ) {
        executor.executeNewSubscribe(
            scheduler,
            STATUS,
            webSocketApi.observeUserSubscribeStatus(),
            callback
        )
    }

    companion object {
        const val SAVE_PROFILE = "user.saveProfile"
        const val GET_BY_NICK = "user.getByNick"
        const val GET_LAST_ACTIVE = "user.getLastActive"
        const val GET_BY_IDS = "user.getByIds"
        const val BLOCK = "user.block"
        const val DELETE = "user.delete"
        const val SAVE_PREFRENCE = "user.savePreference"
        const val SET_CATEGORY = "user.setCategory"
        const val SAVE_HASHTAGS = "user.saveHashtags"
        const val ADD_HASHTAG = "user.addHashtag"
        const val DELETE_HASHTAG = "user.deleteHashtag"
        const val SET_LINK = "user.setLink"
        const val DELETE_LINK = "user.deleteLink"
        const val SUBSCRIBE = "user.subscribe"
        const val SUBSCRIBE_QUEUE = "user.subscribeQueue"
        const val SUBSCRIBE_CONFIRM = "user.subscribeConfirm"
        const val GET_SUBSCRIBERS = "user.getSubscribers"
        const val IMPORT_CONTACTS = "user.importContacts"
        const val ADD_CONTACT = "user.addContact"
        const val DELETE_CONTACT = "user.deleteContact"
        const val GET_CONTACTS = "user.getContacts"
        const val STATUS = "user.status"
    }


}