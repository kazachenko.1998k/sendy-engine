package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feed.responses.FeedMessageByIdResponse
import com.morozov.core_backend_api.feed.responses.FeedMessagesResponse
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse
import com.morozov.core_backend_impl.FeatureBackendImpl

@Suppress("UNCHECKED_CAST")
class FeedImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : FeedApi {

    override fun getFeedMessages(
        request: AnyRequest,
        callback: (response: FeedMessagesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            FEED_GET,
            webSocketApi.observeGetFeedMessages(),
            callback
        )
    }

    override fun getFeedMessageById(
        request: AnyRequest,
        callback: (response: FeedMessageByIdResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            FEED_MESSAGES,
            webSocketApi.observeGetFeedMessagesById(),
            callback
        )
    }

    override fun getFeedReplies(
        request: AnyRequest,
        callback: (response: FeedRepliesResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            FEED_REPLIES,
            webSocketApi.observeGetFeedReplies(),
            callback
        )
    }

    companion object {
        //Получение информации о чате
        const val FEED_GET = "feed.get"
        const val FEED_MESSAGES = "feed.messages"
        const val FEED_REPLIES = "feed.replies"
    }
}