package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.common.CommonApi
import com.morozov.core_backend_api.common.response.CommonCheckFreeNickResponse
import com.morozov.core_backend_api.common.response.CommonGetHashtagsResponse
import com.morozov.core_backend_impl.FeatureBackendImpl

@Suppress("UNCHECKED_CAST")
class CommonImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : CommonApi {

    override fun checkFreeNick(
        request: AnyRequest,
        callback: (response: CommonCheckFreeNickResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            CHECK_FREE_NICK,
            webSocketApi.observeCommonCheckFreeNick(),
            callback
        )
    }

    override fun getHashtags(
        request: AnyRequest,
        callback: (response: CommonGetHashtagsResponse) -> Unit
    ) {
        executor.executeNewRequest(
            request,
            GET_HASHTAGS,
            webSocketApi.observeCommonGetHashtags(),
            callback
        )
    }

    companion object {
        const val CHECK_FREE_NICK = "common.checkFreeNick"
        const val GET_HASHTAGS = "common.getHashtags"
    }

}