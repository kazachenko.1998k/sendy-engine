package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.search.SearchApi
import com.morozov.core_backend_api.search.responses.SearchGlobalResponse
import com.morozov.core_backend_impl.FeatureBackendImpl

class SearchImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : SearchApi {
    override fun global(request: AnyRequest, callback: (response: SearchGlobalResponse) -> Unit) {
        executor.executeNewRequest(
            request,
            GLOBAL,
            webSocketApi.observeSearchGlobal(),
            callback
        )
    }

    companion object {

        //Получение информации о чате
        const val GLOBAL = "search.global"
    }

}