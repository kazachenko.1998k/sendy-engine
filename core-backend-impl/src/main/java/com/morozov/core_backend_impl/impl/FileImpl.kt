package com.morozov.core_backend_impl.impl

import com.morozov.core_backend_api.AllCallsApiWS
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.FileApi
import com.morozov.core_backend_api.file.response.FileUploadResponse
import com.morozov.core_backend_impl.FeatureBackendImpl

class FileImpl(
    private val executor: FeatureBackendImpl,
    private val webSocketApi: AllCallsApiWS
) : FileApi {

    override fun uploadFile(request: AnyRequest, callback: (response: FileUploadResponse) -> Unit) {

        executor.executeNewRequest(
            request,
            FILE_UPLOAD,
            webSocketApi.observeUploadFile(),
            callback
        )
    }


    companion object {

        //Получение информации о чате
        const val FILE_UPLOAD = "file.upload"
    }
}