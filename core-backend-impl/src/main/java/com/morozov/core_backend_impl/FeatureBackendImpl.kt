package com.morozov.core_backend_impl

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.morozov.core_backend_api.*
import com.morozov.core_backend_api.SignData.updateSign
import com.morozov.core_backend_api.activityMemus.ActivityMemusApi
import com.morozov.core_backend_api.auth.AuthApi
import com.morozov.core_backend_api.auth.AuthApiREST
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chatsMemus.ChatsMemusApi
import com.morozov.core_backend_api.common.CommonApi
import com.morozov.core_backend_api.commonMemus.CommonMemusApi
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.morozov.core_backend_api.file.FileApi
import com.morozov.core_backend_api.info.InfoApi
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.profileMemus.ProfileMemusApi
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.search.SearchApi
import com.morozov.core_backend_api.update.UpdateApi
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_impl.access.AccessTokenInterceptor
import com.morozov.core_backend_impl.impl.*
import com.morozov.core_backend_impl.network.NetworkModule
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.lifecycle.LifecycleRegistry
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import java.util.*


@SuppressLint("CheckResult")
class FeatureBackendImpl(
    private val context: Context,
    private val endPointWebSocket: String,
    private val lifecycleRegistry: LifecycleRegistry,
    private val onConnectionFailed: (lifecycle: LifecycleRegistry) -> Unit
) :
    FeatureBackendApi {


    override var connectionIsOpen = false
    override lateinit var pushListener: Flowable<PushResponse>
    private var listEvents: ArrayDeque<() -> Unit> = ArrayDeque()

    private val interceptor: Lazy<AccessTokenInterceptor> =
        lazy { AccessTokenInterceptor() }

    private val scarlet: Lazy<Scarlet> = lazy {
        NetworkModule.scarlet(endPointWebSocket, lifecycleRegistry)
    }

    private val retrofit: Lazy<Retrofit> = lazy {
        NetworkModule.retrofit(interceptor.value)
    }

    val webSocketApiWS = lazy {
        scarlet.value.create(AllCallsApiWS::class.java)
    }

    var counterFailedConnection = 0

    init {
        SignData.connectionState.postValue(connectionIsOpen)
        webSocketApiWS.value.observeWebSocketEvent()
            .subscribeOn(Schedulers.io())
            .subscribe({ event ->
                val veryLongString = event.toString()
                Log.d("WebSocket", veryLongString)
                val before = connectionIsOpen
                connectionIsOpen = when (event) {
                    is WebSocket.Event.OnConnectionOpened<*> -> {
                        invokeAllRequest()
                        counterFailedConnection = 0
                        true
                    }
                    is WebSocket.Event.OnMessageReceived -> {
                        counterFailedConnection = 0
                        true
                    }
                    is WebSocket.Event.OnConnectionFailed -> {
                        counterFailedConnection++
                        if (counterFailedConnection > 2) {
                            counterFailedConnection = 0
                            onConnectionFailed(lifecycleRegistry)
                        }
                        false
                    }
                    else -> {
                        false
                    }
                }
                if (before != connectionIsOpen) {
                    SignData.connectionState.postValue(connectionIsOpen)
                }
            }, {
                it.printStackTrace()
            })
        pushListener = webSocketApiWS.value.observePush()
            .filter { it.notification != null }

    }

    private fun invokeAllRequest() {
        while (listEvents.isNotEmpty()) {
            listEvents.poll()!!.invoke()
        }
    }

    fun executeNewRequest(event: () -> Unit) {
        if (connectionIsOpen) {
            event.invoke()
        } else {
            listEvents.push(event)
        }
    }

    fun <T : AbstractResponse> executeNewRequest(
        nameRequest: String,
        response: Flowable<T>,
        callback: (response: T) -> Unit
    ) {
        executeNewRequest(AnyRequest(AnyInnerRequest(true)), nameRequest, response, callback)
    }


    fun <T : AbstractResponse> executeNewRequest(
        request: AnyRequest,
        nameRequest: String,
        response: Flowable<T>,
        callback: (response: T) -> Unit
    ) {
        executeNewRequest {
            var req: Disposable? = null
            req =
                response
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter { it.rid == request.rid }
                    .subscribe({ event ->
                        if (event!!.result.not() && event.error?.code == TIME_FAILED_CODE) {
                            val authApiREST =
                                NetworkModule.retrofit().create(AuthApiREST::class.java)
                            authApiREST.start().subscribeOn(Schedulers.io())
                                .subscribe({
                                    val backEndTime = it.data?.time_ms
                                    SignData.lambdaTime = if (backEndTime == null) 0L else
                                        System.currentTimeMillis() - backEndTime
                                    Log.e("Retrofit", SignData.lambdaTime.toString())
                                    executeNewRequest(request, nameRequest, response, callback)
                                }, {
                                    Log.e("Retrofit", it.message.toString())
                                })
                        } else {
                            callback(event)
                        }
                        req?.dispose()
                    }, {
                        it.printStackTrace()
                    })
            request.data.method = nameRequest
            request.updateSign()
            Log.d("WebSocket", "OnMessageSend($request)")
            webSocketApiWS.value.sendMessage(request)
        }
    }

    fun <T : AbstractResponse> executeNewSubscribe(
        scheduler: Scheduler,
        nameRequest: String,
        response: Flowable<T>,
        callback: (response: T) -> Unit
    ) {
        response
            .subscribeOn(scheduler)
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.method == nameRequest }
            .subscribe({ event ->
                callback(event)
            }, {
                it.printStackTrace()
            })
    }


    private val vFeedMemusApi: Lazy<FeedMemusApi> =
        lazy { retrofit.value.create(FeedMemusApi::class.java) }
    private val vActivityMemusApi: Lazy<ActivityMemusApi> =
        lazy { retrofit.value.create(ActivityMemusApi::class.java) }
    private val vProfileMemusApi: Lazy<ProfileMemusApi> =
        lazy { retrofit.value.create(ProfileMemusApi::class.java) }
    private val vCommonMemusApi: Lazy<CommonMemusApi> =
        lazy { retrofit.value.create(CommonMemusApi::class.java) }
    private val vChatsMemusApi: Lazy<ChatsMemusApi> =
        lazy { retrofit.value.create(ChatsMemusApi::class.java) }
    private val chatApi: Lazy<ChatApi> =
        lazy { ChatImpl(this, webSocketApiWS.value) }
    private val userApi: Lazy<UserApi> =
        lazy { UserImpl(this, webSocketApiWS.value) }
    private val fileApi: Lazy<FileApi> =
        lazy { FileImpl(this, webSocketApiWS.value) }
    private val searchApi: Lazy<SearchApi> =
        lazy { SearchImpl(this, webSocketApiWS.value) }
    private val messageApi: Lazy<MessageApi> =
        lazy { MessageImpl(this, webSocketApiWS.value) }
    private val feedApi: Lazy<FeedApi> =
        lazy { FeedImpl(this, webSocketApiWS.value) }
    private val infoApi: Lazy<InfoApi> =
        lazy { InfoImpl(this, webSocketApiWS.value) }
    private val commonApi: Lazy<CommonApi> =
        lazy { CommonImpl(this, webSocketApiWS.value) }
    private val updateApi: Lazy<UpdateApi> =
        lazy { UpdateImpl(this, webSocketApiWS.value) }

    /**
     * CommonApi
     */
    private val authApi: Lazy<AuthApi> =
        lazy { AuthImpl(this, context, webSocketApiWS.value, retrofit.value) }

    override fun authApi(): AuthApi {
        return authApi.value
    }

    /**
     * User api
     * */
    override fun userApi(): UserApi {
        return userApi.value
    }

    /**
     * File api
     * */
    override fun fileApi(): FileApi {
        return fileApi.value
    }

    /**
     * MemUs
     */
    override fun feedMemusApi(): FeedMemusApi {
        return vFeedMemusApi.value
    }

    override fun activityMemusApi(): ActivityMemusApi {
        return vActivityMemusApi.value

    }

    override fun profileMemusApi(): ProfileMemusApi {
        return vProfileMemusApi.value
    }

    override fun commonMemusApi(): CommonMemusApi {
        return vCommonMemusApi.value
    }

    override fun chatMemusApi(): ChatsMemusApi {
        return vChatsMemusApi.value
    }

    override fun chatApi(): ChatApi {
        return chatApi.value
    }

    override fun searchApi(): SearchApi {
        return searchApi.value
    }

    override fun messageApi(): MessageApi {
        return messageApi.value
    }

    override fun feedApi(): FeedApi {
        return feedApi.value
    }

    override fun infoApi(): InfoApi {
        return infoApi.value
    }

    override fun commonApi(): CommonApi {
        return commonApi.value
    }

    override fun updateApi(): UpdateApi {
        return updateApi.value
    }


    companion object {
        const val TIME_FAILED_CODE = 616
    }
}