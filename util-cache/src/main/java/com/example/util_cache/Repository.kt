package com.example.util_cache

import android.content.Context
import com.example.util_cache.chat.ChatDaoImpl
import com.example.util_cache.user.UserDaoImpl

object Repository {
    private lateinit var context: Context

    fun initRepository(context: Context) {
        this.context = context
        AppDatabase.getAppDataBase(Repository.context)
//        CoroutineScope(Dispatchers.IO).launch {
//            clearDB()
//        }
    }

    fun clearDB() {
        AppDatabase.INSTANCE!!.clearAllTables()
    }

    val userDaoImpl by lazy { UserDaoImpl(context) }
    val chatDaoImpl by lazy { ChatDaoImpl(context) }
    val userDao by lazy { AppDatabase.getAppDataBase(context).userDao() }
    val chatDao by lazy { AppDatabase.getAppDataBase(context).chatDao() }
    val messageDao by lazy { AppDatabase.getAppDataBase(context).messageDao() }
}