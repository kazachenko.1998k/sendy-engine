package com.example.util_cache.chat

import com.morozov.core_backend_api.chat.Chat
import io.reactivex.Single
import kotlinx.coroutines.launch

fun ChatDao.saveOrUpdateChat(chat: Chat): Single<Boolean> {
    return Single.create { emitter ->
        kotlinx.coroutines.CoroutineScope(kotlinx.coroutines.Dispatchers.IO).launch {
            if (getChatById(chat.id).isEmpty())
                insert(chat)
            else
                update(chat)
            emitter.onSuccess(true)
            deleteIfExists(chat.toMini())
        }
    }
}

fun ChatDao.deleteIfExists(chat: Chat): Single<Boolean> {
    return Single.create { emitter ->
        kotlinx.coroutines.CoroutineScope(kotlinx.coroutines.Dispatchers.IO).launch {
            delete(chat)
            emitter.onSuccess(true)
        }
    }
}