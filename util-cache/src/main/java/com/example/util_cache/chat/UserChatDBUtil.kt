package com.example.util_cache.chat

import com.morozov.core_backend_api.chat.UserChat
import io.reactivex.Single
import kotlinx.coroutines.launch

fun ChatDao.saveOrUpdateChat(chat: UserChat): Single<Boolean> {
    return Single.create { emitter ->
        kotlinx.coroutines.CoroutineScope(kotlinx.coroutines.Dispatchers.IO).launch {
            if (getChatById(chat.chatId).isEmpty())
                insert(chat)
            else
                update(chat)
            emitter.onSuccess(true)
        }
    }
}

fun ChatDao.deleteIfExists(chat: UserChat): Single<Boolean> {
    return Single.create { emitter ->
        kotlinx.coroutines.CoroutineScope(kotlinx.coroutines.Dispatchers.IO).launch {
            delete(chat)
            emitter.onSuccess(true)
        }
    }
}