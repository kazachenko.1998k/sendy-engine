package com.example.util_cache.chat

import androidx.room.*
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatMini
import com.morozov.core_backend_api.chat.UserChat
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.Message.Companion.WATCHED

@Dao
interface ChatDao {

    /**
     * For full Chat model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chat: Chat)

    @Update
    fun update(chat: Chat)

    @Delete
    fun delete(chat: Chat)


    @Query("SELECT * FROM ChatTable WHERE id LIKE :chatId")
    suspend fun getChatById(chatId: Long): List<Chat>

    @Query("SELECT * FROM ChatTable WHERE id = :chatId")
    suspend fun getSingleChatById(chatId: Long): Chat?

    @Query("SELECT * FROM ChatTable")
    suspend fun getAllChats(): List<Chat>

    @Query("SELECT * FROM ChatTable WHERE type IN (:typesSearch) ORDER BY ChatTable.lastMessageTime DESC")
    suspend fun getAllChatsByType(@Chat.Companion.Type typesSearch: List<Int>): MutableList<Chat>

    /**
     * For Chat Mini model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chat: ChatMini)

    @Update
    fun update(chat: ChatMini)

    @Delete
    fun delete(chat: ChatMini)

    @Query("SELECT * FROM ChatMiniTable WHERE chatId LIKE :chatId")
    suspend fun getChatMiniById(chatId: Long): List<ChatMini>

    @Query("SELECT * FROM ChatMiniTable WHERE chatId = :chatId")
    suspend fun getSingleChatMiniById(chatId: Long): ChatMini?

    @Query("SELECT * FROM ChatMiniTable")
    suspend fun getAllChatsMini(): List<ChatMini>

    /**
     * For User Chat model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chat: UserChat)

    @Update
    fun update(chat: UserChat)

    @Delete
    fun delete(chat: UserChat)

    @Query("SELECT * FROM UserChatTable WHERE chatId LIKE :chatId")
    suspend fun getUserChatById(chatId: Long): List<UserChat>

    @Query("SELECT * FROM UserChatTable WHERE chatId = :chatId")
    suspend fun getSingleChatUserById(chatId: Long): UserChat?

    @Query("SELECT * FROM UserChatTable")
    suspend fun getAllUserChats(): List<UserChat>

    /**
     * For full Group model
     * */

    @Query("SELECT * FROM ChatTable WHERE id LIKE :chatId AND type LIKE 'GROUP_CHAT'")
    suspend fun getGroupById(chatId: Long): List<Chat>

    @Query("SELECT * FROM ChatTable WHERE type LIKE 'GROUP_CHAT'")
    suspend fun getAllGroup(): List<Chat>

    /**
     * For Group Mini model
     * */

    @Query("SELECT * FROM ChatMiniTable WHERE chatId LIKE :chatId AND type LIKE 'GROUP_CHAT'")
    suspend fun getGroupMiniById(chatId: Long): List<ChatMini>

    @Query("SELECT * FROM ChatMiniTable WHERE type LIKE 'GROUP_CHAT'")
    suspend fun getAllGroupMini(): List<ChatMini>

    /**
     * For full Group model
     * */

    @Query("SELECT * FROM ChatTable WHERE id LIKE :chatId AND type LIKE 'CHANNEL'")
    suspend fun getChannelById(chatId: Long): List<Chat>

    @Query("SELECT * FROM ChatTable WHERE type LIKE 'CHANNEL'")
    suspend fun getAllChannel(): List<Chat>

    /**
     * For Group Mini model
     * */

    @Query("SELECT * FROM ChatMiniTable WHERE chatId LIKE :chatId AND type LIKE 'CHANNEL'")
    suspend fun getChannelMiniById(chatId: Long): List<ChatMini>

    @Query("SELECT * FROM ChatMiniTable WHERE type LIKE 'CHANNEL'")
    suspend fun getAllChannelMini(): List<ChatMini>

    /**
     * For Messages
     * */

    @Query("SELECT * FROM MessageTable WHERE chatId = :chatId")
    suspend fun getMessagesByChatId(chatId: Long): List<Message>

    @Query("SELECT MAX(timeAddMS) FROM MessageTable")
    suspend fun getTimeLastMessage(): Long?

    @Query("SELECT MAX(editTimeMS) FROM ChatTable")
    suspend fun getTimeLastChat(): Long?

    @Query("SELECT COUNT(id) FROM MessageTable WHERE chatId = :chatId AND status <> $WATCHED AND uid <> :uid")
    suspend fun getUnreadCountMessages(chatId: Long, uid: Long = SignData.uid): Int?

    @Query("SELECT COUNT(DISTINCT chatId) FROM MessageTable WHERE status <> $WATCHED AND uid <> :uid")
    suspend fun getUnreadCountChat(uid: Long = SignData.uid): Int?

}