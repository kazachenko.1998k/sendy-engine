package com.example.util_cache.chat

import android.util.Log
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatMini
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun ChatDao.saveOrUpdateChatMini(chat: ChatMini): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            insert(chat)
            emitter.onSuccess(true)
        }
    }
}

fun ChatDao.saveOrUpdateChatMini(chatFull: Chat): Single<Boolean> {
    val chatMini = convertChatToChatMini(chatFull)
    return this.saveOrUpdateChatMini(chatMini)
}

fun convertChatToChatMini(chat: Chat): ChatMini {
    return ChatMini(
        chat.id,
        chat.type,
        chat.private,
        chat.title,
        chat.iconFileId,
        chat.membersAmount,
        chat.nick,
        chat.inviteHash,
        chat.userRole,
        chat.muteTime
    )
}

private fun ChatMini.getFullChat(baseChat: Chat): Chat {
    baseChat.type = type ?: baseChat.type
    baseChat.private = private ?: baseChat.private
    baseChat.title = title ?: baseChat.title
    baseChat.iconFileId = iconFileId
    baseChat.membersAmount = membersAmount
    baseChat.nick = nick
    baseChat.inviteHash = inviteHash ?: baseChat.inviteHash
    return baseChat
}

fun ChatDao.deleteIfExists(chat: ChatMini): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            delete(chat)
            emitter.onSuccess(true)
        }
    }
}