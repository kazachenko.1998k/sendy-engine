package com.example.util_cache.chat

import android.content.Context
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chat.requests.ChatGetByIDRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMessagesRequest
import com.morozov.core_backend_api.chat.requests.ChatUserChatsRequest
import com.morozov.core_backend_api.chat.responses.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChatDaoImpl(val context: Context) : ChatApi {
    override fun getByID(request: AnyRequest, callback: (response: ChatGetByIDResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun getByIDWithDB(
        request: AnyRequest,
        callback: (response: ChatGetByIDResponse) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val req = request.data.param
            if (req is ChatGetByIDRequest && req.chat_ids.isNullOrEmpty().not()) {
                val chat = getChatFromBD(req.chat_ids.first())
                if (chat != null) withContext(Dispatchers.Main) {
                    callback(ChatGetByIDResponse().apply {
                        data = ChatGetByIDResponse.Data(
                            mutableListOf(chat)
                        )
                    })
                }
            }
        }
    }

    private suspend fun getChatFromBD(request: Long): Chat? {
        val chatDao = AppDatabase.getAppDataBase(context).chatDao()
        val chat = chatDao.getSingleChatById(request)
        if (chat != null) return chat
        val chatMini = chatDao.getSingleChatMiniById(request)
        if (chatMini != null) return Chat(
            id = chatMini.chatId,
            type = chatMini.type ?: 0,
            private = chatMini.private ?: 0,
            title = chatMini.title,
            iconFileId = chatMini.iconFileId,
            membersAmount = chatMini.membersAmount,
            nick = chatMini.nick,
            inviteHash = chatMini.inviteHash,
            userRole = chatMini.userRole,
            muteTime = chatMini.muteTime
        )
        return null
    }

    override fun getDialogID(
        request: AnyRequest,
        callback: (response: ChatGetDialogIdResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getByNick(
        request: AnyRequest,
        callback: (response: ChatGetByNickResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getByHash(
        request: AnyRequest,
        callback: (response: ChatGetByHashResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getMessages(
        request: AnyRequest,
        callback: (response: ChatGetMessagesResponse) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val req = request.data.param
            if (req is ChatGetMessagesRequest) {

                val res = ChatGetMessagesResponse()
                val messages =
                    AppDatabase.getAppDataBase(context).chatDao()
                        .getMessagesByChatId(req.chat_id)
                        .toMutableList()
                val idsMessages = mutableListOf<Long>()

                messages.forEach {
                    if (it.id != null) {
                        idsMessages.add(
                            it.id!!
                        )
                    }
                }

                res.data = ChatGetMessagesResponse.Data(idsMessages, messages)
                withContext(Dispatchers.Main) {
                    callback(res)
                }
            }
        }
    }

    override fun join(request: AnyRequest, callback: (response: ChatJoinResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun leave(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun blockUser(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {

    }

    override fun delete(request: AnyRequest, callback: (response: ChatDeleteResponse) -> Unit) {

    }

    override fun clearHistory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun deleteAdmin(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun setGroupRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun setAdminRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun createNew(
        request: AnyRequest,
        callback: (response: ChatCreateNewResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun setChatPrivacy(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun updateInviteHash(
        request: AnyRequest,
        callback: (response: ChatUpdateInviteHashResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun edit(request: AnyRequest, callback: (response: ChatCreateNewResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun setUserRoles(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun userChats(
        request: AnyRequest,
        callback: (response: ChatUserChatsResponse) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val req = request.data.param
            if (req is ChatUserChatsRequest) {
                val res = ChatUserChatsResponse()
                val chats =
                    AppDatabase.getAppDataBase(context).chatDao()
                        .getAllChatsByType(req.type!!)
                res.data = ChatUserChatsResponse.Data(mutableListOf(), chats)
                callback(res)
            }
        }
    }

    override fun getMembers(
        request: AnyRequest,
        callback: (response: ChatGetMembersResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getMembersAmount(
        request: AnyRequest,
        callback: (response: ChatGetMembersAmountResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun mute(request: AnyRequest, callback: (response: ChatMuteResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun userAction(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun addFavorite(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun addMembers(
        request: AnyRequest,
        callback: (response: ChatAddMembersResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getUserRole(
        request: AnyRequest,
        callback: (response: ChatGetUserRoleResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }
}