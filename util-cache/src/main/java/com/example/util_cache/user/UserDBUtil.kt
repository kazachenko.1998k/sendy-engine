package com.example.util_cache.user

import com.morozov.core_backend_api.user.User
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun UserDao.saveOrUpdateUser(user: User): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            if (getUserById(user.uid).isEmpty())
                insert(user)
            else
                update(user)
            emitter.onSuccess(true)
            deleteIfExists(user.toMini())
        }
    }
}

fun UserDao.deleteIfExists(user: User): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            delete(user)
            emitter.onSuccess(true)
        }
    }
}