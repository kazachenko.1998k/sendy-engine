package com.example.util_cache.user

import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun UserDao.saveOrUpdateUser(user: UserMini): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            val userById = getUserById(user.uid)
            when {
                userById.isNotEmpty() -> update(user.getFullUser(userById.first()))
                getUserMiniById(user.uid).isEmpty() -> insert(user)
                else -> update(user)
            }
            emitter.onSuccess(true)
        }
    }
}

private fun UserMini.getFullUser(baseUser: User): User {
    baseUser.avatarFileId = avatarFileId
    baseUser.nick = nick
    baseUser.firstName = firstName
    baseUser.secondName = secondName
    baseUser.isDeleted = isDeleted
    baseUser.isConfirmed = isConfirmed
    baseUser.lastActive = lastActive.toLong()
    baseUser.isSubscribe = isSubscribe
    return baseUser
}

fun UserDao.deleteIfExists(user: UserMini): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            delete(user)
            emitter.onSuccess(true)
        }
    }
}