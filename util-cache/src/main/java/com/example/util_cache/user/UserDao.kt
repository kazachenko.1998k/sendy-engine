package com.example.util_cache.user

import androidx.room.*
import com.morozov.core_backend_api.user.ContactDB
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import io.reactivex.Flowable

/**         Use info
 * To update local cache use UserDBUtil or UserMiniDBUtil extensions.
 * To get data use getter.
 * */
@Dao
interface UserDao {

    /**
     *  For full User model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: List<User>)

    @Update
    suspend fun update(user: User)

    @Delete
    suspend fun delete(user: User)

    @Query("SELECT * FROM UserTable WHERE uid LIKE :userUid")
    suspend fun getUserById(userUid: Long): List<User>

    @Query("SELECT * FROM UserTable WHERE uid = :userUid")
    suspend fun getSingleUserById(userUid: Long): User?

    @Query("SELECT * FROM UserTable WHERE uid IN (:userUid)")
    suspend fun getUserByIds(userUid: List<Long>): List<User>

    @Query("SELECT * FROM UserTable")
    suspend fun getAllUsers(): List<User>

    /**
     * Get a last active time by user id.
     * @return the last active time from the table with a specific id.
     */
    @Query("SELECT lastActive FROM UserTable WHERE uid = :id")
    fun listenerOnUpdateOnlineStatus(id: Long): Flowable<Long?>

    fun getDistinctListenerOnUpdateOnlineStatus(id: Long):
            Flowable<Long?> = listenerOnUpdateOnlineStatus(id)
        .distinctUntilChanged()


    /**
     * For User Mimi model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: UserMini)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertWithOutUpdate(user: UserMini)

    @Update
    suspend fun update(user: UserMini)

    @Delete
    suspend fun delete(user: UserMini)

    @Deprecated("Not correct query", ReplaceWith("getSingleUserMiniById", "userUid: Long"))
    @Query("SELECT * FROM UserMiniTable WHERE uid LIKE :userUid")
    suspend fun getUserMiniById(userUid: Long): List<UserMini>

    @Query("SELECT * FROM UserMiniTable WHERE uid = :userUid")
    suspend fun getSingleUserMiniById(userUid: Long): UserMini?

    @Query("SELECT * FROM UserMiniTable")
    suspend fun getAllUsersMini(): List<UserMini>

    /**
     *  For full Contact model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(contact: ContactDB)

    @Update
    suspend fun update(contact: ContactDB)

    @Delete
    suspend fun delete(contact: ContactDB)

    @Deprecated("Not correct query", ReplaceWith("getSingleContactById", "contactUid: Long"))
    @Query("SELECT * FROM ContactTable WHERE uid LIKE :contactUid")
    suspend fun getContactById(contactUid: Long): List<ContactDB>

    @Query("SELECT * FROM ContactTable WHERE uid = :contactUid")
    suspend fun getSingleContactById(contactUid: Long): ContactDB?

    @Query("SELECT * FROM ContactTable")
    suspend fun getAllContacts(): List<ContactDB>

    /**
     * For ContactMini model
     * */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(contact: ContactMiniDB)

    @Update
    suspend fun update(contact: ContactMiniDB)

    @Delete
    suspend fun delete(contact: ContactMiniDB)

    @Deprecated("Not correct query", ReplaceWith("getSingleContactMiniById", "contactUid: Long"))
    @Query("SELECT * FROM ContactMiniTable WHERE uid LIKE :contactUid")
    suspend fun getContactMiniById(contactUid: Long): List<ContactMiniDB>

    @Query("SELECT * FROM ContactMiniTable WHERE uid = :contactUid")
    suspend fun getSingleContactMiniById(contactUid: Long): ContactMiniDB?

    @Query("SELECT * FROM ContactMiniTable")
    suspend fun getAllContactsMini(): List<ContactMiniDB>

    @Query("UPDATE UserTable SET lastActive = :lastActiveTime WHERE UserTable.uid = :userUid AND UserTable.lastActive < :lastActiveTime")
    suspend fun updateUserLastActive(userUid: Long, lastActiveTime: Long)

}