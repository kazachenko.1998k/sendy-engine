package com.example.util_cache.user

import android.content.Context
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_api.user.requests.UserGetByIdsRequest
import com.morozov.core_backend_api.user.responses.*
import io.reactivex.Scheduler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserDaoImpl(val context: Context) : UserApi {

    override fun getByIds(request: AnyRequest, callback: (response: UserGetByIdsResponse) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val req = request.data.param
            if (req is UserGetByIdsRequest) {
                val res = UserGetByIdsResponse()
                res.data = UserGetByIdsResponse.Data(
                    AppDatabase.getAppDataBase(context).userDao().getUserByIds(req.uids)
                        .toMutableList()
                )
                withContext(Dispatchers.Main) {
                    callback(res)
                }
            }
        }
    }

    override fun getByIdFromDB(request: Long, callback: (response: User) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val res = getUserFromBD(request)
            if (res != null) withContext(Dispatchers.Main) {
                callback(res)
            }
        }
    }

    private suspend fun getUserFromBD(request: Long): User? {
        val userDao = AppDatabase.getAppDataBase(context).userDao()
        val user = userDao.getSingleUserById(request)
        if (user != null) return user
        val userMini = userDao.getSingleUserMiniById(request)
        if (userMini != null) return User(
            uid = userMini.uid,
            avatarFileId = userMini.avatarFileId,
            nick = userMini.nick,
            firstName = userMini.firstName,
            secondName = userMini.secondName,
            isDeleted = userMini.isDeleted,
            isConfirmed = userMini.isConfirmed,
            lastActive = userMini.lastActive,
            isSubscribe = userMini.isSubscribe
        )
        val userContact = userDao.getSingleContactById(request)
        if (userContact != null) return User(
            uid = userContact.uid,
            avatarFileId = userContact.avatarFileId,
            nick = userContact.nick,
            firstName = userContact.firstName,
            secondName = userContact.secondName,
            isDeleted = userContact.isDeleted,
            isConfirmed = userContact.isConfirmed,
            lastActive = userContact.lastActive,
            isSubscribe = userContact.isSubscribe,
            langId = userContact.langId,
            timeReg = userContact.timeReg,
            countryId = userContact.countryId,
            cityId = userContact.cityId,
            backgroundFileId = userContact.backgroundFileId,
            shortBio = userContact.shortBio,
            isOfficial = userContact.isOfficial,
            phone = userContact.phone,
            status = userContact.status,
            isBan = userContact.isBan,
            banExpired = userContact.banExpired,
            feedChatId = userContact.feedChatId,
            subscribers = userContact.subscribers,
            galleryChatId = userContact.galleryChatId,
            favoritesChatId = userContact.favoritesChatId,
            isBlocked = userContact.isBlocked,
            categories = userContact.categories,
            hashtags = userContact.hashtags,
            favoritesHashtags = userContact.favoritesHashtags,
            links = userContact.links
        )

        val userContactMini = userDao.getSingleContactMiniById(request)
        if (userContactMini != null) return User(
            uid = userContactMini.uid,
            avatarFileId = userContactMini.avatarFileId,
            nick = userContactMini.nick,
            firstName = userContactMini.firstName,
            secondName = userContactMini.secondName,
            isDeleted = userContactMini.isDeleted,
            isConfirmed = userContactMini.isConfirmed,
            lastActive = userContactMini.lastActive,
            isSubscribe = userContactMini.isSubscribe,
            phone = userContactMini.phone
        )
        return null
    }

    override fun saveProfile(
        request: AnyRequest,
        callback: (response: UserSaveProfileResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getByNick(
        request: AnyRequest,
        callback: (response: UserGetByNickResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getLastActive(
        request: AnyRequest,
        callback: (response: UserGetLastActiveResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getByIdsMini(
        request: AnyRequest,
        callback: (response: UserGetByIdsMiniResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun block(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun delete(callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun savePreference(
        request: AnyRequest,
        callback: (response: UserSavePreferenceResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun setCategory(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun saveHashtags(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun addHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun deleteHashtag(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun setLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun deleteLink(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun subscribe(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun subscribeQueue(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun subscribeConfirm(
        request: AnyRequest,
        callback: (response: EmptyResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getSubscribers(
        request: AnyRequest,
        callback: (response: UserGetSubscribersResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun getSubscribersMini(
        request: AnyRequest,
        callback: (response: UserGetSubscribersMiniResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun importContacts(
        request: AnyRequest,
        callback: (response: UserImportContactsResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun addContact(
        request: AnyRequest,
        callback: (response: UserAddContactResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }

    override fun deleteContact(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun getContacts(callback: (response: UserGetContactsResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun sendStatus(request: AnyRequest, callback: (response: EmptyResponse) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun subscribeStatus(
        scheduler: Scheduler,
        callback: (response: UserStatusResponse) -> Unit
    ) {
        TODO("Not yet implemented")
    }
}