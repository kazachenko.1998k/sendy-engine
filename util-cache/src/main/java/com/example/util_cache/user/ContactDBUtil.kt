package com.example.util_cache.user

import com.morozov.core_backend_api.user.ContactDB
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun UserDao.saveOrUpdateContact(contact: ContactDB): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            if (getContactById(contact.uid).isEmpty())
                insert(contact)
            else
                update(contact)
            emitter.onSuccess(true)
            deleteIfExists(contact.toMini())
        }
    }
}

fun UserDao.deleteIfExists(contact: ContactDB): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            delete(contact)
            emitter.onSuccess(true)
        }
    }
}