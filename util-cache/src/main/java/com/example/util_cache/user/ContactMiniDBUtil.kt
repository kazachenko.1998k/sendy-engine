package com.example.util_cache.user

import android.util.Log
import com.morozov.core_backend_api.user.ContactDB
import com.morozov.core_backend_api.user.ContactMiniDB
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun UserDao.saveOrUpdateContactMini(contact: ContactMiniDB): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            val userById = getContactMiniById(contact.uid)
            if (userById.isEmpty()) insert(contact)
            else update(contact)
            emitter.onSuccess(true)
        }
    }
}

private fun ContactMiniDB.getFullContact(baseContact: ContactDB): ContactDB {
    baseContact.avatarFileId = avatarFileId
    baseContact.nick = nick
    baseContact.firstName = firstName
    baseContact.secondName = secondName
    baseContact.isDeleted = isDeleted
    baseContact.isConfirmed = isConfirmed
    baseContact.lastActive = lastActive
    baseContact.isSubscribe = isSubscribe
    return baseContact
}

fun UserDao.deleteIfExists(contact: ContactMiniDB): Single<Boolean> {
    return Single.create { emitter ->
        CoroutineScope(Dispatchers.IO).launch {
            delete(contact)
            emitter.onSuccess(true)
        }
    }
}