package com.example.util_cache

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.util_cache.chat.ChatDao
import com.example.util_cache.message.MessageDao
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatMini
import com.morozov.core_backend_api.chat.UserChat
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.ContactDB
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini

@Database(
    entities = [User::class, UserMini::class, ContactDB::class, ContactMiniDB::class, UserChat::class, Chat::class, ChatMini::class, Message::class],
    version = 15
)
@TypeConverters(MessageConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun messageDao(): MessageDao
    abstract fun chatDao(): ChatDao

    companion object {
        var INSTANCE: AppDatabase? = null
        val nameDB = "sendy-test-database"

        fun getAppDataBase(context: Context): AppDatabase {
            return INSTANCE ?: Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                nameDB
            ).fallbackToDestructiveMigration().build().also {
                INSTANCE = it
            }
        }

    }
}