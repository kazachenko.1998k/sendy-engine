package com.example.util_cache.message

import androidx.room.*
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.Message.Companion.WATCHED

@Dao
interface MessageDao {

    /**
     * For Message model
     * */
    @Suppress("DEPRECATION")
    @Transaction
    fun insertWithUpdate(message: Message) {
        insert(message)
        updateChat(message)
    }

    @Deprecated("Use insertWithUpdate")
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(message: Message)

    @Query("UPDATE ChatTable SET lastMessage = :message, lastMessageTime = :lastMessageTime, lastMessageId = :lastMessageId WHERE ChatTable.id = :chatId AND ChatTable.lastMessageTime < :lastMessageTime")
    fun updateChat(
        message: Message,
        lastMessageTime: Long = message.timeAdd ?: -1,
        lastMessageId: Long = message.id ?: -1,
        chatId: Long = message.chatId ?: -1
    )

    @Update
    fun update(message: Message)

    @Delete
    fun deleteData(message: Message)

    @Query("SELECT * FROM MessageTable WHERE MessageTable.chatId = :chatId AND MessageTable.timeAddMS = (SELECT MAX(timeAddMS) FROM MessageTable WHERE MessageTable.chatId = :chatId)")
    fun getLastMessage(chatId: Long): Message?

    @Transaction
    fun delete(message: Message) {
        deleteData(message)
        updateChat(getLastMessage(message.chatId ?: return) ?: return)
    }

    @Query("SELECT * FROM MessageTable WHERE id LIKE :messageId")
    suspend fun getMessageById(messageId: Long): List<Message>

    @Query("SELECT * FROM MessageTable")
    suspend fun getAllMessages(): List<Message>

    @Query("UPDATE MessageTable SET status = $WATCHED WHERE status <> $WATCHED AND id <= :lastViewMessageId AND chatId = (SELECT chatId FROM MessageTable WHERE id = :lastViewMessageId) AND uid <> :myId")
    suspend fun readMessage(lastViewMessageId: Long, myId: Long = SignData.uid)
}