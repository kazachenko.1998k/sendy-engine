# android-backend

[![](https://jitpack.io/v/top.fcorp.git.socialmessenger/android-backend.svg)](https://jitpack.io/#top.fcorp.git.socialmessenger/android-backend)

lib-backend instruction to connect:
1) Add this string into gradle.properties: "authToken=jp_12rimop6frjdmkghfpgtfn24ct"
2) Add this one into build.gragle of your project: 
allprojects {
    repositories {
        ...
        maven {
            url "https://jitpack.io"
            credentials { username authToken }
        }
    }
}
3) Add this into build.gradle of your module: implementation 'top.fcorp.git.socialmessenger.android-backend:lib-backend:100e2577d6'

*You need: androidX
