package com.morozov.android_backend

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.util_cache.AppDatabase
import com.example.util_cache.user.UserDao
import com.example.util_cache.user.deleteIfExists
import com.example.util_cache.user.saveOrUpdateContactMini
import com.morozov.core_backend_api.user.ContactMiniDB
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataBase = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "sendy-test-database2"
        ).build()

        val userDao = dataBase.userDao()

        buttonAdd.setOnClickListener {
            val userModel = ContactMiniDB()
            userModel.uid = editUserId.text.toString().toLong()
            userModel.nick = editUserName.text.toString()
            userDao.saveOrUpdateContactMini(userModel).subscribeAndShow(userDao)
        }
        buttonDelete.setOnClickListener {
            val userModel = ContactMiniDB()
            userModel.uid = editUserId.text.toString().toLong()
            userModel.nick = editUserName.text.toString()
            userDao.deleteIfExists(userModel).subscribeAndShow(userDao)
        }

        showAllUsersDB(userDao)
    }

    private fun Single<Boolean>.subscribeAndShow(userDao: UserDao): Disposable {
        return subscribe({
            showAllUsersDB(userDao)
        }, {
            it.printStackTrace()
        })
    }

    private fun showAllUsersDB(db: UserDao) {
        CoroutineScope(Dispatchers.IO).launch {
            var string = ""
            for (user in db.getAllContactsMini()) {
                string += "USER: ${user.uid} ${user.nick};\n"
            }
            launch(Dispatchers.Main) {
                textOutput.text = string
            }
        }
    }

    //    @SuppressLint("CheckResult")
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        val featureBackendApi = LibBackendDependency.featureBackendApi(applicationContext)
//        val searchFeedTagsRequest =  GetSearchFeedTagsRequest("a")
//        featureBackendApi.feedMemusApi().getSearchTags(searchFeedTagsRequest).subscribe({
//            if (it == null || it.tags.isEmpty())
//                println("NULL")
//            else {
//                println(it.tags)
//            }
//        }, {
//            it.printStackTrace()
//        })
//
//        val getChatsRequest =
//            GetChatsRequest(
//                0,
//                10
//            )
//        featureBackendApi.chatMemusApi().getChatsPreview(getChatsRequest).subscribe({
//            if (it == null || it.chats.isEmpty())
//                println("NULL")
//            else {
//                println(it.chats)
//            }
//        }, {
//            it.printStackTrace()
//        })
//        val getChannelsRequest =
//            GetChannelsRequest(
//                0,
//                10
//            )
//        featureBackendApi.chatMemusApi().getChannelsPreview(getChannelsRequest).subscribe({
//            if (it == null || it.channels.isEmpty())
//                println("NULL")
//            else {
//                println(it.channels)
//            }
//        }, {
//            it.printStackTrace()
//        })
//        featureBackendApi.profileMemusApi().getProfileInfo().subscribe ({
//            if (it == null )
//                println("NULL")
//            else {
//                println(it.profile)
//            }
//        }, {
//            it.printStackTrace()
//        })
//
//        val getUserPostRequest = GetUserPostsRequest("1")
//        featureBackendApi.profileMemusApi().getUserPosts(getUserPostRequest).subscribe ({
//            if (it == null || it.postUser.isEmpty())
//                println("NULL")
//            else {
//                println(it.postUser)
//            }
//        }, {
//            it.printStackTrace()
//        })
//
//        val profile = ProfileModel("1","+79213134336","https://sun9-65.userapi.com/c850420/v850420106/1772ab/IJ-9HxcuRjk.jpg","onfound", "ilya.dolgushev@yandex.ru", "Илья", "Просто Илья и все",15683,10,4)
//
//        val updateProfileInfoRequest = UpdateProfileInfoRequest(profile)
//        featureBackendApi.profileMemusApi().updateProfileInfo(updateProfileInfoRequest).subscribe ({
//            if (it == null )
//                println("NULL")
//            else {
//                println(it.profile)
//            }
//        }, {
//            it.printStackTrace()
//        })
//    }
}

